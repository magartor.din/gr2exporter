#include "extendeddataview.h"
#include "ui_extendeddataview.h"

ExtendedDataView::ExtendedDataView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ExtendedDataView)
{
    ui->setupUi(this);
}

ExtendedDataView::~ExtendedDataView()
{
    delete ui;
}
void ExtendedDataView::loadGrannyVariant(granny_variant * data){
    resetGrannyVariant();
    granny_data_type_definition * variantType = &data->Type[0];
    void * variantPtr = data->Object;

    unsigned int i = 0;

    do{
        QString objectName(variantType->Name);
        QString objectValue(ExtendedDataView::unreferenceObj(&variantPtr, variantType));

        QLabel * label_newObjName = new QLabel(objectName);
        QLabel * label_newObjValue = new QLabel(objectValue);

        //lstLabel.append(label_newObjName);
        //lstLabel.append(label_newObjValue);

        ui->gridLayout_extendedData->addWidget(label_newObjName, i, 0);
        ui->gridLayout_extendedData->addWidget(label_newObjValue, i, 1);
        i++;
        variantType = &data->Type[i];
    }while(variantType->Type != GrannyEndMember);

}
void ExtendedDataView::resetGrannyVariant(void){
    for(int i = 0; i < ui->gridLayout_extendedData->rowCount();i++){
        GridLayoutUtil::removeRow(ui->gridLayout_extendedData, i, true);
    }

}
/*__attribute__ ((target("no-sse")))*/
QString ExtendedDataView::unreferenceObj(void ** ptr, granny_data_type_definition * variantType){
    granny_int32x ObjectSize = GrannyGetTotalObjectSize(variantType);
    //granny_int32x MemberSize = GrannyGetMemberTypeSize(variantType);
    switch (variantType->Type) {
        case GrannyEndMember :{
            break;
        }
        case GrannyInlineMember :{
            break;
        }
        case GrannyReferenceMember :{
            break;
        }
        case GrannyReferenceToArrayMember :{
            break;
        }
        case GrannyArrayOfReferencesMember :{
            break;
        }
        case GrannyUnsupportedMemberType_Remove :{
            break;
        }
        case GrannyReferenceToVariantArrayMember :{
            break;
        }
        case GrannyStringMember :{
            QString res;

            char *ptrVal = *(char **)*ptr;
            int i=0;

            while(ptrVal[i] != '\0'){
                res += ptrVal[i++];
            }
            (*(char***)ptr)++;

            return res;
        }
        case GrannyTransformMember :{
            break;
        }
        case GrannyReal32Member :{
            QString res;
            granny_real32 ptrVal = *(granny_real32 *)*ptr;
            (*(granny_real32**)ptr)++;
            res += QString::number(ptrVal);
            for(granny_int32 i = 1; i < variantType->ArrayWidth ; i++){
                res += ",";
                granny_real32 ptrVal = *(granny_real32 *)*ptr;
                (*(granny_real32**)ptr)++;
                res += QString::number(ptrVal);
            }
            return res;
        }
        case GrannyBinormalInt8Member :{
            QString res;
            granny_int8 ptrVal = *(granny_int8 *)*ptr;
            (*(granny_int8**)ptr)++;
            res += QString::number(ptrVal);
            for(granny_int32 i = 1; i < variantType->ArrayWidth ; i++){
                res += ",";
                granny_int8 ptrVal = *(granny_int8 *)*ptr;
                (*(granny_int8**)ptr)++;
                res += QString::number(ptrVal);
            }
            return res;
        }
        case GrannyUInt8Member :
        case GrannyNormalUInt8Member :{
            QString res;
            granny_uint8 ptrVal = *(granny_uint8 *)*ptr;
            (*(granny_uint8**)ptr)++;
            res += QString::number(ptrVal);
            for(granny_int32 i = 1; i < variantType->ArrayWidth ; i++){
                res += ",";
                granny_uint8 ptrVal = *(granny_uint8 *)*ptr;
                (*(granny_uint8**)ptr)++;
                res += QString::number(ptrVal);
            }
            return res;
        }
        case GrannyBinormalInt16Member :
        case GrannyInt16Member :{
            QString res;
            granny_int16 ptrVal = *(granny_int16 *)*ptr;
            (*(granny_int16**)ptr)++;
            res += QString::number(ptrVal);
            for(granny_int32 i = 1; i < variantType->ArrayWidth ; i++){
                res += ",";
                granny_int16 ptrVal = *(granny_int16 *)*ptr;
                (*(granny_int16**)ptr)++;
                res += QString::number(ptrVal);
            }
            return res;
        }
        case GrannyNormalUInt16Member :
        case GrannyUInt16Member :{
            QString res;
            granny_uint16 ptrVal = *(granny_uint16 *)*ptr;
            (*(granny_uint16**)ptr)++;
            res += QString::number(ptrVal);
            for(granny_int32 i = 1; i < variantType->ArrayWidth ; i++){
                res += ",";
                granny_uint16 ptrVal = *(granny_uint16 *)*ptr;
                (*(granny_uint16**)ptr)++;
                res += QString::number(ptrVal);
            }
            return res;
        }
        case GrannyInt32Member :{
            QString res;
            granny_int32 ptrVal = *(granny_int32 *)*ptr;
            (*(granny_int32**)ptr)++;
            res += QString::number(ptrVal);
            for(granny_int32 i = 1; i < variantType->ArrayWidth ; i++){
                res += ",";
                granny_int32 ptrVal = *(granny_int32 *)*ptr;
                (*(granny_int32**)ptr)++;
                res += QString::number(ptrVal);
            }
            return res;
        }
        case GrannyUInt32Member :{
            QString res;
            granny_uint32 ptrVal = *(granny_uint32 *)*ptr;
            (*(granny_uint32**)ptr)++;
            res += QString::number(ptrVal);
            for(granny_int32 i = 1; i < variantType->ArrayWidth ; i++){
                res += ",";
                granny_uint32 ptrVal = *(granny_uint32 *)*ptr;
                (*(granny_uint32**)ptr)++;
                res += QString::number(ptrVal);
            }
            return res;
        }
        case GrannyReal16Member :{
            break;
        }
        case GrannyEmptyReferenceMember :{
            break;
        }
        case GrannyOnePastLastMemberType :{
            break;
        }
        case Grannymember_type_forceint :{
            break;
        }
        default:
            return QString::fromLocal8Bit("Unknown member type");
    }
    QString res("Unsupported type");
    for(granny_int32 i = 0; i < ObjectSize ; i++){
        (*(granny_uint8**)ptr)++;
    }
    return res;
}
