#include "Matrice.h"

template <typename T>
T ***alloc3n_init(int n, T initA, int t1);
/*
template <>
double ***alloc3n_init(int n, double initA, int t1);
*/
template <typename T>
void Addition(T **A, int la, int ca,T **B, int lb, int cb ,T **C, int lc, int cc, int n);
template <typename T>
void Soustraction(T **A, int la, int ca,T **B, int lb, int cb ,T **C, int lc, int cc, int n);
template <typename T>
void Multiplication(T **A, int la, int ca,T **B, int lb, int cb ,T **C, int lc, int cc, int n);
template <typename T>
void init(int n, T initA, T **Mat);
template <>
void init(int n, double initA, double **Mat);
template <>
Matrice<double>::Matrice(Matrice<double> const &);
bool isPowerOf2(int x);

template <typename T>
Matrice<T>::Matrice(T ** matriceSrc, int n){
    this->matrice_size = n;
    this->matrice = this->MatrixAlloc(n);
    for(int i = 0; i < this->matrice_size; i++){
        for(int j = 0; j < this->matrice_size; j++){
            this->matrice[i][j] = matriceSrc[i][j];
        }
    }
    //ctor
}
template <typename T>
Matrice<T>::Matrice(const Matrice<T> &matriceSrc){
    this->matrice_size = matriceSrc.MatrixGetSize();
    this->matrice = matriceSrc.MatrixGetPtr();
    //ctor
}
template <>
Matrice<double>::Matrice(const Matrice<double> &matriceSrc){
    this->matrice_size = matriceSrc.MatrixGetSize();
    this->matrice = this->MatrixAlloc(this->matrice_size);
    double ** matPtrSrc = matriceSrc.MatrixGetPtr();
    this->matrice = this->MatrixAlloc(this->matrice_size);
    for(int i = 0; i < this->matrice_size; i++){
        for(int j = 0; j < this->matrice_size; j++){
            this->matrice[i][j] = matPtrSrc[i][j];
        }
    }
    //Matrice::free2DPtr(matriceSrc.MatrixGetPtr(), matriceSrc.MatrixGetSize());
    //ctor
}
template <>
Matrice<double>::Matrice(double ** matriceSrc, int n){
    this->matrice_size = n;
    this->matrice = this->MatrixAlloc(n);
    for(int i = 0; i < this->matrice_size; i++){
        for(int j = 0; j < this->matrice_size; j++){
            this->matrice[i][j] = matriceSrc[i][j];
        }
    }
}

template <typename T>
Matrice<T> Matrice<T>::operator * (Matrice<T> b){
    T ** res = this->MatrixAlloc(this->matrice_size);
    init(this->matrice_size,(double) 0, res);

	Multiplication(this->MatrixGetPtr(),0,0,b.MatrixGetPtr,0,0,res,0,0,this->matrice_size);

    Matrice matRes(res, this->matrice_size);
    return matRes;
}
template <>
Matrice<double> Matrice<double>::operator * (Matrice<double> b){
    if(this->matrice_size != b.MatrixGetSize()){
        //Erreur les matrice doivent avoir la meme tailles
        //Car matrice carre
        double ** res = this->MatrixAlloc(this->matrice_size);
        init(this->matrice_size, (double)0, res);
        Matrice matRes(res, this->matrice_size);
        Matrice::free2DPtr(res, this->matrice_size);
        return matRes;
    }
    else if(isPowerOf2(this->matrice_size)){
        //Utilisation algo strassen
        double ** res = this->MatrixAlloc(this->matrice_size);
        init(this->matrice_size, (double)0, res);

        Multiplication(this->MatrixGetPtr(),0,0,b.MatrixGetPtr(),0,0,res,0,0,this->matrice_size);

        Matrice matRes(res, this->matrice_size);
        Matrice::free2DPtr(res, this->matrice_size);
        return matRes;
    }
    else{
        double ** res = this->MatrixAlloc(this->matrice_size);
        double **matB = b.MatrixGetPtr();
        double **matA = this->MatrixGetPtr();
        //Taille pas puissance 2
        for(int i = 0; i < this->matrice_size; i++){
            for(int j = 0; j < this->matrice_size; j++){
                double resInd = 0;
                for(int k = 0; k < this->matrice_size; k++){
                    resInd += matA[i][k]*matB[k][j];
                }
                res[i][j] = resInd;
            }
        }
        Matrice matRes(res, this->matrice_size);
        Matrice::free2DPtr(res, this->matrice_size);
        return matRes;
    }
}
template <>
Matrice<float>::Matrice(float ** matriceSrc, int n){
    this->matrice_size = n;
    this->matrice = this->MatrixAlloc(n);
    for(int i = 0; i < this->matrice_size; i++){
        for(int j = 0; j < this->matrice_size; j++){
            this->matrice[i][j] = matriceSrc[i][j];
        }
    }
}

template <>
Matrice<double>::Matrice(double matriceSrc[][4]){
    this->matrice_size = 4;
    this->matrice = MatrixAlloc(4);
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            this->matrice[i][j] = matriceSrc[i][j];
        }
    }
}
template <>
Matrice<double>::Matrice(double matriceSrc[][3]){
    this->matrice_size = 3;
    this->matrice = MatrixAlloc(3);
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            this->matrice[i][j] = matriceSrc[i][j];
        }
    }
}
template <>
Matrice<float>::Matrice(float matriceSrc[][4]){
    this->matrice_size = 4;
    this->matrice = MatrixAlloc(4);
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            this->matrice[i][j] = matriceSrc[i][j];
        }
    }
}
template <>
Matrice<float>::Matrice(float matriceSrc[][3]){
    this->matrice_size = 4;
    this->matrice = MatrixAlloc(4);
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            this->matrice[i][j] = matriceSrc[i][j];
        }
    }
}

template <typename T>
Matrice<T>::~Matrice(){
    if(this->matrice != nullptr){
        this->MatrixDelete(this->matrice, this->matrice_size);
    }
    //dtor
}
template <typename T>
T ***alloc3n_init(int n, T initA, int t1){
	int i,j,k;
	int ***res;
	//res=(int***)malloc(t1 * sizeof(int**));
	res = new T**[t1];
	for(i=0;i<t1;i++)
	{
		//res[i] = (int**)malloc(n * sizeof(int*));
		res[i] = new T * [n];
		for(j=0;j<n;j++)
		{
			//res[i][j]=(int*)malloc(n * sizeof(int));
			res[i][j] = new T[n];
			for(k=0;k<n;k++)
			{
				res[i][j][k]=initA;

			}
		}

	}
	return res;
}
template <>
double ***alloc3n_init(int n, double initA, int t1){
	double ***res;
	//res=(int***)malloc(t1 * sizeof(int**));
	res = new double**[t1];
	for(int i=0;i<t1;i++)
	{
		//res[i] = (int**)malloc(n * sizeof(int*));
		res[i] = new double * [n];
		for(int j=0;j<n;j++)
		{
			//res[i][j]=(int*)malloc(n * sizeof(int));
			res[i][j] = new double[n];
			for(int k=0;k<n;k++)
			{
				res[i][j][k]=initA;

			}
		}

	}
	return res;
}
template <typename T>
void delete3n(T ***lMat, int n, int t1){
	for(int i=0;i<t1;i++){
		for(int j=0;j<n;j++){
			delete lMat[i][j];
		}
		delete lMat[i];
	}
	delete lMat;
}

template <>
void delete3n(double ***lMat, int n, int t1){
	for(int i=0;i<t1;i++){
		for(int j=0;j<n;j++){
			delete lMat[i][j];
		}
		delete lMat[i];
	}
	delete lMat;
}
template <typename T>
void Multiplication(T **A, int la, int ca,T **B, int lb, int cb ,T **C, int lc, int cc, int n){
	if(n==1)
	{
		C[lc][cc]=A[la][ca] * B[lb][cb];
	}
	else
	{
		T ***M;
		M = alloc3n_init<T>(n/2, (T)0, 9);
		/*M1 = (A11 + A22) * (B11 + B22)*/
		Addition(A,la,ca,		A,la+(n/2),ca +(n/2),	M[0],0,0,(n/2));
		Addition(B,lb,cb,		B,lb+(n/2),cb +(n/2),	M[8],0,0,(n/2));
		Multiplication(M[0],0,0,	M[8],0,0,M[1],0,0,(n/2));
		/*printf("M1 = (A11+A22)*(B11+B22) = ");
		afficheMatrice(M[1],(n/2));*/

		/*M2 = (A21 + A22)*B11*/
		Addition(A,la+(n/2),ca,		A,la+(n/2),ca+(n/2),	M[0],0,0,(n/2));
		Multiplication(M[0],0,0,	B,lb,cb,		M[2],0,0,(n/2));
		/*printf("M2 = (A21 + A22)*B11 = ");
		afficheMatrice(M[2],(n/2));*/

		/*M3 = A11*(B12-B22)*/
		Soustraction(B,lb,cb+(n/2),	B,lb+(n/2),cb+(n/2),	M[0],0,0,(n/2));
		Multiplication(A,la,ca,		M[0],0,0,		M[3],0,0,(n/2));
		/*printf("M3 = A11*(B12-B22) = ");
		afficheMatrice(M[3],(n/2));*/

		/*M4 = A22*(B21-B11)*/
		Soustraction(B,lb+(n/2),cb,		B,lb,cb,	M[0],0,0,(n/2));
		Multiplication(A,la+(n/2),ca+(n/2),	M[0],0,0,	M[4],0,0,(n/2));
		/*printf("M4 = A22*(B21-B11) = ");
		afficheMatrice(M[4],(n/2));*/

		/*M5 = (A11+A12)*B22*/
		Addition(A,la,ca,		A,la,ca+(n/2),		M[0],0,0,(n/2));
		Multiplication(M[0],0,0,	B,lb+(n/2),cb+(n/2),	M[5],0,0,(n/2));
		/*printf("M5 = (A11+A12)*B22 = ");
		afficheMatrice(M[5],(n/2));*/

		/*M6 = (A21-A11)*(B11+B12)*/
		Addition(B,lb,cb,		B,lb,cb+(n/2),		M[0],0,0,(n/2));
		Soustraction(A,la+(n/2),ca,	A,la,ca,		M[8],0,0,(n/2));
		Multiplication(M[8],0,0,	M[0],0,0,		M[6],0,0,(n/2));
		/*printf("M6 = (A21-A11)*(B11+B12) = ");
		afficheMatrice(M[6],(n/2));*/

		/*M7 = (A12-A22)*(B21+B22)*/
		Addition(B,lb+(n/2),cb,		B,lb+(n/2),cb+(n/2),	M[8],0,0,(n/2));
		Soustraction(A,la,ca+(n/2),	A,la+(n/2),ca+(n/2),	M[0],0,0,(n/2));
		Multiplication(M[0],0,0,	M[8],0,0,	M[7],0,0,(n/2));
		/*printf("M7 = (A12-A22)*(B21+B22) = ");
		afficheMatrice(M[7],(n/2));*/

		/*calcul de C*/
		init(n,(double)0,C);
		/*C11 = M1 + M4 - M5 + M7*/
		Addition(M[1],0,0,		M[4],0,0,	M[0],0,0,(n/2));
		Soustraction(M[0],0,0,		M[5],0,0,	M[8],0,0,(n/2));
		Addition(M[8],0,0,		M[7],0,0,	C,lc,cc,(n/2));

		/*C12 = M3 + M4*/
		Addition(M[3],0,0,		M[5],0,0,	C,lc,cc+(n/2),(n/2));
		/*C21 = M2 + M4*/
		Addition(M[2],0,0,		M[4],0,0,	C,lc+(n/2),cc,(n/2));
		/*C22 = M1 - M2 + M3 + M6*/
		Addition(M[3],0,0,		M[6],0,0,	M[0],0,0,(n/2));
		Soustraction(M[1],0,0,		M[2],0,0,	M[8],0,0,(n/2));
		Addition(M[8],0,0,		M[0],0,0,	C,lc+(n/2),cc+(n/2),(n/2));
		delete3n(M, n/2, 9);
	}
}
template <typename T>
void Soustraction(T **A, int la, int ca,T **B, int lb, int cb ,T **C, int lc, int cc, int n){
	if(n==1)
	{
		C[lc][cc]=A[la][ca] - B[lb][cb];
	}
	else
	{
		Soustraction(A, la, ca,			B, lb,cb, 			C, lc, cc, 			(n/2));
		Soustraction(A, la, ca+((n)/2),		B, lb, cb+((n)/2),		C, lc, cc+((n)/2), 		((n+1)/2));
		Soustraction(A,la + (n/2), ca,		B, lb +(n/2), cb,		C, lc +(n/2), cc, 		((n+1)/2));
		Soustraction(A,la+(n/2), ca +((n+1)/2),	B,lb + (n/2), cb + ((n+1)/2),	C, lc + (n/2), cc +((n+1)/2), 	((n+1)/2));

	}
}
template <typename T>
void Addition(T **A, int la, int ca,T **B, int lb, int cb ,T **C, int lc, int cc, int n){
	if(n==1)
	{
		C[lc][cc]=A[la][ca] + B[lb][cb];
	}
	else
	{
		Addition(A, la, ca,			B, lb,cb, 			C, lc, cc, 			(n/2));
		Addition(A, la, ca+((n)/2),		B, lb, cb+((n)/2),		C, lc, cc+((n)/2), 		((n+1)/2));
		Addition(A,la + (n/2), ca,		B, lb +(n/2), cb,		C, lc +(n/2), cc, 		((n+1)/2));
		Addition(A,la+(n/2), ca +((n+1)/2),	B,lb + (n/2), cb + ((n+1)/2),	C, lc + (n/2), cc +((n+1)/2), 	((n+1)/2));

	}
}
template <typename T>
void init(int n, T initA, T **Mat){
	int i,j;
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			Mat[i][j]=initA;
		}
	}
}
template <>
void init(int n, double initA, double **Mat){
	int i,j;
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			Mat[i][j]=initA;
		}
	}
}
bool isPowerOf2(int x){
    int mask = 1;
    bool res = false;
    long long limit = (1<<(sizeof(int)*8 - 1));
    for(int i = 0; i < limit; i++){
        if((x&mask) != 0){
            if(res == true){
                return false;
            }
            res = true;
        }
    }
    return res;
}
