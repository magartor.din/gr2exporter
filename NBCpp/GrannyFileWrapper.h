#ifndef GRANNYFILEWRAPPER_H
#define GRANNYFILEWRAPPER_H

#include <unistd.h>
#include <string>
#include <cstring>
#include "granny.h"

class GrannyFileWrapper
{
    public:
        GrannyFileWrapper(granny_file * granny_file);
        virtual ~GrannyFileWrapper();

        granny_file * wrappedFile;
        granny_file_info * m_info;

        granny_art_tool_info * getArtToolInfoPtr();
        void setFromArtToolInfo(std::string toolName, int majorNum, int minorNum);
        std::string getArtToolInfoName();
        void setFromArtToolName(std::string meshName);
        void setArtToolRevisionNumbers(int majorNum, int minorNum);
        void setUnitsPerMeter(float unitsPerMeter);
        void setOrigin(float * origin);
        void setMatrix(float * matrix);
        granny_exporter_info * getExporterInfoPtr();
        void setExporterInfo(std::string name, int majorNum, int minorNum, int customization, int build);
        std::string getExporterName();
        void setExporterName(std::string meshName);
        void setExporterNumbers(int majorNum, int minorNum, int customization, int build);
        void setFromFileName(std::string meshName);
        int getNumMeshes();
        void setNumMeshes(int numMeshes);
        granny_mesh ** getMeshesPtr();
        int getNumMaterials();
        void setNumMaterials(int num);
        granny_material ** getMaterialsPtr();
        int getNumModels();
        void setNumModels(int num);
        granny_model ** getModelsPtr();
        int getNumTrackGroups();
        void setNumTrackGroups(int num);
        granny_track_group ** getTrackGroupsPtr();
        void setTrackGroupsPtr(granny_track_group ** ptr);
        int getNumAnimations();
        void setNumAnimations(int num);
        granny_animation ** getAnimationsPtr();
        void setAnimationsPtr(granny_animation ** ptr);
        int getNumSkeletons();
        void setNumSkeletons(int num);
        granny_skeleton ** getSkeletonsPtr();
        int getNumVertexDatas();
        void setNumVertexDatas(int num);
        granny_vertex_data ** getVertexDatasPtr();
        int getNumTriTopologies();
        void setNumTriTopologies(int num);
        granny_tri_topology ** getTriTopologiesPtr();
        void addSkeletonPointer(granny_skeleton * skeletonPtr);
        void addVertexDatasPointer(granny_vertex_data * VertexPtr);
        void addTriTopologiesPointer(granny_tri_topology * TriTopoPtr);
        void addTrackGroupsPointer(granny_track_group * trackGroupPtr);
    protected:

    private:
};
/*C#





*/
#endif // GRANNYFILEWRAPPER_H
