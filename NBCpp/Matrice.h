#ifndef MATRICE_H
#define MATRICE_H


template <typename T>
class Matrice{
    public:
        Matrice(T ** matriceSrc, int n);
		Matrice(const Matrice<T> &);
        //template<unsigned int N>
        Matrice(T matriceSrc[][4]);
		Matrice(T matriceSrc[][3]);

        virtual ~Matrice();
        T MatrixDet(void) const {
            if(this->matrice_size == 1){
                return this->matrice[0][0];
            }
            else if(this->matrice_size == 2){
                return this->matrice[0][0]*this->matrice[1][1] - this->matrice[0][1]*this->matrice[1][0];
            }
            else{
                T ***SubMat = new T **[this->matrice_size];//Cr�� la liste des sous matrice
                T res = 0;
                for(int i = 0; i < this->matrice_size; i++){
                    //Allou les collones des sous matrice
                    SubMat[i] = new T *[this->matrice_size-1];
                    for(int j = 0; j < this->matrice_size-1; j++){
                        //Allou les lignes des sous matrice
                        SubMat[i][j] = new T[this->matrice_size-1];
                    }
                    for(int j = 0; j < this->matrice_size-1; j++){
                        int l = 0;
                        for(int k = 0; k < i; k++){//Pour toute les element de la ligne a droite du coef
                            SubMat[i][j][l++] = this->matrice[1+j][k];
                        }
                        for(int k = i+1; k < this->matrice_size; k++){//Pour toute les element de la ligne a gauche du coef
                            SubMat[i][j][l++] = this->matrice[1+j][k];
                        }
                    }
                }
                //Calcul le d�terminant
                for(int i = 0; i < this->matrice_size; i++){
                    if(i%2 == 1){
                        res -= this->matrice[0][i]*MatrixDet(SubMat[i], this->matrice_size-1);
                    }
                    else{
                        res += this->matrice[0][i]*MatrixDet(SubMat[i], this->matrice_size-1);
                    }

                }
                //Lib�re la m�moire
                for(int i = 0; i < this->matrice_size; i++){
                    for(int j = 0; j < this->matrice_size-1; j++){
                        delete SubMat[i][j];    //Supprime une ligne de sous matrice
                    }
                    delete SubMat[i];       //Supprime la liste des lignes
                }
                delete SubMat;      //Supprime la liste des sous matrices

                return res;
            }
        }
        T MatrixDet(T ** matrice, int matrice_size) const {
            if(matrice_size == 1){
                return matrice[0][0];
            }
            else if(matrice_size == 2){
                return matrice[0][0]*matrice[1][1] - matrice[0][1]*matrice[1][0];
            }
            else{
                T ***SubMat = new T **[matrice_size];//Cr�� la liste des sous matrice
                T res = 0;
                for(int i = 0; i < matrice_size; i++){
                    //Allou les collones des sous matrice
                    SubMat[i] = new T *[matrice_size-1];
                    for(int j = 0; j < matrice_size-1; j++){
                        //Allou les lignes des sous matrice
                        SubMat[i][j] = new T[matrice_size-1];
                    }
                    for(int j = 0; j < matrice_size-1; j++){
                        int l = 0;
                        for(int k = 0; k < i; k++){//Pour toute les element de la ligne a droite du coef
                            SubMat[i][j][l++] = matrice[1+j][k];
                        }
                        for(int k = i+1; k < matrice_size; k++){//Pour toute les element de la ligne a gauche du coef
                            SubMat[i][j][l++] = matrice[1+j][k];
                        }
                    }
                }
                //Calcul le d�terminant
                for(int i = 0; i < matrice_size; i++){
                    if(i%2 == 1){
                        res -= matrice[0][i]*MatrixDet(SubMat[i], matrice_size-1);
                    }
                    else{
                        res += matrice[0][i]*MatrixDet(SubMat[i], matrice_size-1);
                    }

                }
                //Lib�re la m�moire
                for(int i = 0; i < matrice_size; i++){
                    for(int j = 0; j < matrice_size-1; j++){
                        delete SubMat[i][j];    //Supprime une ligne de sous matrice
                    }
                    delete SubMat[i];       //Supprime la liste des lignes
                }
                delete SubMat;      //Supprime la liste des sous matrices

                return res;
            }
        }
        void MatrixSwap(void) const {
            for(int i = 0; i < this->matrice_size; i++){
                for(int j = 0; j < i ; j ++){
                    T tempo = this->matrice[i][j];
                    this->matrice[i][j] = this->matrice[j][i];
                    this->matrice[j][i] = tempo;
                }
            }
        }
        void MatrixSwap(T ** matrice, int n) const {
            for(int i = 0; i < n; i++){
                for(int j = 0; j < i ; j ++){
                    T tempo = matrice[i][j];
                    matrice[i][j] = matrice[j][i];
                    matrice[j][i] = tempo;
                }
            }
        }
        T ** MatrixGetSubMatrice(int indexX, int indexY) const {
            if(this->matrice_size < 2){
                return nullptr;
            }

            T** res = MatrixAlloc(this->matrice_size-1);
            int indexMatX = 0;
            int indexMatY = 0;

            for(int i = 0; i < indexX; i++){
                for(int j = 0; j < indexY; j++){
                    res[indexMatX][indexMatY++] = this->matrice[i][j];
                }
                for(int j = indexY + 1; j < this->matrice_size; j++){
                    res[indexMatX][indexMatY++] = this->matrice[i][j];
                }
                indexMatY = 0;
                indexMatX++;
            }
            for(int i = indexX+1; i < this->matrice_size; i++){
                for(int j = 0; j < indexY; j++){
                    res[indexMatX][indexMatY++] = this->matrice[i][j];
                }
                for(int j = indexY + 1; j < this->matrice_size; j++){
                    res[indexMatX][indexMatY++] = this->matrice[i][j];
                }
                indexMatY = 0;
                indexMatX++;
            }
            return res;
        }
        T ** MatrixGetComatrice(void) const {
            //Alloc comatrice
            T **res = MatrixAlloc(this->matrice_size);
            T **subMat;
            int sign = 1;
            for(int i = 0; i < this->matrice_size; i++){
                for(int j = 0; j < this->matrice_size; j++){
                    subMat = this->MatrixGetSubMatrice(i, j);
                    sign = ((i+j)%2 == 1)?-1:1;
                    res[i][j] = sign * MatrixDet(subMat, this->matrice_size-1);
                    MatrixDelete(subMat, this->matrice_size-1);
                }
            }
            return res;
        }
        T ** MatrixAlloc(int n) const {
            T **res = new T *[n];
            for(int i = 0; i < n; i++){
                res[i] = new T[n];
            }
            return res;
        }
        void MatrixDelete(T **Mat, int n) const {
            for(int i = 0; i < n; i++){
                delete Mat[i];
            }
            delete Mat;
        }
        T ** MatrixInverse(void) const{
            //Calcul determinant
            T det = this->MatrixDet();
            if(det == 0){
                return nullptr;
            }
            //Calcul comatrice
            T ** CoMat = MatrixGetComatrice();
            //Swap la comatrice
            MatrixSwap(CoMat, this->matrice_size);
            for(int i = 0; i < this->matrice_size; i++){
                for(int j = 0; j < this->matrice_size; j++){
                    CoMat[i][j] /= det;
                }
            }
            return CoMat;
        }
        T ** MatrixGetPtr(void) const{
            return this->matrice;
        }
        int MatrixGetSize(void) const {
            return this->matrice_size;
        }
        Matrice<T> operator * (Matrice<T> b);
        static void free2DPtr(T** mat, int n) {
            for(int i = 0; i < n; i ++){
                delete mat[i];
            }
            delete mat;
        }
    protected:

    private:
        T **matrice;
        int matrice_size;
};
/* code source sans class
template <typename T>
T MatrixDet(T **Mat, int n);
template <typename T>
void MatrixSwap(T **Mat, int n);
template <typename T>
T ** MatrixGetSubMatrice(T **Mat, int n, int indexX, int indexY);
template <typename T>
void printMat(double **Mat, int n);
template <typename T>
T ** MatrixGetComatrice(double **Mat, int n);
template <typename T>
T ** MatrixAlloc(int n);
template <typename T>
void MatrixDelete(T **Mat, int n);

int nbAlloc = 0;
#define COUNT_ALLOC


void printMat(double **Mat, int n){
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n ; j ++){
            printf("%f, ", Mat[i][j]);
        }
        printf("\n");
    }
}
template <typename T>
void MatrixDelete(T **Mat, int n){
    for(int i = 0; i < n; i++){
#ifdef COUNT_ALLOC
    nbAlloc--;
#endif // COUNT_ALLOC
        delete Mat[i];
    }
#ifdef COUNT_ALLOC
    nbAlloc--;
#endif // COUNT_ALLOC
    delete Mat;
}
template <typename T>
T ** MatrixAlloc(int n){
#ifdef COUNT_ALLOC
    nbAlloc++;
#endif // COUNT_ALLOC
    T **res = new T *[n];
    for(int i = 0; i < n; i++){
#ifdef COUNT_ALLOC
    nbAlloc++;
#endif // COUNT_ALLOC
        res[i] = new T[n];
    }
    return res;
}
template <typename T>
T ** MatrixGetComatrice(T **Mat, int n){
    //Alloc comatrice
    T **res = MatrixAlloc<T>(n);
    T **subMat;
    int sign = 1;
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            subMat = MatrixGetSubMatrice(Mat, n, i, j);
            sign = ((i+j)%2 == 1)?-1:1;
            res[i][j] = sign * MatrixDet(subMat, n-1);
            MatrixDelete(subMat, n-1);
        }
    }
    return res;
}
template <typename T>
T ** MatrixGetSubMatrice(T **Mat, int n, int indexX, int indexY){
    if(n < 2){
        return nullptr;
    }

    T** res = MatrixAlloc<T>(n-1);
    int indexMatX = 0;
    int indexMatY = 0;

    for(int i = 0; i < indexX; i++){
        for(int j = 0; j < indexY; j++){
            res[indexMatX][indexMatY++] = Mat[i][j];
        }
        for(int j = indexY + 1; j < n; j++){
            res[indexMatX][indexMatY++] = Mat[i][j];
        }
        indexMatY = 0;
        indexMatX++;
    }
    for(int i = indexX+1; i < n; i++){
        for(int j = 0; j < indexY; j++){
            res[indexMatX][indexMatY++] = Mat[i][j];
        }
        for(int j = indexY + 1; j < n; j++){
            res[indexMatX][indexMatY++] = Mat[i][j];
        }
        indexMatY = 0;
        indexMatX++;
    }
    return res;
}
template <typename T>
void MatrixSwap(T **Mat, int n){
    for(int i = 0; i < n; i++){
        for(int j = 0; j < i ; j ++){
            T tempo = Mat[i][j];
            Mat[i][j] = Mat[j][i];
            Mat[j][i] = tempo;
        }
    }
}
template <typename T>
T MatrixDet(T **Mat, int n){
    if(n == 1){
        return Mat[0][0];
    }
    else if(n == 2){
        return Mat[0][0]*Mat[1][1] - Mat[0][1]*Mat[1][0];
    }
    else{
#ifdef COUNT_ALLOC
    nbAlloc++;
#endif // COUNT_ALLOC
        T ***SubMat = new T **[n];//Cr�� la liste des sous matrice
        T res = 0;
        for(int i = 0; i < n; i++){
            //Allou les collones des sous matrice
#ifdef COUNT_ALLOC
    nbAlloc++;
#endif // COUNT_ALLOC
            SubMat[i] = new T *[n-1];
            for(int j = 0; j < n-1; j++){
                //Allou les lignes des sous matrice
#ifdef COUNT_ALLOC
    nbAlloc++;
#endif // COUNT_ALLOC
                SubMat[i][j] = new T[n-1];
            }
            for(int j = 0; j < n-1; j++){
                int l = 0;
                for(int k = 0; k < i; k++){//Pour toute les element de la ligne a droite du coef
                    SubMat[i][j][l++] = Mat[1+j][k];
                }
                for(int k = i+1; k < n; k++){//Pour toute les element de la ligne a gauche du coef
                    SubMat[i][j][l++] = Mat[1+j][k];
                }
            }
        }
        //Calcul le determinant
        for(int i = 0; i < n; i++){
            if(i%2 == 1){
                res -= Mat[0][i]*MatrixDet(SubMat[i], n-1);
            }
            else{
                res += Mat[0][i]*MatrixDet(SubMat[i], n-1);
            }

        }
        //Libere la m�moire
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n-1; j++){
#ifdef COUNT_ALLOC
    nbAlloc--;
#endif // COUNT_ALLOC
                delete SubMat[i][j];    //Supprime une ligne de sous matrice
            }
#ifdef COUNT_ALLOC
    nbAlloc--;
#endif // COUNT_ALLOC
            delete SubMat[i];       //Supprime la liste des lignes
        }
#ifdef COUNT_ALLOC
    nbAlloc--;
#endif // COUNT_ALLOC
        delete SubMat;      //Supprime la liste des sous matrices

        return res;
    }
}
template <typename T>
T** MatrixInverse(T **Mat, int n){
    //Calcul determinant
    T det = MatrixDet(Mat, n);
    if(det == 0){
        return nullptr;
    }
    //Calcul comatrice
    T ** CoMat = MatrixGetComatrice(Mat, n);
    //Swap la comatrice
    MatrixSwap(CoMat, n);
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            CoMat[i][j] /= det;
        }
    }
    return CoMat;
}
*/
#endif // MATRICE_H
