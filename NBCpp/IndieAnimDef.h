#ifndef INDIEANIMDEF_H
#define INDIEANIMDEF_H

#include <unistd.h>
#include <string>
#include <cstring>
#include <vector>
#include "granny.h"

class IndieAnimDef
{
    public:
        IndieAnimDef();
        virtual ~IndieAnimDef();
        std::string getEventCodes(void);
        void setEventCodes(std::string value);

        std::string Name;
        std::string StartFrame;
        std::string EndFrame;
        std::string EventCodes;
    protected:

    private:
        std::vector<int> eventCodes;
};

#endif // INDIEANIMDEF_H
