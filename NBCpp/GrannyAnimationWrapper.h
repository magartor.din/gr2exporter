#ifndef GRANNYANIMATIONWRAPPER_H
#define GRANNYANIMATIONWRAPPER_H

#include <unistd.h>
#include <vector>

#include "granny.h"

class GrannyAnimationWrapper
{
    public:
       GrannyAnimationWrapper(granny_animation * inputAnimation);
        virtual ~GrannyAnimationWrapper();

        std::vector< granny_track_group * > * getTrackGroups();
    protected:

    private:
        void * m_pkAnimation;
        std::vector< granny_track_group * > m_lstTrackGroups;
        granny_animation * wrappedAnimation;
};

/*C# definition

        private granny_animation* m_pkAnimation = (granny_animation*)0;
        private List<IGrannyTrackGroup> m_lstTrackGroups;
        private IGrannyAnimation wrappedAnimation;

        public GrannyAnimationWrapper(IGrannyAnimation inputAnimation)
        {
            wrappedAnimation = inputAnimation;
            Type myType = inputAnimation.GetType();
            FieldInfo fm_lstTrackGroups = myType.GetField("m_lstTrackGroups", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            m_lstTrackGroups = (List<IGrannyTrackGroup>)fm_lstTrackGroups.GetValue(inputAnimation);
        }

        public List<IGrannyTrackGroup> getTrackGroups()
        {
            return m_lstTrackGroups;
        }

*/

#endif // GRANNYANIMATIONWRAPPER_H
