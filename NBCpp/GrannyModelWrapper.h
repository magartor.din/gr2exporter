#ifndef GRANNYMODELWRAPPER_H
#define GRANNYMODELWRAPPER_H

#include <unistd.h>
#include <vector>
#include <string>

#include "granny.h"

class GrannyModelWrapper
{
    public:
        GrannyModelWrapper(granny_model * inputModel);
        virtual ~GrannyModelWrapper();

        granny_model* m_pkModel;
        granny_model* wrappedModel;

        std::vector< granny_model_mesh_binding > m_lstMeshBindings;
        std::vector<std::string> m_pkBoneNames;
        granny_skeleton * m_pkSkeleton;
        granny_transform m_kInitialPlacement;

        char const * getNamePtr();
         granny_skeleton * getSkeletonPtr();
        int getNumMeshBindings();
        void setNumMeshBindings(int num);
        granny_model_mesh_binding * getMeshBindingsPtr();
        void setName(std::string meshName);
        std::string getName();
    protected:

    private:
};
/*


*/
#endif // GRANNYMODELWRAPPER_H
