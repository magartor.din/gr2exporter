#ifndef INDIEMATERIAL_H
#define INDIEMATERIAL_H

#include <unistd.h>
#include <string>
#include <cstring>
#include "granny.h"



class IndieMaterial
{
    public:
        IndieMaterial(granny_material * material);
        virtual ~IndieMaterial();
        std::string getName();
        void setName(std::string name);
        //void AddToListView(ListView view)
        static std::string trimPathFromFileName(std::string str);
        std::string getFileNameByUsageName(std::string);
        void CopyTextureIfExists(std::string file, std::string outputFolder);

        std::string getBuildingSREF(void);
        std::string getDiffuse(void);
        std::string getBaseTextureMap(void);
        std::string getSREFMap(void);
        std::string getSpecTextureMap(void);
        std::string getIrradiance(void);
        std::string getSpecSharp(void);
        std::string getSpecSoft(void);
        std::string getDistanceFogRamp(void);
        std::string getColorCorrection(void);
        std::string getFOWColorKey(void);
        std::string getFOWMask(void);
        std::string getFOWBaseLayer(void);
        std::string getUVScrollingMap0(void);
        std::string getAlphaLookupMap0(void);
        std::string getHeightTextureMap(void);
        std::string getFUR_BaseMap(void);
        std::string getFUR_SREF(void);
        std::string getFUR_Transparency(void);
        std::string getFUR_IrradianceMap(void);
        std::string getNormalMap(void);
        std::string getIrradianceMap(void);
        std::string getEnvironmentMap(void);
        std::string getDiffuseMap(void);
        std::string getTransparencyMap(void);
        std::string getTangentMap(void);
        std::string getMaskMap(void);
        std::string getMatte(void);
        std::string getSkinBlurMap(void);
        std::string getTransparency(void);
        std::string getBaseMap(void);
        std::string getSheenMap(void);
        std::string getSPECMap(void);
        std::string getBaseSampler(void);
        std::string getEnvironmentMaskSampler(void);
        std::string getLightCubeMapSampler(void);
        std::string getEnvironmentMapSampler(void);
        std::string getTeamColorSampler(void);
        std::string getIrradianceCubeMap(void);
        std::string getDullEnvironmentCubeMap(void);
        std::string getEnvironmentCubeMap(void);

        granny_material * getMaterial();
        const char * GetType();
    protected:

    private:
        std::string m_name;
        granny_material * material;

};

#endif // INDIEMATERIAL_H
