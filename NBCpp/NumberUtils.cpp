#include "NumberUtils.h"

#ifndef minimum
#define minimum(a,b) (a>b)?a:b
#endif // minimum

NumberUtils::NumberUtils()
{
    //ctor
}

NumberUtils::~NumberUtils()
{
    //dtor
}

float NumberUtils::halfToFloat(unsigned short u){
    int sign = (u >> 15) & 0x00000001;
    int exp = (u >> 10) & 0x0000001F;
    int mant = u & 0x000003FF;
    exp = exp + (127 - 15);
    int i = (sign << 31) | (exp << 23) | (mant << 13);

    union{
        unsigned int iVal;
        char cVal[sizeof(int)];
    }xIntToChar;
    union{
        unsigned char cVal[sizeof(float)];
        float fVal;
    }xCharToFloat;

    xIntToChar.iVal = i;

    memcpy(xCharToFloat.cVal, xIntToChar.cVal, minimum(sizeof(int), sizeof(float)));

    return xCharToFloat.fVal;
}
// http://stackoverflow.com/questions/6162651/half-precision-floating-point-in-java/6162687#6162687
unsigned short NumberUtils::floatToHalf(float f){

    union{
        unsigned char iVal;
        float fVal;
    }xFloatToInt;
    xFloatToInt.fVal = f;

    int fbits = xFloatToInt.iVal;

    int sign = (int)((unsigned int)fbits >> 16 & 0x8000);
    int val = (fbits & 0x7fffffff) + 0x1000; // rounded value

    if (val >= 0x47800000)               // might be or become NaN/Inf
    {                                     // avoid Inf due to rounding
        if ((fbits & 0x7fffffff) >= 0x47800000)
        {                                 // is or must become NaN/Inf
            if (val < 0x7f800000)        // was value but too large
                return (unsigned short)(sign | 0x7c00);     // make it +/-Inf
            return (unsigned short)(sign | 0x7c00 |        // remains +/-Inf or NaN
                (int)((unsigned int)(fbits & 0x007fffff) >> 13)); // keep NaN (and Inf) bits
        }
        return (unsigned short)(sign | 0x7bff);             // unrounded not quite Inf
    }

    if (val >= 0x38800000)               // remains normalized value
        return (unsigned short)(sign | (int)(((unsigned int)val - 0x38000000) >> 13)); // exp - 127 + 15

    if (val < 0x33000000)                // too small for subnormal
        return (unsigned short)sign;                      // becomes +/-0

    val = (int)((unsigned int)(fbits & 0x7fffffff) >> 23);  // tmp exp for subnormal calc
// add subnormal bit // round depending on cut off// div by 2^(1-(exp-127+15)) and >> 13 | exp=0
    return (unsigned short)(sign | (int)((unsigned int)((fbits & 0x7fffff) | 0x800000) + ((int)((unsigned int)0x800000 >> (val - 102)) >> (126 - val))));
}
unsigned short NumberUtils::floatToHalfOld(float f){
    union{
        double dVal;
        unsigned char cVal[sizeof(double)];
        unsigned long long llVal;
    }xDoubleToChar;

    xDoubleToChar.dVal = (double)f;

    unsigned long long bits = xDoubleToChar.llVal;


    unsigned long long exponent = bits & 0x7ff0000000000000L;
    unsigned long long mantissa = bits & 0x000fffffffffffffL;
    unsigned long long sign = bits & 0x8000000000000000L;

    int placement = (int)((exponent >> 52) - 1023);
    if (placement > 15 || placement < -14)
        return floatToHalf(-1.0f); //Whatever counts as error

    unsigned short exponentBits = (unsigned short)((15 + placement) << 10);
    unsigned short mantissaBits = (unsigned short)(mantissa >> 42);
    unsigned short signBits = (unsigned short)(sign >> 48);
    return (unsigned short)(exponentBits | mantissaBits | signBits);
}
int NumberUtils::parseInt(std::string str){
    int res = std::stoi(str);
    return res;
}
float NumberUtils::parseFloat(std::string str){
    int res = std::stof(str);
    return res;
}
bool NumberUtils::almostEquals(float float1, float float2, int precision){
    double epsilon = pow(10.0, -precision);
    return (abs(float1 - float2) <= epsilon);
}
