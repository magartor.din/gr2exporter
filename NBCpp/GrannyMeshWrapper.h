#ifndef GRANNYMESHWRAPPER_H
#define GRANNYMESHWRAPPER_H

#include <unistd.h>
#include <string>
#include <vector>
#include "GrannyMeshVertexStructInfo.h"
#include "GrannyMeshInfo.h"
#include "granny.h"

class GrannyMeshWrapper
{
    public:
        GrannyMeshWrapper(granny_mesh * inputMesh);
        virtual ~GrannyMeshWrapper();

        granny_mesh * wrappedMesh;
        granny_mesh * m_pkMesh;

        std::string getName();
        void setName(std::string meshName);
        char const * getNamePtr();
        granny_vertex_data * getPrimaryVertexDataPtr();
        int getNumMorphTargets();
        granny_morph_target * getMorphTargetsPtr();
        granny_tri_topology * getPrimaryTopologyPtr();
        void setNumMaterialBindings(int num);
        int getNumMaterialBindings();
        granny_material_binding * getMaterialBindingsPtr();
        int getNumBoneBindings();
        void setNumBoneBindings(int num);
        granny_bone_binding * getBoneBindingsPtr();
        int getNumVertices();
        void setNumVertices(int num);
        granny_uint8 * getVerticesPtr();
        int getNumIndices();
        void setNumIndices(int num);
        granny_int32 * getIndicesPtr();
        int getNumIndices16();
        void setNumIndices16(int num);
        granny_uint16 * getIndices16Ptr();
        int getGroup0TriCount();
        void setGroup0TriCount(int num);
        GrannyMeshInfo getMeshInfo();
        void writeMeshInfo(GrannyMeshInfo * meshInfo, bool isLeaderFormat, bool isSceneFormat);
        bool meshEqual(granny_mesh * otherMesh);
    protected:

    private:
        std::vector<GrannyMeshVertexStructInfo> getVertexStructInfos();
        void writeVertices(GrannyMeshInfo * meshInfo, bool isLeaderFormat, bool isSceneFormat);
        void writeTriangles(std::vector<int *> * triangles, bool isLeaderOrSceneFormat);
        void writeBoneBindings(std::vector<std::string> * boneBindings);
};
/*


*/
#endif // GRANNYMESHWRAPPER_H
