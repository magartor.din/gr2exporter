#include "GrannyTransformInfo.h"

GrannyTransformInfo::GrannyTransformInfo()
{
    //ctor
}

GrannyTransformInfo::~GrannyTransformInfo()
{
    //dtor
}

int GrannyTransformInfo::getFlagsInt(granny_uint32 flags){
    int output = 0;
    if ((GrannyHasPosition&flags) != 0){
        output = output + 1;
    }
    if ((GrannyHasOrientation&flags) != 0){
        output = output + 2;
    }
    if ((GrannyHasScaleShear&flags) != 0){
        output = output + 4;
    }
    return output;
}
