#ifndef GRANNYTRANSFORMINFO_H
#define GRANNYTRANSFORMINFO_H


#include <unistd.h>
#include "granny.h"

class GrannyTransformInfo
{
    public:
        GrannyTransformInfo();
        virtual ~GrannyTransformInfo();

        float * position; // float[3] x,y,z
        float * orientation; // float[4] quaternion
        float ** scaleShear; //float[9] - quaternion for first 4 values - then identity matrix
        int flags; // public int flags; // hasOrientationEtc

        static int getFlagsInt(granny_uint32 flags);
    protected:

    private:
};

#endif // GRANNYTRANSFORMINFO_H
