#include "GrannyMeshWrapper.h"

extern int newIndicator;

GrannyMeshWrapper::GrannyMeshWrapper(granny_mesh *  inputMesh)
{
    this->wrappedMesh = inputMesh;
    this->m_pkMesh = inputMesh;
    //ctor
}
GrannyMeshWrapper::~GrannyMeshWrapper()
{
    //dtor
}
std::string GrannyMeshWrapper::getName(){
    std::string resName(this->m_pkMesh->Name);
    return resName;
}
void GrannyMeshWrapper::setName(std::string meshName){
    this->m_pkMesh->Name = meshName.c_str();
}
char const * GrannyMeshWrapper::getNamePtr(){
    return this->m_pkMesh->Name;
}
granny_vertex_data * GrannyMeshWrapper::getPrimaryVertexDataPtr(){
    return this->m_pkMesh->PrimaryVertexData;
}
int GrannyMeshWrapper::getNumMorphTargets(){
    return (int)this->m_pkMesh->MorphTargetCount;
}
granny_morph_target * GrannyMeshWrapper::getMorphTargetsPtr(){
    return this->m_pkMesh->MorphTargets;
}
granny_tri_topology * GrannyMeshWrapper::getPrimaryTopologyPtr(){
    return this->m_pkMesh->PrimaryTopology;
}
void GrannyMeshWrapper::setNumMaterialBindings(int num){
    this->m_pkMesh->MaterialBindingCount = num;
}
int GrannyMeshWrapper::getNumMaterialBindings(){
    return this->m_pkMesh->MaterialBindingCount;
}
granny_material_binding * GrannyMeshWrapper::getMaterialBindingsPtr(){
    return this->m_pkMesh->MaterialBindings;
}
int GrannyMeshWrapper::getNumBoneBindings(){
    return this->m_pkMesh->BoneBindingCount;
}
void GrannyMeshWrapper::setNumBoneBindings(int num){
    this->m_pkMesh->BoneBindingCount = num;
}
granny_bone_binding * GrannyMeshWrapper::getBoneBindingsPtr(){
    return this->m_pkMesh->BoneBindings;
}
int GrannyMeshWrapper::getNumVertices(){
    return this->m_pkMesh->PrimaryVertexData->VertexCount;
}
void GrannyMeshWrapper::setNumVertices(int num){
    this->m_pkMesh->PrimaryVertexData->VertexCount = num;
}
granny_uint8 * GrannyMeshWrapper::getVerticesPtr(){
    return this->m_pkMesh->PrimaryVertexData->Vertices;
}
int GrannyMeshWrapper::getNumIndices(){
    return this->m_pkMesh->PrimaryTopology->IndexCount;
}
void GrannyMeshWrapper::setNumIndices(int num){
    this->m_pkMesh->PrimaryTopology->IndexCount = num;
}
granny_int32 * GrannyMeshWrapper::getIndicesPtr(){
    return this->m_pkMesh->PrimaryTopology->Indices;
}
int GrannyMeshWrapper::getNumIndices16(){
    return this->m_pkMesh->PrimaryTopology->Index16Count;
}
void GrannyMeshWrapper::setNumIndices16(int num){
    this->m_pkMesh->PrimaryTopology->Index16Count = num;
}
granny_uint16 * GrannyMeshWrapper::getIndices16Ptr(){
    return this->m_pkMesh->PrimaryTopology->Indices16;
}
int GrannyMeshWrapper::getGroup0TriCount(){
    return this->m_pkMesh->PrimaryTopology->GroupCount;
}
void GrannyMeshWrapper::setGroup0TriCount(int num){
    this->m_pkMesh->PrimaryTopology->GroupCount = num;
}
GrannyMeshInfo GrannyMeshWrapper::getMeshInfo(){    //Test�
    //instancie un objet GrannyMeshInfo
    GrannyMeshInfo  meshInfo;
    // donne comme nom le pointeur d'une struc=ture
    meshInfo.name.assign(this->m_pkMesh->Name); //Marshal.PtrToStringAnsi((IntPtr)(sbyte*)*(int*)(m_pkMesh));
    // Cr�� une liste de nom
    std::vector<std::string> boneBindings;
    // Pour tout les os
    for (int i = 0; i < this->getNumBoneBindings(); i++){
        std::string stringTempo(this->m_pkMesh->BoneBindings[i].BoneName);
        boneBindings.push_back(stringTempo);
    }
    meshInfo.boneBindings = boneBindings;

    meshInfo.setVertexStructInfos(this->getVertexStructInfos());

    int vertexCount = this->getNumVertices();
    granny_uint8 * vertexPtr = this->getVerticesPtr();

    int vertexSize = meshInfo.bytesPerVertex;

    std::vector<GrannyVertexInfo> vertexInfos;
    for (int i = 0; i < vertexCount; i++){
        GrannyVertexInfo currentVertex;

        GrannyMeshVertexStructInfo * structInfo = meshInfo.getVertexStructInfoByName("Position");
        if (structInfo != nullptr){
            newIndicator++;
            currentVertex.position = new float[3];
            float * tempo = (float*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset );
            currentVertex.position[0] = *tempo;
            delete tempo;
            tempo = (float*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1));
            currentVertex.position[1] = *tempo;
            delete tempo;
            tempo = (float*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 2));
            currentVertex.position[2] = *tempo;
            delete tempo;
        }

        structInfo = meshInfo.getVertexStructInfoByName("BoneWeights");
        if (structInfo != nullptr){
            newIndicator++;
            currentVertex.boneWeights = new int[4];
            unsigned char * tempo = (unsigned char*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset);
            currentVertex.boneWeights[0] = *tempo;
            delete tempo;
            tempo = (unsigned char*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1));
            currentVertex.boneWeights[1] = *tempo;
            delete tempo;
            tempo = (unsigned char*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 2));
            currentVertex.boneWeights[2] = *tempo;
            delete tempo;
            tempo = (unsigned char*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 3));
            currentVertex.boneWeights[3] = *tempo;
            delete tempo;
        }
        else{
            newIndicator++;
            currentVertex.boneWeights = new int[4];
            currentVertex.boneWeights[0] = 255;
            currentVertex.boneWeights[1] = 0;
            currentVertex.boneWeights[2] = 0;
            currentVertex.boneWeights[3] = 0;
        }

        structInfo = meshInfo.getVertexStructInfoByName("BoneIndices");
        if (structInfo != nullptr){
            newIndicator++;
            currentVertex.boneIndices = new int[4];
            unsigned char * tempo = (unsigned char*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset);
            currentVertex.boneIndices[0] = *tempo;
            delete tempo;
            tempo = (unsigned char*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1));
            currentVertex.boneIndices[1] = *tempo;
            delete tempo;
            tempo = (unsigned char*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 2));
            currentVertex.boneIndices[2] = *tempo;
            delete tempo;
            tempo = (unsigned char*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 3));
            currentVertex.boneIndices[3] = *tempo;
            delete tempo;
        }
        else{
            newIndicator++;
            currentVertex.boneIndices = new int[4];
            currentVertex.boneIndices[0] = 0;
            currentVertex.boneIndices[1] = 0;
            currentVertex.boneIndices[2] = 0;
            currentVertex.boneIndices[3] = 0;
        }

        structInfo = meshInfo.getVertexStructInfoByName("Normal");
        if (structInfo != nullptr){
            newIndicator++;
            currentVertex.normal = new float[3];
            float * tempo = (float*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset);
            currentVertex.normal[0] = *tempo;
            delete tempo;
            tempo = (float*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1));
            currentVertex.normal[1] = *tempo;
            delete tempo;
            tempo = (float*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 2));
            currentVertex.normal[2] = *tempo;
            delete tempo;
        }
        else{
            currentVertex.normal = nullptr;
        }

        structInfo = meshInfo.getVertexStructInfoByName("Binormal");
        if (structInfo != nullptr){
            newIndicator++;
            currentVertex.binormal = new float[3];
            float * tempo = (float*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset);
            currentVertex.binormal[0] = *tempo;
            delete tempo;
            tempo = (float*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1));
            currentVertex.binormal[1] = *tempo;
            delete tempo;
            tempo = (float*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 2));
            currentVertex.binormal[2] = *tempo;
            delete tempo;
        }
        else{
            currentVertex.binormal = nullptr;
        }

        structInfo = meshInfo.getVertexStructInfoByName("Tangent");
        if (structInfo != nullptr){
            newIndicator++;
            currentVertex.tangent = new float[3];
            float * tempo = (float*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset);
            currentVertex.tangent[0] = *tempo;
            delete tempo;
            tempo = (float*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1));
            currentVertex.tangent[1] = *tempo;
            delete tempo;
            tempo = (float*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 2));
            currentVertex.tangent[2] = *tempo;
            delete tempo;
        }
        else{
            currentVertex.tangent = nullptr;
        }

        structInfo = meshInfo.getVertexStructInfoByName("TextureCoordinates0");
        if (structInfo != nullptr){
            newIndicator++;
            currentVertex.uv = new float[2];
            float * tempo = (float*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset);
            currentVertex.uv[0] = *tempo;
            delete tempo;
            tempo = (float*)structInfo->convert(vertexPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1));
            currentVertex.uv[1] = *tempo;
            delete tempo;
        }
        else{
            currentVertex.uv = nullptr;
        }
        vertexInfos.push_back(currentVertex);
    }
    meshInfo.vertices = vertexInfos;

    if (this->getMaterialBindingsPtr() != 0){
        std::string materialName(this->m_pkMesh->MaterialBindings->Material->Name);
        meshInfo.materialName = materialName;
    }

    //int numTriangleGroups = this->getPrimaryTopologyPtr()->GroupCount;

    //int group0MaterialIndex = this->m_pkMesh->PrimaryTopology->Groups->MaterialIndex;
    //int group0TriFirst = this->m_pkMesh->PrimaryTopology->Groups->TriFirst;

    int group0TriCount = this->m_pkMesh->PrimaryTopology->Groups->TriCount;

    int numIndices16 = this->getNumIndices16();
    int numIndices = this->getNumIndices();

    std::vector<int> indices;
    if (numIndices16 > 0){
        for (int i = 0; i < numIndices16; i++){
            indices.push_back(this->m_pkMesh->PrimaryTopology->Indices16[i]);
        }
    }
    else if (numIndices > 0){
        for (int i = 0; i < numIndices; i++){
            indices.push_back(this->m_pkMesh->PrimaryTopology->Indices[i]);
        }
    }

    if (indices.size() > 0){
        std::vector<int *> triangles;
        for (int iTriangle = 0; iTriangle < group0TriCount; iTriangle++){
            int index0 = iTriangle * 3;
            int index1 = index0 + 1;
            int index2 = index0 + 2;

            newIndicator++;
            int * triangle = new int[3];
            triangle[0] = indices[index0];
            triangle[1] = indices[index1];
            triangle[2] = indices[index2];

            triangles.push_back(triangle);
        }
        meshInfo.triangles = triangles;
    }
    return meshInfo;
}
void GrannyMeshWrapper::writeMeshInfo(GrannyMeshInfo * meshInfo, bool isLeaderFormat, bool isSceneFormat){
    meshInfo->setVertexStructInfos(this->getVertexStructInfos());
    this->setName(meshInfo->name);
    this->writeVertices(meshInfo, isLeaderFormat, isSceneFormat);
    this->writeTriangles(&meshInfo->triangles, isLeaderFormat || isSceneFormat);
    this->writeBoneBindings(&meshInfo->boneBindings);
}
void GrannyMeshWrapper::writeVertices(GrannyMeshInfo * meshInfo, bool isLeaderFormat, bool isSceneFormat){
    int vertexCount = meshInfo->vertices.size();
    this->setNumVertices(vertexCount);

    int vertexSize = meshInfo->bytesPerVertex;

    granny_uint8 * oldVerticesPtr = this->getVerticesPtr();
    this->m_pkMesh->PrimaryVertexData->Vertices = (granny_uint8 *)malloc(vertexCount * vertexSize);
    granny_uint8 * newVerticesPtr = this->getVerticesPtr();

    std::vector<GrannyVertexInfo> vertexInfos(meshInfo->vertices.begin(), meshInfo->vertices.end());
    for (int i = 0; i < vertexCount; i++){
        memcpy((void*)(newVerticesPtr + i * vertexSize), (void*)oldVerticesPtr, vertexSize);

        GrannyVertexInfo currentVertex = vertexInfos[i];

        GrannyMeshVertexStructInfo * structInfo = meshInfo->getVertexStructInfoByName("Position");
        if (structInfo != nullptr){
            *(float*)(newVerticesPtr + (i * vertexSize) + structInfo->offset) = currentVertex.position[0];
            *(float*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1)) = currentVertex.position[1];
            *(float*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 2)) = currentVertex.position[2];
        }

        if (!isSceneFormat) {
            structInfo = meshInfo->getVertexStructInfoByName("BoneWeights");
            if (structInfo != nullptr){
                *(unsigned char *)(newVerticesPtr + (i * vertexSize) + structInfo->offset ) = (unsigned char)currentVertex.boneWeights[0];
                *(unsigned char *)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1)) = (unsigned char)currentVertex.boneWeights[1];
                *(unsigned char *)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 2)) = (unsigned char)currentVertex.boneWeights[2];
                *(unsigned char *)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 3)) = (unsigned char)currentVertex.boneWeights[3];
            }

            structInfo = meshInfo->getVertexStructInfoByName("BoneIndices");
            if (structInfo != nullptr){
                *(unsigned char*)(newVerticesPtr + (i * vertexSize) + structInfo->offset ) = (unsigned char)currentVertex.boneIndices[0];
                *(unsigned char*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1)) = (unsigned char)currentVertex.boneIndices[1];
                *(unsigned char*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 2)) = (unsigned char)currentVertex.boneIndices[2];
                *(unsigned char*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 3)) = (unsigned char)currentVertex.boneIndices[3];
            }
        }

        if (isLeaderFormat || isSceneFormat){
            structInfo = meshInfo->getVertexStructInfoByName("Normal");
            if (structInfo != nullptr){
                // granny_real16
                *(unsigned short*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 0)) = NumberUtils::floatToHalf(currentVertex.normal[0]);
                *(unsigned short*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1)) = NumberUtils::floatToHalf(currentVertex.normal[1]);
                *(unsigned short*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 2)) = NumberUtils::floatToHalf(currentVertex.normal[2]);
            }

            structInfo = meshInfo->getVertexStructInfoByName("Binormal");
            if ((structInfo != nullptr) && (currentVertex.binormal != nullptr)){
                // granny_real16
                *(unsigned short*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 0)) = NumberUtils::floatToHalf(currentVertex.binormal[0]);
                *(unsigned short*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1)) = NumberUtils::floatToHalf(currentVertex.binormal[1]);
                *(unsigned short*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 2)) = NumberUtils::floatToHalf(currentVertex.binormal[2]);
            }

            structInfo = meshInfo->getVertexStructInfoByName("Tangent");
            if (structInfo != nullptr && currentVertex.tangent != nullptr){
                // granny_real16
                *(unsigned short*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 0)) = NumberUtils::floatToHalf(currentVertex.tangent[0]);
                *(unsigned short*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1)) = NumberUtils::floatToHalf(currentVertex.tangent[1]);
                *(unsigned short*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 2)) = NumberUtils::floatToHalf(currentVertex.tangent[2]);
            }
        }
        else{
            structInfo = meshInfo->getVertexStructInfoByName("Normal");
            if (structInfo != nullptr){
                // granny_real32
                *(float*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 0)) = currentVertex.normal[0];
                *(float*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1)) = currentVertex.normal[1];
                *(float*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 2)) = currentVertex.normal[2];
            }
        }

        if (isLeaderFormat || isSceneFormat){
            structInfo = meshInfo->getVertexStructInfoByName("TextureCoordinates0");
            if (structInfo != nullptr){
                // granny_real16
                *(unsigned short*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 0)) = NumberUtils::floatToHalf(currentVertex.uv[0]);
                *(unsigned short*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1)) = NumberUtils::floatToHalf(currentVertex.uv[1]);
            }
        }
        else{
            structInfo = meshInfo->getVertexStructInfoByName("TextureCoordinates0");
            if (structInfo != nullptr){
                // granny_real32
                *(float*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 0)) = currentVertex.uv[0];
                *(float*)(newVerticesPtr + (i * vertexSize) + structInfo->offset + (structInfo->length * 1)) = currentVertex.uv[1];
            }
        }
    }
}
void GrannyMeshWrapper::writeTriangles(std::vector<int *> * triangles, bool isLeaderOrSceneFormat){
    int group0TriCount = (*triangles).size();
    this->setGroup0TriCount(group0TriCount);

    if (isLeaderOrSceneFormat){
        // Assumes using 2 byte indices (Indices16 - not Indices)
        int indexSize = 2;
        int numIndicies = group0TriCount * 3;
        this->setNumIndices16(numIndicies);

        granny_uint16 * oldIndicesPtr = this->getIndices16Ptr();
        this->m_pkMesh->PrimaryTopology->Indices16 = (granny_uint16 *)malloc(numIndicies * indexSize);
        granny_uint16 * newIndicesPtr = this->getIndices16Ptr();

        for (int iTriangle = 0; iTriangle < group0TriCount; iTriangle++)
        {
            int index0 = iTriangle * 3;
            int index1 = index0 + 1;
            int index2 = index0 + 2;

            memcpy((void*)(newIndicesPtr + index0 * indexSize), (void*)oldIndicesPtr, indexSize * 3);

            int * triangle = (*triangles)[iTriangle];
            *(unsigned short*)(newIndicesPtr + index0 * indexSize) = (unsigned short)triangle[0];
            *(unsigned short*)(newIndicesPtr + index1 * indexSize) = (unsigned short)triangle[1];
            *(unsigned short*)(newIndicesPtr + index2 * indexSize) = (unsigned short)triangle[2];
        }
    }
    else
    {
        // Assumes using 4 byte indices (Indices - not Indices16)
        int indexSize = 4;
        int numIndicies = group0TriCount * 3;
        this->setNumIndices(numIndicies);

        granny_int32 * oldIndicesPtr = this->getIndicesPtr();

        this->m_pkMesh->PrimaryTopology->Indices = ( granny_int32 *)malloc(numIndicies * indexSize);
        granny_int32 * newIndicesPtr = this->getIndicesPtr();

        for (int iTriangle = 0; iTriangle < group0TriCount; iTriangle++)
        {
            int index0 = iTriangle * 3;
            int index1 = index0 + 1;
            int index2 = index0 + 2;

            memcpy((void*)(newIndicesPtr + index0 * indexSize), (void*)oldIndicesPtr, indexSize * 3);

            int * triangle = (*triangles)[iTriangle];
            *(int*)(newIndicesPtr + index0 * indexSize) = triangle[0];
            *(int*)(newIndicesPtr + index1 * indexSize) = triangle[1];
            *(int*)(newIndicesPtr + index2 * indexSize) = triangle[2];
        }
    }
}
void GrannyMeshWrapper::writeBoneBindings(std::vector<std::string> * boneBindings){
    setNumBoneBindings(boneBindings->size());

    granny_bone_binding * oldBBPointer = this->getBoneBindingsPtr();
    // Blank out OBBMin/Max fields
    oldBBPointer->OBBMin[0] = 0.0f;
    oldBBPointer->OBBMin[1] = 0.0f;
    oldBBPointer->OBBMin[2] = 0.0f;

    oldBBPointer->OBBMax[0] = 0.0f;
    oldBBPointer->OBBMax[1] = 0.0f;
    oldBBPointer->OBBMax[2] = 0.0f;
    //*(float*)(oldBBPointer + 4) = 0.0f;
    //*(float*)(oldBBPointer + 8) = 0.0f;
    //*(float*)(oldBBPointer + 12) = 0.0f;
    //*(float*)(oldBBPointer + 16) = 0.0f;
    //*(float*)(oldBBPointer + 20) = 0.0f;
    //*(float*)(oldBBPointer + 24) = 0.0f;

    this->m_pkMesh->BoneBindings = (granny_bone_binding *)malloc(boneBindings->size() * sizeof(granny_bone_binding));
    granny_bone_binding * newBBPointer = this->m_pkMesh->BoneBindings;
    int sizeVectorBoneBindings = boneBindings->size();
    for (int i = 0; i < sizeVectorBoneBindings; i++){
        memcpy(&newBBPointer[i], oldBBPointer, sizeof(granny_bone_binding)); //copy first old bone binding to ith slot
        newBBPointer[i].BoneName = (*boneBindings)[i].c_str();// overwrite bone binding string
    }
}
bool GrannyMeshWrapper::meshEqual(granny_mesh * otherMesh){
    if ((wrappedMesh->Name == otherMesh->Name) &&
        (wrappedMesh->PrimaryTopology->IndexCount == otherMesh->PrimaryTopology->IndexCount) &&
        (wrappedMesh->PrimaryVertexData->VertexCount == otherMesh->PrimaryVertexData->VertexCount) &&
        (wrappedMesh->BoneBindingCount == otherMesh->BoneBindingCount) &&
        (wrappedMesh->MaterialBindingCount == otherMesh->MaterialBindingCount))
    {
        return true;
    }
    else
    {
        return false;
    }
}
std::vector<GrannyMeshVertexStructInfo> GrannyMeshWrapper::getVertexStructInfos(){
    granny_vertex_data * vertexInfoPtr = this->getPrimaryVertexDataPtr();

    std::vector<GrannyMeshVertexStructInfo> vertexStructInfos;
    int z = 0;
    while (true)
    {
        int type = vertexInfoPtr->VertexType[z].Type;
        if (type != 0)
        {
            std::string name(vertexInfoPtr->VertexType[z].Name);
            int count = vertexInfoPtr->VertexType[z].ArrayWidth;
            GrannyMeshVertexStructInfo info(type, name, count);
            vertexStructInfos.push_back(info);
            z++;
        }
        else
        {
            break;
        }
    }
    return vertexStructInfos;
}
