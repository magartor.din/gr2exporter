#ifndef GRANNYBONEWRAPPER_H
#define GRANNYBONEWRAPPER_H

#include <unistd.h>
#include <string>
#include "granny.h"

class GrannyBoneWrapper
{
    public:
        GrannyBoneWrapper(granny_bone * inputBone);
        virtual ~GrannyBoneWrapper();

        granny_bone * wrappedBone;

        std::string getName();
        void setName(std::string name);
        float * getPosition();
        float * getOrientation();
        void setScaleShear(float * scaleShear);
        float ** getScaleShear();
        float ** getInverseWorldMatrixPtr();
        float ** getInverseWorldMatrix();
    protected:

    private:
};
#endif // GRANNYBONEWRAPPER_H
