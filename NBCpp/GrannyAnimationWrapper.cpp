#include "GrannyAnimationWrapper.h"

GrannyAnimationWrapper::GrannyAnimationWrapper(granny_animation * inputAnimation)
{
    this->wrappedAnimation = inputAnimation;
    for(int indexTrackGroup= 0; indexTrackGroup < inputAnimation->TrackGroupCount; indexTrackGroup++){
        this->m_lstTrackGroups.push_back(inputAnimation->TrackGroups[indexTrackGroup]);
    }
    this->m_pkAnimation = nullptr;
    //ctor
}
GrannyAnimationWrapper::~GrannyAnimationWrapper()
{
    //dtor
}

std::vector< granny_track_group * > * GrannyAnimationWrapper::getTrackGroups(){
    return &m_lstTrackGroups;
}

/*C# definition

        private granny_animation* m_pkAnimation = (granny_animation*)0;
        private List<IGrannyTrackGroup> m_lstTrackGroups;
        private IGrannyAnimation wrappedAnimation;

        public GrannyAnimationWrapper(IGrannyAnimation inputAnimation)
        {
            wrappedAnimation = inputAnimation;
            Type myType = inputAnimation.GetType();
            FieldInfo fm_lstTrackGroups = myType.GetField("m_lstTrackGroups", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            m_lstTrackGroups = (List<IGrannyTrackGroup>)fm_lstTrackGroups.GetValue(inputAnimation);
        }

        public List<IGrannyTrackGroup> getTrackGroups()
        {
            return m_lstTrackGroups;
        }

*/
