#include "GrannyFileWrapper.h"

extern int newIndicator;

GrannyFileWrapper::GrannyFileWrapper(granny_file * inputFile){
    wrappedFile = inputFile;
    m_info = GrannyGetFileInfo(inputFile);;
    //ctor
}

GrannyFileWrapper::~GrannyFileWrapper(){
    //dtor
}

granny_art_tool_info * GrannyFileWrapper::getArtToolInfoPtr(){
    return this->m_info->ArtToolInfo;
}
void GrannyFileWrapper::setFromArtToolInfo(std::string toolName, int majorNum, int minorNum){
    this->setFromArtToolName(toolName);
    this->setArtToolRevisionNumbers(majorNum, minorNum);
}
std::string GrannyFileWrapper::getArtToolInfoName(){
    std::string artToolInfoName(this->m_info->ArtToolInfo->FromArtToolName);
    return artToolInfoName;
}
void GrannyFileWrapper::setFromArtToolName(std::string meshName){
    this->m_info->ArtToolInfo->FromArtToolName = meshName.c_str();
}
void GrannyFileWrapper::setArtToolRevisionNumbers(int majorNum, int minorNum){
    this->m_info->ArtToolInfo->ArtToolMajorRevision = majorNum;
    this->m_info->ArtToolInfo->ArtToolMinorRevision = minorNum;
}
void GrannyFileWrapper::setUnitsPerMeter(float unitsPerMeter){
    this->m_info->ArtToolInfo->UnitsPerMeter = unitsPerMeter;
}
void GrannyFileWrapper::setOrigin(float * origin){
    this->m_info->ArtToolInfo->Origin[0] = origin[0];
    this->m_info->ArtToolInfo->Origin[1] = origin[1];
    this->m_info->ArtToolInfo->Origin[2] = origin[2];
}
void GrannyFileWrapper::setMatrix(float * matrix){
    this->m_info->ArtToolInfo->RightVector[0] = matrix[0];
    this->m_info->ArtToolInfo->RightVector[1] = matrix[1];
    this->m_info->ArtToolInfo->RightVector[2] = matrix[2];

    this->m_info->ArtToolInfo->UpVector[0] = matrix[3];
    this->m_info->ArtToolInfo->UpVector[1] = matrix[4];
    this->m_info->ArtToolInfo->UpVector[2] = matrix[5];

    this->m_info->ArtToolInfo->BackVector[0] = matrix[6];
    this->m_info->ArtToolInfo->BackVector[1] = matrix[7];
    this->m_info->ArtToolInfo->BackVector[2] = matrix[8];
}
granny_exporter_info * GrannyFileWrapper::getExporterInfoPtr(){
    return this->m_info->ExporterInfo;
}
void GrannyFileWrapper::setExporterInfo(std::string name, int majorNum, int minorNum, int customization, int build){
    this->setExporterName(name);
    this->setExporterNumbers(majorNum, minorNum, customization, build);
}
std::string GrannyFileWrapper::getExporterName(){
    std::string exporterName(this->m_info->ExporterInfo->ExporterName);
    return exporterName;
}
void GrannyFileWrapper::setExporterName(std::string meshName){
    newIndicator++;
    char * strPtr = (char *)malloc((meshName.size()+1)*sizeof(char));
    std::memcpy(strPtr, meshName.c_str(), meshName.size());
    this->m_info->ExporterInfo->ExporterName = strPtr;
}
void GrannyFileWrapper::setExporterNumbers(int majorNum, int minorNum, int customization, int build){
    this->m_info->ExporterInfo->ExporterMajorRevision = majorNum;
    this->m_info->ExporterInfo->ExporterMinorRevision = minorNum;
    this->m_info->ExporterInfo->ExporterCustomization = customization;
    this->m_info->ExporterInfo->ExporterBuildNumber = build;
}
void GrannyFileWrapper::setFromFileName(std::string meshName){
    this->m_info->FromFileName = meshName.c_str();
}
int GrannyFileWrapper::getNumMeshes(){
    return this->m_info->MeshCount;
}
void GrannyFileWrapper::setNumMeshes(int numMeshes){
    this->m_info->MeshCount = numMeshes;
}
granny_mesh ** GrannyFileWrapper::getMeshesPtr(){
    return this->m_info->Meshes;
}
int GrannyFileWrapper::getNumMaterials(){
    return this->m_info->MaterialCount;
}
void GrannyFileWrapper::setNumMaterials(int num){
    this->m_info->MaterialCount = num;
}
granny_material ** GrannyFileWrapper::getMaterialsPtr(){
    return this->m_info->Materials;
}
int GrannyFileWrapper::getNumModels(){
    return this->m_info->ModelCount;
}
void GrannyFileWrapper::setNumModels(int num){
    this->m_info->ModelCount = num;
}
granny_model ** GrannyFileWrapper::getModelsPtr(){
     return this->m_info->Models;
}
int GrannyFileWrapper::getNumTrackGroups(){
    return this->m_info->TrackGroupCount;
}
void GrannyFileWrapper::setNumTrackGroups(int num){
    this->m_info->TrackGroupCount = num;
}
granny_track_group ** GrannyFileWrapper::getTrackGroupsPtr(){
    return this->m_info->TrackGroups;
}
void GrannyFileWrapper::setTrackGroupsPtr(granny_track_group ** ptr){
    this->m_info->TrackGroups = ptr;
}
int GrannyFileWrapper::getNumAnimations(){
    return this->m_info->AnimationCount;
}
void GrannyFileWrapper::setNumAnimations(int num){
    this->m_info->AnimationCount = num;
}
granny_animation ** GrannyFileWrapper::getAnimationsPtr(){
    return this->m_info->Animations;
}
void GrannyFileWrapper::setAnimationsPtr(granny_animation ** ptr){
    this->m_info->Animations = ptr;
}
int GrannyFileWrapper::getNumSkeletons(){
    return this->m_info->SkeletonCount;
}
void GrannyFileWrapper::setNumSkeletons(int num){
    this->m_info->SkeletonCount = num;
}
granny_skeleton ** GrannyFileWrapper::getSkeletonsPtr(){
    return this->m_info->Skeletons;
}
int GrannyFileWrapper::getNumVertexDatas(){
    return this->m_info->VertexDataCount;
}
void GrannyFileWrapper::setNumVertexDatas(int num){
    this->m_info->VertexDataCount = num;
}
granny_vertex_data ** GrannyFileWrapper::getVertexDatasPtr(){
    return this->m_info->VertexDatas;
}
int GrannyFileWrapper::getNumTriTopologies(){
    return this->m_info->TriTopologyCount;
}
void GrannyFileWrapper::setNumTriTopologies(int num){
    this->m_info->TriTopologyCount = num;
}
granny_tri_topology ** GrannyFileWrapper::getTriTopologiesPtr(){
    return this->m_info->TriTopologies;
}
void GrannyFileWrapper::addSkeletonPointer(granny_skeleton * skeletonPtr){
    //Calc tab sizes
    int oldNumSkeletons = this->getNumSkeletons();
    int newNumSkeletons = oldNumSkeletons + 1;
    //Get previous pointer
    granny_skeleton ** oldSkeletonsPtr = this->getSkeletonsPtr();
    //Alloc new pointer table
    newIndicator++;
    this->m_info->Skeletons = (granny_skeleton **)malloc(newNumSkeletons * sizeof(granny_skeleton *));
    //Copy all skeleton
    memcpy((void*)this->m_info->Skeletons, (void*)oldSkeletonsPtr,  oldNumSkeletons * sizeof(granny_skeleton *));
    //Add new skeleton to the end
    this->m_info->Skeletons[oldNumSkeletons] = skeletonPtr;
    //Set new skeleton number
    setNumSkeletons(newNumSkeletons);
}
void GrannyFileWrapper::addVertexDatasPointer(granny_vertex_data * VertexPtr){
    //Calc tab sizes
    int oldNumVertexDatas = getNumVertexDatas();
    int newNumVertexDatas = oldNumVertexDatas + 1;
    //Get previous pointer
    granny_vertex_data ** oldVertexDatasPtr = this->getVertexDatasPtr();
    //Alloc new pointer table
    newIndicator++;
    this->m_info->VertexDatas = (granny_vertex_data **)malloc(newNumVertexDatas * sizeof(granny_vertex_data *));
    //copy all vertex data pointer
    memcpy((void*)this->m_info->VertexDatas, (void*)oldVertexDatasPtr, oldNumVertexDatas * sizeof(granny_vertex_data *));
    //Add new vertex data pointer to the end
    this->m_info->VertexDatas[oldNumVertexDatas] = VertexPtr;
    //Set new num vertex data
    setNumVertexDatas(newNumVertexDatas);
}
void GrannyFileWrapper::addTriTopologiesPointer(granny_tri_topology * TriTopoPtr){
    int oldNumTriTopologies = getNumTriTopologies();
    int newNumTriTopologies = oldNumTriTopologies + 1;

    granny_tri_topology ** oldTriTopoPtr = getTriTopologiesPtr();

    newIndicator++;
    this->m_info->TriTopologies = (granny_tri_topology **)malloc(newNumTriTopologies * sizeof(granny_tri_topology *));

    memcpy((void*)this->m_info->TriTopologies, (void*)oldTriTopoPtr, oldNumTriTopologies * sizeof(granny_tri_topology *));

    this->m_info->TriTopologies[oldNumTriTopologies] = TriTopoPtr;

    setNumTriTopologies(newNumTriTopologies);
}
void GrannyFileWrapper::addTrackGroupsPointer(granny_track_group * trackGroupPtr){
    int oldNumTrackGroups = getNumTrackGroups();
    int newNumTrackGroups = oldNumTrackGroups + 1;

    granny_track_group ** oldTriTopoPtr = getTrackGroupsPtr();

    newIndicator++;
    this->m_info->TrackGroups = (granny_track_group **)malloc(newNumTrackGroups * sizeof(granny_track_group *));

    memcpy((void*)this->m_info->TrackGroups, (void*)oldTriTopoPtr, oldNumTrackGroups * sizeof(granny_track_group *));

    this->m_info->TrackGroups[oldNumTrackGroups] = trackGroupPtr;

    setNumTriTopologies(newNumTrackGroups);
}
