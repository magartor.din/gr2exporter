#include "BiLookup.h"

template <class TFirst, class TSecond>
BiLookup<TFirst, TSecond>::BiLookup()
{
    //ctor
}
template <>
BiLookup<int, std::string>::BiLookup()
{
    //ctor
}

template <class TFirst, class TSecond>
BiLookup<TFirst, TSecond>::~BiLookup()
{
    //dtor
}


template <class TFirst, class TSecond>
void BiLookup<TFirst, TSecond>::Add(TFirst first, TSecond second){
    std::vector<TFirst> firsts;
    std::vector<TSecond> seconds;

    if(!(this->firstToSecond.count(first) > 0)){
        this->firstToSecond[first] = seconds;
    }
    else{
        this->firstToSecond[first].push_back(second);
    }
    if(!(this->secondToFirst.count(second) > 0)){
        this->secondToFirst[second] = firsts;
    }
    else{
        this->secondToFirst[second].push_back(firsts);
    }
}

template <class TFirst, class TSecond>
std::vector<TSecond> BiLookup<TFirst, TSecond>::operator [](TFirst first){
    return this->GetByFirst(first);
}

template <class TFirst, class TSecond>
std::vector<TFirst> BiLookup<TFirst, TSecond>::operator [](TSecond second){
    return this->GetBySecond(second);
}

template <class TFirst, class TSecond>
std::vector<TSecond> BiLookup<TFirst, TSecond>::GetByFirst(TFirst first){
    if(this->firstToSecond.find(first) != this->firstToSecond.end()){
        std::vector<TSecond> liste(this->firstToSecond[first]);
        return liste;
    }
    return this->EmptySecondList;
}

template <class TFirst, class TSecond>
std::vector<TFirst> BiLookup<TFirst, TSecond>::GetBySecond(TSecond second){
    if(this->secondToFirst.find(second) != this->secondToFirst.end()){
        std::vector<TFirst> liste(this->secondToFirst[second]);
        return liste;
    }
    return this->EmptyFirstList;
}

template <>
void BiLookup<int, std::string> ::Add(int first, std::string second){
    std::vector<int> firsts;
    std::vector<std::string> seconds;

    if(this->firstToSecond.find(first) == firstToSecond.end()){
        //this->firstToSecond.insert(std::pair<TFirst, std::vector<TSecond>>(first, seconds));
        this->firstToSecond[first] = seconds;
        this->firstToSecond[first].push_back(second);
    }
    else{
        this->firstToSecond[first].push_back(second);
    }
    if(this->secondToFirst.find(second) == this->secondToFirst.end()){
        this->secondToFirst[second] = firsts;
        this->secondToFirst[second].push_back(first);
    }
    else{
        this->secondToFirst[second].push_back(first);
    }
}
template <>
std::vector<std::string> BiLookup<int, std::string>::GetByFirst(int first){
    if(this->firstToSecond.find(first) != this->firstToSecond.end()){
        std::vector<std::string> liste(this->firstToSecond[first]);
        return liste;
    }
    return this->EmptySecondList;
}
template <>
std::vector<std::string> BiLookup<int, std::string>::operator [](int first){
    return this->GetByFirst(first);
}
template <>
std::vector<int> BiLookup<int, std::string>::GetBySecond(std::string second){
    if(this->secondToFirst.find(second) != this->secondToFirst.end()){
        std::vector<int> liste(this->secondToFirst[second]);
        return liste;
    }
    return this->EmptyFirstList;
}
template <>
std::vector<int> BiLookup<int, std::string>::operator [](std::string second){
    return this->GetBySecond(second);
}
