#include "GrannyModelWrapper.h"

GrannyModelWrapper::GrannyModelWrapper(granny_model * inputModel)
{
    this->wrappedModel = inputModel;
    for(int i = 0;i < this->wrappedModel->MeshBindingCount; i++){
        this->m_lstMeshBindings.push_back(this->wrappedModel->MeshBindings[i]);
    }
    for(int i = 0; i < this->wrappedModel->Skeleton->BoneCount; i++){
        this->m_pkBoneNames.push_back(this->wrappedModel->Skeleton->Bones[i].Name);
    }
    this->m_pkSkeleton = this->wrappedModel->Skeleton;
    this->m_kInitialPlacement = this->wrappedModel->InitialPlacement;
    this->m_pkModel = inputModel;
    //ctor
}

GrannyModelWrapper::~GrannyModelWrapper()
{
    //dtor
}

char const * GrannyModelWrapper::getNamePtr(){
    return this->m_pkModel->Name;
}
granny_skeleton * GrannyModelWrapper::getSkeletonPtr(){
    return this->m_pkModel->Skeleton;
}
int GrannyModelWrapper::getNumMeshBindings(){
    return this->m_pkModel->MeshBindingCount;
}
void GrannyModelWrapper::setNumMeshBindings(int num){
    this->m_pkModel->MeshBindingCount = num;
}
granny_model_mesh_binding * GrannyModelWrapper::getMeshBindingsPtr(){
    return this->m_pkModel->MeshBindings;
}
void GrannyModelWrapper::setName(std::string meshName){
    this->m_pkModel->Name = meshName.c_str();
}
std::string GrannyModelWrapper::getName(){
    std::string res(this->m_pkModel->Name);
    return res;
}
