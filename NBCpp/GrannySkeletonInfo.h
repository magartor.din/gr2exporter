#ifndef GRANNYSKELETONINFO_H
#define GRANNYSKELETONINFO_H

#include <unistd.h>
#include <vector>
#include "granny.h"
#include "GrannyBoneInfo.h"

class GrannySkeletonInfo
{
    public:
        GrannySkeletonInfo();
        std::vector<GrannyBoneInfo> bones;
    protected:

    private:
};

#endif // GRANNYSKELETONINFO_H
