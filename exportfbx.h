#ifndef EXPORTFBX_H
#define EXPORTFBX_H

#include <QObject>

class ExportFBX : public QObject
{
    Q_OBJECT
public:
    explicit ExportFBX(QObject *parent = nullptr);

    ~ExportFBX();


public slots:


private slots:


signals:

};

#endif // EXPORTFBX_H
