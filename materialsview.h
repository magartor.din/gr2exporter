#ifndef MATERIALSVIEW_H
#define MATERIALSVIEW_H

#include <QWidget>
#include <exception>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QFile>
#include <QFileInfo>

#include "NBCpp/granny.h"

namespace Ui {
class MaterialsView;
}

class MaterialsView : public QWidget
{
    Q_OBJECT

public:
    explicit MaterialsView(QWidget *parent = nullptr);
    ~MaterialsView();
public slots:
    void addMaterials(granny_int32 MaterialCount, granny_material ** Materials);
    void addMaterialsToTable(granny_int32 MaterialCount, granny_material ** Materials);
    void addMaterialsToTree(granny_int32 MaterialCount, granny_material ** Materials);

    void removeMaterial(void);

    void loadMaterial(int key);

    void loadMaterialMaps(granny_material *Materials);
    void loadMaterialTextures(granny_material *Materials);

    void loadMapSelected(granny_material_map *Materials);
    void loadMapSelectedUsage(granny_material_map *Materials);
    void loadMapSelectedMaterialName(granny_material_map *Materials);

    void resetTextureSelected(void);
    void resetTextureSelectedFileName(void);
    void resetTextureSelectedType(void);
    void resetTextureSelectedWidth(void);
    void resetTextureSelectedHeight(void);
    void resetTextureSelectedEncoding(void);
    void resetTextureSelectedSubFormat(void);
    void resetTextureSelectedLayout(void);
    void resetTextureSelectedLayoutBytePerPixel(void);
    void resetTextureSelectedLayoutShiftForCompo(void);
    void resetTextureSelectedLayoutBitsForCompo(void);
    void resetTextureSelectedImageCount(void);

    void loadTextureSelected(granny_texture * texture);
    void loadTextureSelectedFileName(granny_texture * texture);
    void loadTextureSelectedType(granny_texture * texture);
    void loadTextureSelectedWidth(granny_texture * texture);
    void loadTextureSelectedHeight(granny_texture * texture);
    void loadTextureSelectedEncoding(granny_texture * texture);
    void loadTextureSelectedSubFormat(granny_texture * texture);
    void loadTextureSelectedLayout(granny_texture * texture);
    void loadTextureSelectedLayoutBytePerPixel(granny_texture * texture);
    void loadTextureSelectedLayoutShiftForCompo(granny_texture * texture);
    void loadTextureSelectedLayoutBitsForCompo(granny_texture * texture);
    void loadTextureSelectedImageCount(granny_texture * texture);

    static QTreeWidgetItem *addElementOnTree(granny_material * Materials);
    static void buildTextureItem(granny_texture * texture, QTreeWidgetItem *rootItem);
    static void buildExtendDataItem(granny_variant * variant, QTreeWidgetItem *rootItem);

    static QString unreferenceObj(void ** ptr, granny_data_type_definition *variantType);
private slots:
    void on_tableWidget_materials_cellClicked(int row, int column);

    void on_tableWidget_materials_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

private:
    Ui::MaterialsView *ui;
    QString log;
    std::map<int, granny_material *> mapListMaterial;
    int materialSelected;
};

#endif // MATERIALSVIEW_H
