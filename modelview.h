#ifndef MODELVIEW_H
#define MODELVIEW_H

#include <QWidget>
#include "matrix3x3.h"
#include "NBCpp/granny.h"

namespace Ui {
class ModelView;
}

class ModelView : public QWidget
{
    Q_OBJECT

public:
    explicit ModelView(QWidget *parent = nullptr);
    ~ModelView();
public slots:
    void addModels(granny_int32 ModelCount, granny_model ** Models);

    void resetModels(void);
    void resetInitPlacment(void);
    void resetMeshBinded(void);
    void resetSkeleton(void);

private slots:
    void on_tableWidget_model_cellClicked(int row, int column);

    void loadModel(int key);
    void loadModelName(granny_model * model);
    void loadModelSkeleton(granny_model * model);

    void loadInitPlacment(granny_model * model);
    void loadInitPlacmentFlags(granny_model * model);
    void loadInitPlacmentPos(granny_model * model);
    void loadInitPlacmentOri(granny_model * model);
    void loadInitPlacmentScale(granny_model * model);

    void loadMeshBinded(granny_model * model);

    void loadSkeletonSelected(granny_skeleton *skeleton);
    void loadSkeletonSelectedName(granny_skeleton *skeleton);
    void loadSkeletonSelectedBoneCount(granny_skeleton *skeleton);
    void loadSkeletonSelectedLODType(granny_skeleton *skeleton);

    void loadMeshBindedSelected(granny_model_mesh_binding * model);
    void loadMeshBindedSelectedName(granny_model_mesh_binding * model);
    void loadMeshBindedSelectedMorphCount(granny_model_mesh_binding * model);
    void loadMeshBindedSelectedMaterialBindedCount(granny_model_mesh_binding * model);
    void loadMeshBindedSelectedBoneBindedCount(granny_model_mesh_binding * model);
    void on_comboBox_modelMeshSelected_currentIndexChanged(int index);

    void on_comboBox_modelMeshSelected_activated(int index);

    void on_tableWidget_model_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

private:
    QString log;
    Ui::ModelView *ui;
    Matrix3x3 * scaleMatrix;
    std::map<int, granny_model *> mapListModel;
    int modelSelected;
};

#endif // MODELVIEW_H
