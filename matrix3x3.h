#ifndef MATRIX3X3_H
#define MATRIX3X3_H

#include <QWidget>
#include <QLabel>
#include "NBCpp/granny.h"

namespace Ui {
class Matrix3x3;
}

class Matrix3x3 : public QWidget
{
    Q_OBJECT

public:
    explicit Matrix3x3(QWidget *parent = nullptr);
    ~Matrix3x3();
    template<typename T>
    void setValues(T vals);
    void reset(void);

private:
    Ui::Matrix3x3 *ui;
    QString log;
    QLabel * matLabel[3][3];
};

#endif // MATRIX3X3_H
