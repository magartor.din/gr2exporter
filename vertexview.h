#ifndef VERTEXVIEW_H
#define VERTEXVIEW_H

#include <QWidget>

#include "extendeddataview.h"
#include "NBCpp/granny.h"

namespace Ui {
class VertexView;
}

class VertexView : public QWidget
{
    Q_OBJECT

public:
    explicit VertexView(QWidget *parent = nullptr);
    ~VertexView();
public slots:
    void addVertex(granny_int32 VertexDataCount, granny_vertex_data ** VertexDatas);

    void resetVertex(void);

    void loadVertexSelected(granny_vertex_data * VertexDatas);
    void loadVertexSelectedTypeName(granny_vertex_data * VertexDatas);
    void loadVertexSelectedVertices(granny_vertex_data * VertexDatas);
    void loadVertexSelectedComponent(granny_vertex_data * VertexDatas);
    void loadVertexSelectedAnnotation(granny_vertex_data * VertexDatas);

private slots:
    void on_tableWidget_vertex_cellClicked(int row, int column);

    void on_tableWidget_vertex_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

private:
    Ui::VertexView *ui;
    QString log;

    std::map<int, granny_vertex_data * > mapListVertex;

    int vertexSelected;
};

#endif // VERTEXVIEW_H
