#ifndef MATRIX4X4_H
#define MATRIX4X4_H

#include <QWidget>
#include <QLabel>
#include <QString>

#include "NBCpp/granny.h"

namespace Ui {
class Matrix4x4;
}

class Matrix4x4 : public QWidget
{
    Q_OBJECT

public:
    explicit Matrix4x4(QWidget *parent = nullptr);
    ~Matrix4x4();
    template<typename T>
    void setValues(T vals);
    void reset(void);

private:
    Ui::Matrix4x4 *ui;
    QLabel * matLabel[4][4];
    QString log;
};

#endif // MATRIX4X4_H
