#ifndef ARTTOOLVIEWER_H
#define ARTTOOLVIEWER_H

#include <QWidget>
#include <QTabWidget>

#include "NBCpp/granny.h"

namespace Ui {
class ArtToolViewer;
}

class ArtToolViewer : public QWidget
{
    Q_OBJECT

public:
    explicit ArtToolViewer(QWidget *parent = nullptr);
    ~ArtToolViewer();
public slots:
    void addArtTool(granny_art_tool_info * artTool);

    void resetArtTool(void);

    void loadArtToolSelected(granny_art_tool_info * artTool);
    void loadArtToolSelectedName(granny_art_tool_info * artTool);
    void loadArtToolSelectedMajorVersion(granny_art_tool_info * artTool);
    void loadArtToolSelectedMinorVersion(granny_art_tool_info * artTool);
    void loadArtToolSelectedPointerSize(granny_art_tool_info * artTool);
    void loadArtToolSelectedUnitPerMeter(granny_art_tool_info * artTool);
    void loadArtToolSelectedOrigin(granny_art_tool_info * artTool);
    void loadArtToolSelectedRightVector(granny_art_tool_info * artTool);
    void loadArtToolSelectedUpVector(granny_art_tool_info * artTool);
    void loadArtToolSelectedBackVector(granny_art_tool_info * artTool);

private slots:
    void on_tableWidget_arttool_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);
    void on_tableWidget_arttool_cellClicked(int row, int column);

private:
    Ui::ArtToolViewer *ui;

    QString log;

    std::map<int, granny_art_tool_info *> mapListArtTool;
    int artToolSelected;
};

#endif // ARTTOOLVIEWER_H
