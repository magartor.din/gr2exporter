#include "tritopologyview.h"
#include "ui_tritopologyview.h"

TritopologyView::TritopologyView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TritopologyView){
    ui->setupUi(this);
    ui->tableWidget_annotationSet->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    ui->tableWidget_materialgroup->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    TritopologySelected = -1;
}
TritopologyView::~TritopologyView(){
    delete ui;
}

void TritopologyView::addTritopology(granny_int32 TriTopologyCount, granny_tri_topology ** TriTopologies){
    //Verifie le pointeur vers le tableau de TriTopologies
    if(TriTopologies == nullptr){
        return;
    }
    //Variable iteration TriTopologies
    granny_int32 index;
    for(index = 0; index < TriTopologyCount;index++){
        //Pour tout les modèles du tableau
        try{
            if(TriTopologies[index] != nullptr){
                //Si le pointeur est valid
                try {
                    //stock le pointeur dans une variable locale
                    granny_tri_topology * curTriTopology = TriTopologies[index];
                    if(curTriTopology != nullptr){
                        //Si le pointeur du modèle est valide on recupère le nombre de ligne
                        int row = ui->tableWidget_tritopology->rowCount();
                        //on ajoute une ligne et on increment le nombre de ligne
                        ui->tableWidget_tritopology->insertRow(row);
                        QString triTopologyName;
                        //Recupère le nombre de colone
                        //int column = ui->tableWidget_tritopology->columnCount();
                        QTableWidgetItem *newItem = nullptr;
                        triTopologyName = "0x" + QString::number((int)curTriTopology, 16);
                        //Crée l'objet représentant le modèle dans la liste
                        newItem = new QTableWidgetItem(triTopologyName);
                        //Ajoute le modèle à la liste
                        ui->tableWidget_tritopology->setItem(row, 0, newItem);
                        mapListTritopology[row] = curTriTopology;
                    }
                }catch (...){
                    log += "Error accessing TriTopologies " + QString::number(index);
                }
            }
        }catch (...) {
            log += "Error accessing TriTopologies table" + QString::number(index);
        }
    }
}
void TritopologyView::resetTritopology(void){
    for(int i = ui->tableWidget_tritopology->rowCount(); i >= 0; i--){
        ui->tableWidget_tritopology->removeRow(i);
    }
    ui->tableWidget_tritopology->clear();
    ui->tableWidget_tritopology->setHorizontalHeaderItem(0,new QTableWidgetItem("Addr"));

    for(int i = ui->tableWidget_materialgroup->rowCount(); i >= 0; i--){
        ui->tableWidget_materialgroup->removeRow(i);
    }
    ui->tableWidget_materialgroup->clear();
    ui->tableWidget_materialgroup->setHorizontalHeaderItem(0,new QTableWidgetItem("Index"));
    ui->tableWidget_materialgroup->setHorizontalHeaderItem(1,new QTableWidgetItem("Tri first"));
    ui->tableWidget_materialgroup->setHorizontalHeaderItem(2,new QTableWidgetItem("Tri count"));
    ui->tableWidget_materialgroup->setHorizontalHeaderItem(3,new QTableWidgetItem("Material index"));

    for(int i = ui->tableWidget_index->rowCount(); i >= 0; i--){
        ui->tableWidget_index->removeRow(i);
    }
    ui->tableWidget_index->clear();
    ui->tableWidget_index->setHorizontalHeaderItem(0,new QTableWidgetItem("Indice"));

    for(int i = ui->tableWidget_vertexToTriangle->rowCount(); i >= 0; i--){
        ui->tableWidget_vertexToTriangle->removeRow(i);
    }
    ui->tableWidget_vertexToTriangle->clear();
    ui->tableWidget_vertexToTriangle->setHorizontalHeaderItem(0,new QTableWidgetItem("Triangle"));

    for(int i = ui->tableWidget_sideToNeighbor->rowCount(); i >= 0; i--){
        ui->tableWidget_sideToNeighbor->removeRow(i);
    }
    ui->tableWidget_sideToNeighbor->clear();
    ui->tableWidget_sideToNeighbor->setHorizontalHeaderItem(0,new QTableWidgetItem("Neighbor"));

    for(int i = ui->tableWidget_index16->rowCount(); i >= 0; i--){
        ui->tableWidget_index16->removeRow(i);
    }
    ui->tableWidget_index16->clear();
    ui->tableWidget_index16->setHorizontalHeaderItem(0,new QTableWidgetItem("Indice 16"));

    for(int i = ui->tableWidget_boneForTriangle->rowCount(); i >= 0; i--){
        ui->tableWidget_boneForTriangle->removeRow(i);
    }
    ui->tableWidget_boneForTriangle->clear();
    ui->tableWidget_boneForTriangle->setHorizontalHeaderItem(0,new QTableWidgetItem("Triangle"));

    for(int i = ui->tableWidget_vertexToVertex->rowCount(); i >= 0; i--){
        ui->tableWidget_vertexToVertex->removeRow(i);
    }
    ui->tableWidget_vertexToVertex->clear();
    ui->tableWidget_vertexToVertex->setHorizontalHeaderItem(0,new QTableWidgetItem("Vertex"));

    for(int i = ui->tableWidget_triangleToBone->rowCount(); i >= 0; i--){
        ui->tableWidget_triangleToBone->removeRow(i);
    }
    ui->tableWidget_triangleToBone->clear();
    ui->tableWidget_triangleToBone->setHorizontalHeaderItem(0,new QTableWidgetItem("Triangle"));

    for(int i = ui->tableWidget_annotationSet->rowCount(); i >= 0; i--){
        ui->tableWidget_annotationSet->removeRow(i);
    }
    ui->tableWidget_annotationSet->clear();
    ui->tableWidget_annotationSet->setHorizontalHeaderItem(0,new QTableWidgetItem("Name"));
    ui->tableWidget_annotationSet->setHorizontalHeaderItem(1,new QTableWidgetItem("Tri annotation count"));
    ui->tableWidget_annotationSet->setHorizontalHeaderItem(2,new QTableWidgetItem("Tri annotation index countt"));
    ui->tableWidget_annotationSet->setHorizontalHeaderItem(3,new QTableWidgetItem("Indice map"));

    mapListTritopology.clear();
    TritopologySelected = -1;
}
void TritopologyView::loadTritopologySelected(granny_tri_topology * TriTopologies){
    if(TriTopologies == nullptr){
        return;
    }
    try {
        loadTritopologySelectedMaterialGroup(TriTopologies);
        loadTritopologySelectedVertexToTri(TriTopologies);
        loadTritopologySelectedIndex(TriTopologies);
        loadTritopologySelectedSideToNeighbor(TriTopologies);
        loadTritopologySelectedIndex16(TriTopologies);
        loadTritopologySelectedBoneForTri(TriTopologies);
        loadTritopologySelectedVertexToVertex(TriTopologies);
        loadTritopologySelectedTriToBone(TriTopologies);
        loadTritopologySelectedAnnotationList(TriTopologies);
    } catch (...) {
        log += "Error accessing TriTopologies";
    }
}
void TritopologyView::loadTritopologySelectedMaterialGroup(granny_tri_topology * TriTopologies){
    if(TriTopologies == nullptr){
        return;
    }
    try {
        for(int i = ui->tableWidget_materialgroup->rowCount(); i >= 0; i--){
            ui->tableWidget_materialgroup->removeRow(i);
        }
        for(granny_int32 i = 0; i < TriTopologies->GroupCount; i++){
            int row = ui->tableWidget_materialgroup->rowCount();
            ui->tableWidget_materialgroup->insertRow(row);
            ui->tableWidget_materialgroup->setItem(row, 0,new QTableWidgetItem(QString::number(TriTopologies->Groups[i].MaterialIndex)));
            ui->tableWidget_materialgroup->setItem(row, 1,new QTableWidgetItem(QString::number(TriTopologies->Groups[i].TriFirst)));
            ui->tableWidget_materialgroup->setItem(row, 2,new QTableWidgetItem(QString::number(TriTopologies->Groups[i].TriCount)));

        }
        ui->tableWidget_materialgroup->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    } catch (...) {
        log += "Error accessing TriTopologies";
    }
}
void TritopologyView::loadTritopologySelectedVertexToTri(granny_tri_topology * TriTopologies){
    if(TriTopologies == nullptr){
        return;
    }
    try {
        for(int i = ui->tableWidget_vertexToTriangle->rowCount(); i >= 0; i--){
            ui->tableWidget_vertexToTriangle->removeRow(i);
        }
        for(granny_int32 i = 0; i < TriTopologies->VertexToTriangleCount; i++){
            int row = ui->tableWidget_vertexToTriangle->rowCount();
            ui->tableWidget_vertexToTriangle->insertRow(row);
            ui->tableWidget_vertexToTriangle->setItem(row, 0,new QTableWidgetItem(QString::number(TriTopologies->VertexToTriangleMap[i])));
        }
        ui->tableWidget_vertexToTriangle->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    } catch (...) {
        log += "Error accessing TriTopologies";
    }
}
void TritopologyView::loadTritopologySelectedIndex(granny_tri_topology * TriTopologies){
    if(TriTopologies == nullptr){
        return;
    }
    try {
        for(int i = ui->tableWidget_index->rowCount(); i >= 0; i--){
            ui->tableWidget_index->removeRow(i);
        }
        for(granny_int32 i = 0; i < TriTopologies->IndexCount; i++){
            int row = ui->tableWidget_index->rowCount();
            ui->tableWidget_index->insertRow(row);
            ui->tableWidget_index->setItem(row, 0,new QTableWidgetItem(QString::number(TriTopologies->Indices[i])));
        }
        ui->tableWidget_index->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    } catch (...) {
        log += "Error accessing TriTopologies";
    }
}
void TritopologyView::loadTritopologySelectedSideToNeighbor(granny_tri_topology * TriTopologies){
    if(TriTopologies == nullptr){
        return;
    }
    try {
        for(int i = ui->tableWidget_sideToNeighbor->rowCount(); i >= 0; i--){
            ui->tableWidget_sideToNeighbor->removeRow(i);
        }
        for(granny_int32 i = 0; i < TriTopologies->SideToNeighborCount; i++){
            int row = ui->tableWidget_sideToNeighbor->rowCount();
            ui->tableWidget_sideToNeighbor->insertRow(row);
            ui->tableWidget_sideToNeighbor->setItem(row, 0,new QTableWidgetItem(QString::number(TriTopologies->SideToNeighborMap[i])));
        }
        ui->tableWidget_sideToNeighbor->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    } catch (...) {
        log += "Error accessing TriTopologies";
    }
}
void TritopologyView::loadTritopologySelectedIndex16(granny_tri_topology * TriTopologies){
    if(TriTopologies == nullptr){
        return;
    }
    try {
        for(int i = ui->tableWidget_index16->rowCount(); i >= 0; i--){
            ui->tableWidget_index16->removeRow(i);
        }
        for(granny_int32 i = 0; i < TriTopologies->Index16Count; i++){
            int row = ui->tableWidget_index16->rowCount();
            ui->tableWidget_index16->insertRow(row);
            ui->tableWidget_index16->setItem(row, 0,new QTableWidgetItem(QString::number(TriTopologies->Indices16[i])));
        }
        ui->tableWidget_index16->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    } catch (...) {
        log += "Error accessing TriTopologies";
    }
}
void TritopologyView::loadTritopologySelectedBoneForTri(granny_tri_topology * TriTopologies){
    if(TriTopologies == nullptr){
        return;
    }
    try {
        for(int i = ui->tableWidget_boneForTriangle->rowCount(); i >= 0; i--){
            ui->tableWidget_boneForTriangle->removeRow(i);
        }
        for(granny_int32 i = 0; i < TriTopologies->BonesForTriangleCount; i++){
            int row = ui->tableWidget_boneForTriangle->rowCount();
            ui->tableWidget_boneForTriangle->insertRow(row);
            ui->tableWidget_boneForTriangle->setItem(row, 0,new QTableWidgetItem(QString::number(TriTopologies->BonesForTriangle[i])));
        }
        ui->tableWidget_boneForTriangle->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    } catch (...) {
        log += "Error accessing TriTopologies";
    }
}
void TritopologyView::loadTritopologySelectedVertexToVertex(granny_tri_topology * TriTopologies){
    if(TriTopologies == nullptr){
        return;
    }
    try {
        for(int i = ui->tableWidget_vertexToVertex->rowCount(); i >= 0; i--){
            ui->tableWidget_vertexToVertex->removeRow(i);
        }
        for(granny_int32 i = 0; i < TriTopologies->VertexToVertexCount; i++){
            int row = ui->tableWidget_vertexToVertex->rowCount();
            ui->tableWidget_vertexToVertex->insertRow(row);
            ui->tableWidget_vertexToVertex->setItem(row, 0,new QTableWidgetItem(QString::number(TriTopologies->VertexToVertexMap[i])));
        }
        ui->tableWidget_vertexToVertex->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    } catch (...) {
        log += "Error accessing TriTopologies";
    }
}
void TritopologyView::loadTritopologySelectedTriToBone(granny_tri_topology * TriTopologies){
    if(TriTopologies == nullptr){
        return;
    }
    try {
        for(int i = ui->tableWidget_triangleToBone->rowCount(); i >= 0; i--){
            ui->tableWidget_triangleToBone->removeRow(i);
        }
        for(granny_int32 i = 0; i < TriTopologies->TriangleToBoneCount; i++){
            int row = ui->tableWidget_triangleToBone->rowCount();
            ui->tableWidget_triangleToBone->insertRow(row);
            ui->tableWidget_triangleToBone->setItem(row, 0,new QTableWidgetItem(QString::number(TriTopologies->TriangleToBoneIndices[i])));
        }
        ui->tableWidget_triangleToBone->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    } catch (...) {
        log += "Error accessing TriTopologies";
    }
}
void TritopologyView::loadTritopologySelectedAnnotationList(granny_tri_topology * TriTopologies){
    if(TriTopologies == nullptr){
        return;
    }
    try {
        for(int i = ui->tableWidget_annotationSet->rowCount(); i >= 0; i--){
            ui->tableWidget_annotationSet->removeRow(i);
        }
        for(granny_int32 i = 0; i < TriTopologies->TriAnnotationSetCount; i++){
            int row = ui->tableWidget_annotationSet->rowCount();
            ui->tableWidget_annotationSet->insertRow(row);
            ui->tableWidget_annotationSet->setItem(row, 0,new QTableWidgetItem(TriTopologies->TriAnnotationSets[i].Name));
            ui->tableWidget_annotationSet->setItem(row, 1,new QTableWidgetItem(QString::number(TriTopologies->TriAnnotationSets[i].TriAnnotationCount)));
            ui->tableWidget_annotationSet->setItem(row, 2,new QTableWidgetItem(QString::number(TriTopologies->TriAnnotationSets[i].TriAnnotationIndexCount)));
            ui->tableWidget_annotationSet->setItem(row, 3,new QTableWidgetItem(QString::number(TriTopologies->TriAnnotationSets[i].TriAnnotationCount)));
            ui->tableWidget_annotationSet->setItem(row, 4,new QTableWidgetItem(QString::number(TriTopologies->TriAnnotationSets[i].IndicesMapFromTriToAnnotation)));
        }

        ui->tableWidget_annotationSet->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    } catch (...) {
        log += "Error accessing TriTopologies";
    }
}

void TritopologyView::on_tableWidget_tritopology_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(TritopologySelected != row){
        TritopologySelected = row;
        loadTritopologySelected(mapListTritopology[row]);
    }
}
void TritopologyView::on_tableWidget_tritopology_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(currentColumn < 0){
        if(previousColumn < 0){
            return;
        }
        return;
    }
    if(currentRow != previousRow){
        if(TritopologySelected != currentRow){
            TritopologySelected = currentRow;
            loadTritopologySelected(mapListTritopology[currentRow]);
        }
    }
}
