#include "materialsview.h"
#include "ui_materialsview.h"

MaterialsView::MaterialsView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MaterialsView)
{
    ui->setupUi(this);
}

MaterialsView::~MaterialsView()
{
    delete ui;
}

void MaterialsView::addMaterials(granny_int32 MaterialCount, granny_material ** Materials){
    //Verifie le pointeur vers le tableau de materials
    if(Materials == nullptr){
        return;
    }
    addMaterialsToTable(MaterialCount, Materials);
    addMaterialsToTree(MaterialCount, Materials);
}

void MaterialsView::removeMaterial(void){
    for(int i = ui->tableWidget_materials->rowCount(); i >= 0; i--){
        ui->tableWidget_materials->removeRow(i);
    }
    ui->tableWidget_materials->clear();
    ui->tableWidget_materials->setHorizontalHeaderItem(0,new QTableWidgetItem("Name"));

    for(int i = ui->comboBox_materialSelectedMap->count(); i >= 0; i--){
        ui->comboBox_materialSelectedMap->removeItem(i);
    }
    ui->comboBox_materialSelectedMap->clear();

    for(int i = ui->comboBox_materialSelectedTexture->count(); i >= 0; i--){
        ui->comboBox_materialSelectedTexture->removeItem(i);
    }
    ui->comboBox_materialSelectedTexture->clear();

    ui->treeWidget_materials->clear();
    resetTextureSelected();
    resetTextureSelectedFileName();
    resetTextureSelectedType();
    resetTextureSelectedWidth();
    resetTextureSelectedHeight();
    resetTextureSelectedEncoding();
    resetTextureSelectedSubFormat();
    resetTextureSelectedLayout();
    resetTextureSelectedLayoutBytePerPixel();
    resetTextureSelectedLayoutShiftForCompo();
    resetTextureSelectedLayoutBitsForCompo();
    resetTextureSelectedImageCount();
}

void MaterialsView::addMaterialsToTable(granny_int32 MaterialCount, granny_material ** Materials){
    //Variable iteration material
    granny_int32 indexMaterial;
    for(indexMaterial = 0; indexMaterial < MaterialCount;indexMaterial++){
        //Pour tout les modèles du tableau
        try{
            if(Materials[indexMaterial] != nullptr){
                //Si le pointeur est valid
                try {
                    //stock le pointeur dans une variable locale
                    granny_material * curMaterial = Materials[indexMaterial];
                    if(curMaterial != nullptr){
                        //Si le pointeur du modèle est valide on recupère le nombre de ligne
                        int row = ui->tableWidget_materials->rowCount();
                        //on ajoute une ligne et on increment le nombre de ligne
                        ui->tableWidget_materials->insertRow(row);
                        QString materialName;
                        //Recupère le nombre de colone
                        //int column = ui->tableWidget_materials->columnCount();
                        QTableWidgetItem *newItem = nullptr;
                        if(curMaterial->Name != nullptr){
                            //Si le nom du modèle est valide
                            materialName = QString::fromLocal8Bit(curMaterial->Name);
                        }
                        else{
                            //Si le nom du modèle n'est pas valide on genere un nom
                            materialName = QString::number((int)curMaterial, 16);
                        }
                        //Crée l'objet représentant le modèle dans la liste
                        newItem = new QTableWidgetItem(materialName);
                        //Ajoute le modèle à la liste
                        ui->tableWidget_materials->setItem(row, 0, newItem);
                        mapListMaterial[row] = curMaterial;
                    }
                }catch (...){
                    log += "Error accessing material " + QString::number(indexMaterial);
                }
            }
        }catch (...) {
            log += "Error accessing materials table" + QString::number(indexMaterial);
        }
    }
}
void MaterialsView::addMaterialsToTree(granny_int32 MaterialCount, granny_material ** Materials){
    //Variable iteration material
    granny_int32 indexMaterial;
    for(indexMaterial = 0; indexMaterial < MaterialCount;indexMaterial++){
        //Pour tout les modèles du tableau
        try {
            QTreeWidgetItem * curItem = addElementOnTree(Materials[indexMaterial]);
            ui->treeWidget_materials->addTopLevelItem(curItem);
        } catch (...) {
            log += "Error accessing materials table" + QString::number(indexMaterial);
        }
    }
}

QTreeWidgetItem * MaterialsView::addElementOnTree(granny_material * Materials){
    //Cree un item pour le material
    QTreeWidgetItem * curItem = new QTreeWidgetItem();
    //Ajoute le nom du material
    curItem->setText(0,Materials->Name);
    //Ajoute le type d'objet
    curItem->setText(1,"Material");
    if(Materials->MapCount != 0){
        //Si le material contient des map
        granny_int32 indexMap;
        for(indexMap = 0; indexMap < Materials->MapCount;indexMap++){
            //Pour toute les map du material cree un item
            QTreeWidgetItem * curItemChild = new QTreeWidgetItem();
            //Ajoute l'usage de la map
            curItemChild->setText(0,Materials->Maps[indexMap].Usage);
            //Ajoute le type d'objet
            curItemChild->setText(1,"Map");
            //Ajoute tous les sous-item a l'item courant
            curItemChild->addChild(addElementOnTree(Materials->Maps[indexMap].Material));
            //Ajoute l'item map à l'item material
            curItem->addChild(curItemChild);
        }
    }
    else{
        //Si le materiel ne contient pas de map
        if(Materials->Texture != nullptr){
            //Si le material contient une texture valide
            QTreeWidgetItem * itemTexture = new QTreeWidgetItem();
            //Set le nom de l'item a "texture"
            itemTexture->setText(0, "Texture");
            //Ajoute l'item de texture à l'item de material
            MaterialsView::buildTextureItem(Materials->Texture, itemTexture);
            //Crée un item contenant les données étendu
            QTreeWidgetItem * itemExtendData = new QTreeWidgetItem();
            //Ajoute le nom de fichier
            itemExtendData->setText(0, "ExtendedData");
            //Contruit l'items qui contient les données etendu
            MaterialsView::buildExtendDataItem(&(Materials->ExtendedData), itemExtendData);
            //Ajoute l'item de texture et des données étentdu
            curItem->addChild(itemTexture);
            curItem->addChild(itemExtendData);
        }
    }
    //Retourne l'item de material
    return curItem;
}
void MaterialsView::buildExtendDataItem(granny_variant * variant, QTreeWidgetItem *rootItem){
    unsigned int i = 0;
    granny_data_type_definition * variantType = &variant->Type[i];
    void * ptr = variant->Object;
    /*char tmpChar[200];
    GrannyConvertSingleObject(variantType, ptr, variantType, tmpChar, 0);
    */
    do{
        QTreeWidgetItem * newItem = new QTreeWidgetItem();
        //Charge le nom de l'objet dans la première colonne
        newItem->setText(0, variantType->Name);
        //Charge la valeur de l'objet dans la deuxième colonne
        newItem->setText(1, unreferenceObj(&ptr, variantType));

        //Ajoute le nouvelle item à la racine
        rootItem->addChild(newItem);
        //Passe à l'objet suivant
        i++;
        variantType = &variant->Type[i];
    }while(variantType->Type != GrannyEndMember);
}
QString /*__attribute__ ((target("no-sse")))*/ MaterialsView::unreferenceObj(void ** ptr, granny_data_type_definition * variantType){
    granny_int32x ObjectSize = GrannyGetTotalObjectSize(variantType);
    //granny_int32x MemberSize = GrannyGetMemberTypeSize(variantType);
    switch (variantType->Type) {
        case GrannyEndMember :{
            break;
        }
        case GrannyInlineMember :{
            break;
        }
        case GrannyReferenceMember :{
            break;
        }
        case GrannyReferenceToArrayMember :{
            break;
        }
        case GrannyArrayOfReferencesMember :{
            break;
        }
        case GrannyUnsupportedMemberType_Remove :{
            break;
        }
        case GrannyReferenceToVariantArrayMember :{
            break;
        }
        case GrannyStringMember :{
            QString res;

            char *ptrVal = *(char **)*ptr;
            int i=0;

            while(ptrVal[i] != '\0'){
                res += ptrVal[i++];
            }
            (*(char***)ptr)++;

            return res;
        }
        case GrannyTransformMember :{
            break;
        }
        case GrannyReal32Member :{
            QString res;
            granny_real32 ptrVal = *(granny_real32 *)*ptr;
            (*(granny_real32**)ptr)++;
            res += QString::number(ptrVal);
            for(granny_int32 i = 1; i < variantType->ArrayWidth ; i++){
                res += ",";
                granny_real32 ptrVal = *(granny_real32 *)*ptr;
                (*(granny_real32**)ptr)++;
                res += QString::number(ptrVal);
            }
            return res;
        }
        case GrannyBinormalInt8Member :{
            QString res;
            granny_int8 ptrVal = *(granny_int8 *)*ptr;
            (*(granny_int8**)ptr)++;
            res += QString::number(ptrVal);
            for(granny_int32 i = 1; i < variantType->ArrayWidth ; i++){
                res += ",";
                granny_int8 ptrVal = *(granny_int8 *)*ptr;
                (*(granny_int8**)ptr)++;
                res += QString::number(ptrVal);
            }
            return res;
        }
        case GrannyUInt8Member :
        case GrannyNormalUInt8Member :{
            QString res;
            granny_uint8 ptrVal = *(granny_uint8 *)*ptr;
            (*(granny_uint8**)ptr)++;
            res += QString::number(ptrVal);
            for(granny_int32 i = 1; i < variantType->ArrayWidth ; i++){
                res += ",";
                granny_uint8 ptrVal = *(granny_uint8 *)*ptr;
                (*(granny_uint8**)ptr)++;
                res += QString::number(ptrVal);
            }
            return res;
        }
        case GrannyBinormalInt16Member :
        case GrannyInt16Member :{
            QString res;
            granny_int16 ptrVal = *(granny_int16 *)*ptr;
            (*(granny_int16**)ptr)++;
            res += QString::number(ptrVal);
            for(granny_int32 i = 1; i < variantType->ArrayWidth ; i++){
                res += ",";
                granny_int16 ptrVal = *(granny_int16 *)*ptr;
                (*(granny_int16**)ptr)++;
                res += QString::number(ptrVal);
            }
            return res;
        }
        case GrannyNormalUInt16Member :
        case GrannyUInt16Member :{
            QString res;
            granny_uint16 ptrVal = *(granny_uint16 *)*ptr;
            (*(granny_uint16**)ptr)++;
            res += QString::number(ptrVal);
            for(granny_int32 i = 1; i < variantType->ArrayWidth ; i++){
                res += ",";
                granny_uint16 ptrVal = *(granny_uint16 *)*ptr;
                (*(granny_uint16**)ptr)++;
                res += QString::number(ptrVal);
            }
            return res;
        }
        case GrannyInt32Member :{
            QString res;
            granny_int32 ptrVal = *(granny_int32 *)*ptr;
            (*(granny_int32**)ptr)++;
            res += QString::number(ptrVal);
            for(granny_int32 i = 1; i < variantType->ArrayWidth ; i++){
                res += ",";
                granny_int32 ptrVal = *(granny_int32 *)*ptr;
                (*(granny_int32**)ptr)++;
                res += QString::number(ptrVal);
            }
            return res;
        }
        case GrannyUInt32Member :{
            QString res;
            granny_uint32 ptrVal = *(granny_uint32 *)*ptr;
            (*(granny_uint32**)ptr)++;
            res += QString::number(ptrVal);
            for(granny_int32 i = 1; i < variantType->ArrayWidth ; i++){
                res += ",";
                granny_uint32 ptrVal = *(granny_uint32 *)*ptr;
                (*(granny_uint32**)ptr)++;
                res += QString::number(ptrVal);
            }
            return res;
        }
        case GrannyReal16Member :{
            break;
        }
        case GrannyEmptyReferenceMember :{
            break;
        }
        case GrannyOnePastLastMemberType :{
            break;
        }
        case Grannymember_type_forceint :{
            break;
        }
        default:
            return QString::fromLocal8Bit("Unknown member type");
    }
    QString res("Unsupported type");
    for(granny_int32 i = 0; i < ObjectSize ; i++){
        (*(granny_uint8**)ptr)++;
    }
    return res;
}
void MaterialsView::buildTextureItem(granny_texture * texture, QTreeWidgetItem *rootItem){
    //Récupère le path du fichier de texture
    QFileInfo fileInfo(texture->FromFileName);
    //Extrai le nom de fichier
    QString fileName = fileInfo.fileName();

    //Crée un item représentant le type de texture
    QTreeWidgetItem * newItem = new QTreeWidgetItem();
    newItem->setText(1, fileName);
    newItem->setText(0, "File Name");
    rootItem->addChild(newItem);

    //Crée un item représentant le type de texture
    newItem = new QTreeWidgetItem();

    newItem->setText(1, QString::fromLocal8Bit(GrannyGetTextureTypeName(texture->TextureType)));
    newItem->setText(0, "Texture Type");
    rootItem->addChild(newItem);

    //Crée un item représentant le width de la texture
    newItem = new QTreeWidgetItem();
    newItem->setText(1, QString::number(texture->Width));
    newItem->setText(0, "Width");
    rootItem->addChild(newItem);

    //Crée un item représentant le height de la texture
    newItem = new QTreeWidgetItem();
    newItem->setText(1, QString::number(texture->Height));
    newItem->setText(0, "Height");
    rootItem->addChild(newItem);

    //Crée un item représentant le height de la texture
    newItem = new QTreeWidgetItem();
    newItem->setText(1, QString::number(texture->Height));
    newItem->setText(0, "Height");
    rootItem->addChild(newItem);

    //Crée un item représentant l'encoding de la texture

    newItem = new QTreeWidgetItem();
    newItem->setText(1, QString::fromLocal8Bit(GrannyGetTextureEncodingName(texture->Encoding)));
    newItem->setText(0, "Encoding");
    rootItem->addChild(newItem);

    //Crée un item représentant l'encoding de la texture
    newItem = new QTreeWidgetItem();
    newItem->setText(1, QString::number(texture->SubFormat));
    newItem->setText(0, "SubFormat");
    rootItem->addChild(newItem);

    //Crée un item représentant le nombre d'image de la texture
    newItem = new QTreeWidgetItem();
    newItem->setText(1, QString::number(texture->ImageCount));
    newItem->setText(0, "ImageCount");
    rootItem->addChild(newItem);
}

void MaterialsView::on_tableWidget_materials_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(materialSelected != row){
        loadMaterial(row);
        materialSelected = row;
    }
}

void MaterialsView::loadMaterial(int key){
    granny_material * ptrMaterial = this->mapListMaterial[key];
    if(ptrMaterial == nullptr){
        return;
    }
    //Affiche le nom du modele selectionne dans la liste
    loadMaterialMaps(ptrMaterial);
    //Affiche le placement initial du model
    loadMaterialTextures(ptrMaterial);
}
void MaterialsView::loadMaterialMaps(granny_material *Materials){
    ui->comboBox_materialSelectedMap->clear();
    if(Materials == nullptr){
        return;
    }
    if(Materials->Maps == nullptr){
        return;
    }
    try {
        granny_int32 index;
        for(index = 0; index < Materials->MapCount; index++){
            if(Materials->Maps->Usage == nullptr){
                ui->comboBox_materialSelectedMap->insertItem(ui->comboBox_materialSelectedMap->count(),
                                                               "0x"+QString::number((int)Materials->Maps[index].Usage, 16));
            }
            else {
                ui->comboBox_materialSelectedMap->insertItem(ui->comboBox_materialSelectedMap->count(), Materials->Maps[index].Usage);
            }
        }
        ui->comboBox_materialSelectedMap->setCurrentIndex(0);
        if(index>=1){
            loadMapSelected(&Materials->Maps[index]);
        }
    }catch(std::exception& e){
        log += QString::fromLocal8Bit(e.what());
    }
    catch (...) {
        log += "Erreur lecture Map material selectionne\n";
    }
}
void MaterialsView::loadMaterialTextures(granny_material *Materials){
    ui->comboBox_materialSelectedTexture->clear();
    resetTextureSelected();
    if(Materials == nullptr){
        return;
    }
    if(Materials->Texture == nullptr){
        return;
    }
    try {
        if(Materials->Texture->FromFileName == nullptr){
            ui->comboBox_materialSelectedTexture->insertItem(ui->comboBox_materialSelectedTexture->count(),
                                                           "0x"+QString::number((int)Materials->Texture, 16));
        }
        else {
            ui->comboBox_materialSelectedTexture->insertItem(ui->comboBox_materialSelectedTexture->count(), Materials->Texture->FromFileName);
        }

        ui->comboBox_materialSelectedTexture->setCurrentIndex(0);
        loadTextureSelected(Materials->Texture);
    } catch (...) {
        log += "Erreur lecture Texture material selectionne\n";
    }
}


void MaterialsView::loadMapSelected(granny_material_map * Map){
    if(Map == nullptr){
        return;
    }
    try {
        loadMapSelectedUsage(Map);
        loadMapSelectedMaterialName(Map);
    } catch (...) {
        log += "Erreur lecture map material selectionne\n";
    }
}
void MaterialsView::loadMapSelectedUsage(granny_material_map *Map){
    if(Map == nullptr){
        return;
    }
    if(Map->Usage == nullptr){
        return;
    }
    try {
        if(Map->Material == nullptr){
            return;
        }
        ui->label_valMaterialMapSelectedUsage->setText(Map->Usage);
    }catch(std::exception& e){
        log += QString::fromLocal8Bit(e.what());
    }catch (...) {
        log += "Erreur lecture map usage material selectionne\n";
    }
}
void MaterialsView::loadMapSelectedMaterialName(granny_material_map *Map){
    if(Map == nullptr){
        return;
    }
    if(Map->Material == nullptr){
        return;
    }
    if(Map->Material->Name == nullptr){
        return;
    }
    try {
        ui->label_valMaterialMapSelectedMaterialName->setText(Map->Material->Name);
    }catch (...) {
        log += "Erreur lecture map material name material selectionne\n";
    }
}

void MaterialsView::loadTextureSelected(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        loadTextureSelectedFileName(texture);
        loadTextureSelectedType(texture);
        loadTextureSelectedWidth(texture);
        loadTextureSelectedHeight(texture);
        loadTextureSelectedEncoding(texture);
        loadTextureSelectedSubFormat(texture);
        loadTextureSelectedLayout(texture);
        loadTextureSelectedImageCount(texture);
    } catch (...) {
        log += "Erreur lecture Texture material selectionne\n";
    }
}
void MaterialsView::loadTextureSelectedFileName(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    if(texture->FromFileName == nullptr){
        return;
    }
    try {
        ui->label_valMaterialsTextureSelectedFileName->setText(texture->FromFileName);
    } catch (...) {
        log += "Erreur lecture FileName Texture selectionne material selectionne\n";
    }
}
void MaterialsView::loadTextureSelectedType(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        QString strType(GrannyGetTextureTypeName(texture->TextureType));
        ui->label_valMaterialsTextureSelectedTextureType->setText(strType);
    } catch (...) {
        log += "Erreur lecture Type Texture selectionne material selectionne\n";
    }
}
void MaterialsView::loadTextureSelectedWidth(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        ui->label_valMaterialsTextureSelectedWidth->setText(QString::number(texture->Width));
    } catch (...) {
        log += "Erreur lecture Width Texture selectionne material selectionne\n";
    }
}
void MaterialsView::loadTextureSelectedHeight(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        ui->label_valMaterialsTextureSelectedHeight->setText(QString::number(texture->Height));
    } catch (...) {
        log += "Erreur lecture Height Texture selectionne material selectionne\n";
    }
}
void MaterialsView::loadTextureSelectedEncoding(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        QString strType(GrannyGetTextureEncodingName(texture->Encoding));
        ui->label_valMaterialsTextureSelectedEncoding->setText(strType);
    } catch (...) {
        log += "Erreur lecture Encoding Texture selectionne material selectionne\n";
    }
}
void MaterialsView::loadTextureSelectedSubFormat(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        ui->label_valMaterialsTextureSelectedSubFormat->setText(QString::number(texture->SubFormat));
    } catch (...) {
        log += "Erreur lecture Sub format Texture selectionne material selectionne\n";
    }
}

void MaterialsView::loadTextureSelectedLayout(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        loadTextureSelectedLayoutBytePerPixel(texture);
        loadTextureSelectedLayoutShiftForCompo(texture);
        loadTextureSelectedLayoutBitsForCompo(texture);
    } catch (...) {
        log += "Erreur lecture Layout Texture selectionne material selectionne\n";
    }
}
void MaterialsView::loadTextureSelectedLayoutBytePerPixel(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        ui->label_valBytePerPixel->setText(QString::number(texture->Layout.BytesPerPixel));
    } catch (...) {
        log += "Erreur lecture Layout Texture selectionne material selectionne\n";
    }
}
void MaterialsView::loadTextureSelectedLayoutShiftForCompo(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        ui->label_valShiftForComponent0->setText(QString::number(texture->Layout.ShiftForComponent[0]));
        ui->label_valShiftForComponent1->setText(QString::number(texture->Layout.ShiftForComponent[1]));
        ui->label_valShiftForComponent2->setText(QString::number(texture->Layout.ShiftForComponent[2]));
        ui->label_valShiftForComponent3->setText(QString::number(texture->Layout.ShiftForComponent[3]));
    } catch (...) {
        log += "Erreur lecture Layout Texture selectionne material selectionne\n";
    }
}
void MaterialsView::loadTextureSelectedLayoutBitsForCompo(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        ui->label_valBitsForCompo0->setText(QString::number(texture->Layout.BitsForComponent[0]));
        ui->label_valBitsForCompo1->setText(QString::number(texture->Layout.BitsForComponent[1]));
        ui->label_valBitsForCompo2->setText(QString::number(texture->Layout.BitsForComponent[2]));
        ui->label_valBitsForCompo3->setText(QString::number(texture->Layout.BitsForComponent[3]));
    } catch (...) {
        log += "Erreur lecture Layout Texture selectionne material selectionne\n";
    }
}

void MaterialsView::loadTextureSelectedImageCount(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        ui->label_valMaterialsTextureSelectedImageCount->setText(QString::number(texture->ImageCount));
    } catch (...) {
        log += "Erreur lecture ImageCount Texture selectionne material selectionne\n";
    }
}

void MaterialsView::resetTextureSelected(void){
    resetTextureSelectedFileName();
    resetTextureSelectedType();
    resetTextureSelectedWidth();
    resetTextureSelectedHeight();
    resetTextureSelectedEncoding();
    resetTextureSelectedSubFormat();
    resetTextureSelectedLayout();
    resetTextureSelectedImageCount();
}
void MaterialsView::resetTextureSelectedFileName(void){
    ui->label_valMaterialsTextureSelectedFileName->setText("None");
}
void MaterialsView::resetTextureSelectedType(void){
    ui->label_valMaterialsTextureSelectedTextureType->setText("None");
}
void MaterialsView::resetTextureSelectedWidth(void){
    ui->label_valMaterialsTextureSelectedWidth->setText("None");
}
void MaterialsView::resetTextureSelectedHeight(void){
    ui->label_valMaterialsTextureSelectedHeight->setText("None");
}
void MaterialsView::resetTextureSelectedEncoding(void){
    ui->label_valMaterialsTextureSelectedEncoding->setText("None");
}
void MaterialsView::resetTextureSelectedSubFormat(void){
    ui->label_valMaterialsTextureSelectedSubFormat->setText("None");
}
void MaterialsView::resetTextureSelectedLayout(void){
    resetTextureSelectedLayoutBytePerPixel();
    resetTextureSelectedLayoutShiftForCompo();
    resetTextureSelectedLayoutBitsForCompo();
}
void MaterialsView::resetTextureSelectedLayoutBytePerPixel(void){
    ui->label_valBytePerPixel->setText("None");
}
void MaterialsView::resetTextureSelectedLayoutShiftForCompo(void){
    ui->label_valShiftForComponent0->setText("0");
    ui->label_valShiftForComponent1->setText("0");
    ui->label_valShiftForComponent2->setText("0");
    ui->label_valShiftForComponent3->setText("0");
}
void MaterialsView::resetTextureSelectedLayoutBitsForCompo(void){
    ui->label_valBitsForCompo0->setText("0");
    ui->label_valBitsForCompo1->setText("0");
    ui->label_valBitsForCompo2->setText("0");
    ui->label_valBitsForCompo3->setText("0");
}
void MaterialsView::resetTextureSelectedImageCount(void){
    ui->label_valMaterialsTextureSelectedImageCount->setText("0");
}

void MaterialsView::on_tableWidget_materials_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(currentColumn < 0){
        return;
    }
    if(currentRow == previousRow){
        //anti warning
        if(previousColumn)
            return;
        return;
    }
    if(currentRow != materialSelected){
        materialSelected = currentRow;
        loadMaterial(currentRow);
    }
}
