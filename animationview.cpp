#include "animationview.h"
#include "ui_animationview.h"

AnimationView::AnimationView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AnimationView)
{
    ui->setupUi(this);
    ui->tableWidget_animation->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    animationSelected = -1;
}

AnimationView::~AnimationView()
{
    delete ui;
}

void AnimationView::addAnimation(granny_int32 AnimationCount, granny_animation ** Animations){
    //Verifie le pointeur vers le tableau de Animations
    if(Animations == nullptr){
        return;
    }
    //Variable iteration Animations
    granny_int32 index;
    for(index = 0; index < AnimationCount;index++){
        //Pour tout les modèles du tableau
        try{
            if(Animations[index] != nullptr){
                //Si le pointeur est valid
                try {
                    //stock le pointeur dans une variable locale
                    granny_animation * curAnimation = Animations[index];
                    if(curAnimation != nullptr){
                        //Si le pointeur du modèle est valide on recupère le nombre de ligne
                        int row = ui->tableWidget_animation->rowCount();
                        //on ajoute une ligne et on increment le nombre de ligne
                        ui->tableWidget_animation->insertRow(row);
                        QString AnimationName;
                        //Recupère le nombre de colone
                        //int column = ui->tableWidget_animation->columnCount();
                        QTableWidgetItem *newItem = nullptr;
                        if(curAnimation->Name != nullptr){
                            //Si le nom du modèle est valide
                            AnimationName = QString::fromLocal8Bit(curAnimation->Name);
                        }
                        else{
                            //Si le nom du modèle n'est pas valide on genere un nom
                            AnimationName = QString::number((int)curAnimation, 16);
                        }
                        //Crée l'objet représentant le modèle dans la liste
                        newItem = new QTableWidgetItem(AnimationName);
                        //Ajoute le modèle à la liste
                        ui->tableWidget_animation->setItem(row, 0, newItem);
                        mapListAnimation[row] = curAnimation;
                    }
                }catch (...){
                    log += "Error accessing Animations " + QString::number(index);
                }
            }
        }catch (...) {
            log += "Error accessing Animations table" + QString::number(index);
        }
    }
}
void AnimationView::resetAnimations(void){
    animationSelected = -1;
    log = "";
    mapListAnimation.clear();
    for(int i = ui->tableWidget_animation->rowCount(); i >= 0; i--){
        ui->tableWidget_animation->removeRow(i);
    }
}
void AnimationView::loadAnimationSelected(granny_animation * Animations){
    if(Animations == nullptr){
        return;
    }
    try {
        loadAnimationSelectedName(Animations);
        loadAnimationSelectedDuration(Animations);
        loadAnimationSelectedTimeStep(Animations);
        loadAnimationSelectedOverSampling(Animations);
        loadAnimationSelectedLoop(Animations);
        loadAnimationSelectedFlags(Animations);
    } catch (...) {
        log += "Error accessing Animation Selected";
    }
}
void AnimationView::loadAnimationSelectedName(granny_animation * Animations){
    if(Animations == nullptr){
        return;
    }
    try {
        if(Animations->Name == nullptr){
            ui->label_valName->setText(QString::number((int)Animations, 16));
        }
        else{
            ui->label_valName->setText(Animations->Name);
        }
    } catch (...) {
        log += "Error accessing Name Animation Selected";
    }
}
void AnimationView::loadAnimationSelectedDuration(granny_animation * Animations){
    if(Animations == nullptr){
        return;
    }
    try {
        ui->label_valDuration->setText(QString::number(Animations->Duration));
    } catch (...) {
        log += "Error accessing Name Animation Selected";
    }
}
void AnimationView::loadAnimationSelectedTimeStep(granny_animation * Animations){
    if(Animations == nullptr){
        return;
    }
    try {
        ui->label_valTimestep->setText(QString::number(Animations->TimeStep));
    } catch (...) {
        log += "Error accessing Name Animation Selected";
    }
}
void AnimationView::loadAnimationSelectedOverSampling(granny_animation * Animations){
    if(Animations == nullptr){
        return;
    }
    try {
        ui->label_valOversampling->setText(QString::number(Animations->Oversampling));
    } catch (...) {
        log += "Error accessing Name Animation Selected";
    }
}
void AnimationView::loadAnimationSelectedLoop(granny_animation * Animations){
    if(Animations == nullptr){
        return;
    }
    try {
        ui->label_valDefaultloop->setText(QString::number(Animations->DefaultLoopCount));
    } catch (...) {
        log += "Error accessing Name Animation Selected";
    }
}
void AnimationView::loadAnimationSelectedFlags(granny_animation * Animations){
    if(Animations == nullptr){
        return;
    }
    try {
        switch (Animations->Flags) {
            case GrannyAnimationDefaultLoopCountValid:{
                ui->label_valFlags->setText("GrannyAnimationDefaultLoopCountValid");
                break;
            }
            case Grannyanimation_flags_forceint:{
                ui->label_valFlags->setText("Grannyanimation_flags_forceint");
                break;
            }
            default:
                ui->label_valFlags->setText("Unsupported animation flag (" + QString::number(Animations->Flags) + ")");
        }
    } catch (...) {
        log += "Error accessing Name Animation Selected";
    }
}

void AnimationView::on_tableWidget_animation_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(row != animationSelected){
        animationSelected = row;
        loadAnimationSelected(mapListAnimation[row]);
    }
}
void AnimationView::on_tableWidget_animation_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(currentColumn < 0){
        return;
    }
    if(currentRow == previousRow){
        //anti warning
        if(previousColumn)
            return;
        return;
    }
    if(currentRow != animationSelected){
        animationSelected = currentRow;
        loadAnimationSelected(mapListAnimation[currentRow]);
    }
}
