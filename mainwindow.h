#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "exporter.h"
#include "fileconfig.h"
#include "threadconvert.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    int nbProcRunning;
    static std::string getFileNameFromPath(std::string str);
    static std::string getFolderFromPath(std::string str);
private slots:
    void on_ThreadFinish(ThreadConvert * s);
    void on_pushButton_Open_clicked();
    void on_pushButton_ExportAllModelNB2_clicked();
    void on_pushButton_ExportAnimNA2_clicked();
    void on_pushButton_exportCN6_clicked();
    void on_pushButton_OpenMSM_clicked();
    void on_pushButton_OpenAnimTxt_clicked();
    void on_pushButton_OpenFolder_clicked();
    void on_ExporterNewNa2File(std::string fileName);
    void on_ExporterNewCn6File(std::string fileName);
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionReset_triggered();
    void on_actionSaveAs_triggered();
    bool evaluateFolder(QString folder, QStringList &msmFile, QStringList &txtFile);
    bool convertSimpleDir(QString dirName, QStringList &msmFile, QStringList &txtFile);
    bool convertMultipleDir(QString dirName, QStringList &msmFile, QStringList &txtFile);
    bool readMsmFileSA(std::string fileName, Exporter * exporterObjSA);
    bool readTxtFileSA(std::string fileNameStd, Exporter * exporterObjSA);

private:

    bool readMsmFile(std::string fileName);
    bool readTxtFile(std::string fileNameStd);
    Ui::MainWindow *ui;
    Exporter * exporterObj;
    FileConfig configObj;
    std::vector<std::string> m_animFiles;
    std::vector<std::string> m_animName;

    std::vector<std::string> m_listNa2File;
    std::vector<std::string> m_listCn6File;
    std::string configFile;
};
#endif // MAINWINDOW_H
