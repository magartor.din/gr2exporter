#include "vertexview.h"
#include "ui_vertexview.h"

VertexView::VertexView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::VertexView)
{
    ui->setupUi(this);
    ui->tableWidget_annotationSet->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    vertexSelected = -1;
}

VertexView::~VertexView(){
    delete ui;
}

void VertexView::addVertex(granny_int32 VertexDataCount, granny_vertex_data ** VertexDatas){
    //Verifie le pointeur vers le tableau de VertexDatas
    if(VertexDatas == nullptr){
        return;
    }
    //Variable iteration VertexDatas
    granny_int32 index;
    for(index = 0; index < VertexDataCount;index++){
        //Pour tout les modèles du tableau
        try{
            if(VertexDatas[index] != nullptr){
                //Si le pointeur est valid
                try{
                    //stock le pointeur dans une variable locale
                    granny_vertex_data * curVertexDatas = VertexDatas[index];
                    if(curVertexDatas != nullptr){
                        //Si le pointeur du modèle est valide on recupère le nombre de ligne
                        int row = ui->tableWidget_vertex->rowCount();
                        //on ajoute une ligne et on increment le nombre de ligne
                        ui->tableWidget_vertex->insertRow(row);
                        QString vertexDatasName;
                        //Recupère le nombre de colone
                        //int column = ui->tableWidget_vertex->columnCount();
                        QTableWidgetItem *newItem = nullptr;
                        if(curVertexDatas->VertexType != nullptr){
                            vertexDatasName = "0x" + QString::number((int)curVertexDatas, 16);
                        }
                        else{
                            continue;
                        }
                        //Crée l'objet représentant le modèle dans la liste
                        newItem = new QTableWidgetItem(vertexDatasName);
                        //Ajoute le modèle à la liste
                        ui->tableWidget_vertex->setItem(row, 0, newItem);
                        mapListVertex[row] = curVertexDatas;
                    }
                }catch (...){
                    log += "Error accessing vertex datas " + QString::number(index);
                }
            }
        }catch (...) {
            log += "Error accessing vertex datas table" + QString::number(index);
        }
    }
}

void VertexView::resetVertex(void){
    ui->label_valVertexType->setText("None");

    mapListVertex.clear();

    for(int i = ui->tableWidget_vertexComponentName->rowCount(); i >= 0; i--){
        ui->tableWidget_vertexComponentName->removeRow(i);
    }
    for(int i = ui->tableWidget_vertex->rowCount(); i >= 0; i--){
        ui->tableWidget_vertex->removeRow(i);
    }
    for(int i = ui->tableWidget_annotationSet->rowCount(); i >= 0; i--){
        ui->tableWidget_annotationSet->removeRow(i);
    }
    for(int i = ui->tableWidget_vertice->rowCount(); i >= 0; i--){
        ui->tableWidget_vertice->removeRow(i);
    }
    vertexSelected = -1;
}
void VertexView::loadVertexSelected(granny_vertex_data * VertexDatas){
    if(VertexDatas == nullptr){
        return;
    }
    try {
        loadVertexSelectedTypeName(VertexDatas);
        loadVertexSelectedVertices(VertexDatas);
        loadVertexSelectedComponent(VertexDatas);
        loadVertexSelectedAnnotation(VertexDatas);
    } catch (...) {
        log += "Error accessing vertex selected";
    }
}
void VertexView::loadVertexSelectedTypeName(granny_vertex_data * VertexDatas){
    if(VertexDatas == nullptr){
        return;
    }
    try {
        if(VertexDatas->VertexType == nullptr){
            ui->label_valVertexType->setText("nullptr Type");
        }
        if(VertexDatas->VertexType->Name == nullptr){
            ui->label_valVertexType->setText("nullptr NameType");
        }
        else{
            ui->label_valVertexType->setText(VertexDatas->VertexType->Name);
        }
    } catch (...) {
        log += "Error accessing name vertex selected";
    }
}
void VertexView::loadVertexSelectedVertices(granny_vertex_data * VertexDatas){
    if(VertexDatas == nullptr){
        return;
    }
    try {
        for(int i = ui->tableWidget_vertice->rowCount(); i >= 0; i--){
            ui->tableWidget_vertice->removeRow(i);
        }
        for(int i = ui->tableWidget_vertice->columnCount(); i >= 0; i--){
            ui->tableWidget_vertice->removeColumn(i);
        }
        void * ptrData = (void *)VertexDatas->Vertices;

        std::map<QString, QString> mapFieldValue;
        std::map<QString, int> mapIndexField;
        std::vector<QString> lstField;

        granny_data_type_definition * variantType = &VertexDatas->VertexType[0];
        int indexField = 0;
        int indexVariant = 0;
        QString emptyStr("");
        //Regarde le format des vertice
        while(variantType->Type != GrannyEndMember){
            //Si le type est un fin de type definition on passe au vertex suivant
            QString fieldName = QString::fromLocal8Bit(variantType->Name);
            lstField.push_back(fieldName);
            mapFieldValue[fieldName] = emptyStr;
            mapIndexField[fieldName] = indexField++;
            variantType = &VertexDatas->VertexType[++indexVariant];
        }
        //Ajoute les colonne en fonction du format de vertice
        for(size_t i = 0; i < lstField.size(); i++){
            //Pour tous les nouveau field on ajout la colone
            ui->tableWidget_vertice->insertColumn(i);
            ui->tableWidget_vertice->setHorizontalHeaderItem(i,new QTableWidgetItem(lstField.at(i)));
        }
        //Parcours tous les vertices
        for(granny_int32 indexVertex = 0;  indexVertex < VertexDatas->VertexCount; indexVertex++){
            //Pour tous les vertices
            //Index du field courrant
            int indexVariant = 0;
            //Récupère le type du premier field
            granny_data_type_definition * variantType = &VertexDatas->VertexType[0];
            //Iteration d'un vertice
            while(variantType->Type != GrannyEndMember){
                //Tant que le fields n'est pas une fin de type récupère le type
                granny_data_type_definition * variantType = &VertexDatas->VertexType[indexVariant++];
                //Si le type est un fin de type definition on passe au vertex suivant
                if(variantType->Type == GrannyEndMember){
                    break;
                }
                //Récupère la valeur du champ
                QString str(ExtendedDataView::unreferenceObj(&ptrData, variantType));
                //Récupère le nom du champ
                QString fieldName = QString::fromLocal8Bit(variantType->Name);
                mapFieldValue[fieldName] = str;
            }
            //On a parcouru un vertice. On ajoute une ligne
            int row = ui->tableWidget_vertice->rowCount();
            ui->tableWidget_vertice->insertRow(row);
            //Pour tous les fields des vertice
            for(size_t i = 0; i < lstField.size(); i++){
                //On ajoute un item dans la celule correspondante au field
                ui->tableWidget_vertice->setItem(row, mapIndexField[lstField.at(i)], new QTableWidgetItem(mapFieldValue[lstField.at(i)]));
            }
        }
        //On ajoute un item dans la celule correspondante au field
        ui->tableWidget_vertice->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
        //ExtendedDataView::unreferenceObj
    } catch (...) {
        log += "Error accessing vertices vertex selected";
    }
}
void VertexView::loadVertexSelectedComponent(granny_vertex_data * VertexDatas){
    if(VertexDatas == nullptr){
        return;
    }
    try {

        for(int i = ui->tableWidget_vertexComponentName->rowCount(); i >= 0; i--){
            ui->tableWidget_vertexComponentName->removeRow(i);
        }
        for(granny_int32 i = 0; i < VertexDatas->VertexComponentNameCount; i++){
            int row = ui->tableWidget_vertexComponentName->rowCount();
            ui->tableWidget_vertexComponentName->insertRow(row);
            if(VertexDatas->VertexComponentNames[i] == nullptr){
                ui->tableWidget_vertexComponentName->setItem(row, 0, new QTableWidgetItem(QString::number((int)&VertexDatas->VertexComponentNames[i], 16)));
            }
            else {
                ui->tableWidget_vertexComponentName->setItem(row, 0, new QTableWidgetItem(VertexDatas->VertexComponentNames[i]));
            }
        }
    } catch (...) {
        log += "Error accessing component vertex selected";
    }
}
void VertexView::loadVertexSelectedAnnotation(granny_vertex_data * VertexDatas){
    if(VertexDatas == nullptr){
        return;
    }
    try {
        for(int i = ui->tableWidget_annotationSet->rowCount(); i >= 0; i--){
            ui->tableWidget_annotationSet->removeRow(i);
        }
        for(granny_int32 i = 0; i < VertexDatas->VertexAnnotationSetCount; i++){
            int row = ui->tableWidget_annotationSet->rowCount();
            ui->tableWidget_annotationSet->insertRow(row);
            ui->tableWidget_annotationSet->setItem(row, 0,new QTableWidgetItem(VertexDatas->VertexAnnotationSets[i].Name));
            ui->tableWidget_annotationSet->setItem(row, 1,new QTableWidgetItem(QString::number(VertexDatas->VertexAnnotationSets[i].VertexAnnotationCount)));
            ui->tableWidget_annotationSet->setItem(row, 2,new QTableWidgetItem(QString::number(VertexDatas->VertexAnnotationSets[i].VertexAnnotationIndexCount)));
            ui->tableWidget_annotationSet->setItem(row, 3,new QTableWidgetItem(QString::number(VertexDatas->VertexAnnotationSets[i].VertexAnnotationCount)));
            ui->tableWidget_annotationSet->setItem(row, 4,new QTableWidgetItem(QString::number(VertexDatas->VertexAnnotationSets[i].IndicesMapFromVertexToAnnotation)));
        }

        ui->tableWidget_annotationSet->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);

    } catch (...) {
        log += "Error accessing annotation vertex selected";
    }
}

void VertexView::on_tableWidget_vertex_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(row != vertexSelected){
        vertexSelected = row;
        loadVertexSelected(mapListVertex[row]);
    }
}

void VertexView::on_tableWidget_vertex_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(currentColumn < 0){
        if(previousColumn < 0){
            return;
        }
        return;
    }
    if(currentRow != previousRow){
        if(currentRow != vertexSelected){
            vertexSelected = currentRow;
            loadVertexSelected(mapListVertex[currentRow]);
        }
    }
}
