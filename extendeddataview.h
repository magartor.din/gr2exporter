#ifndef EXTENDEDDATAVIEW_H
#define EXTENDEDDATAVIEW_H

#include <QWidget>
#include <QLabel>
#include <QGridLayout>

#include "NBCpp/granny.h"

#include "GridLayoutUtil.h"

namespace Ui {
class ExtendedDataView;
}


class ExtendedDataView : public QWidget
{
    Q_OBJECT

public:
    explicit ExtendedDataView(QWidget *parent = nullptr);
    ~ExtendedDataView();
    /*__attribute__ ((target("no-sse")))*/
    void loadGrannyVariant(granny_variant * data);
    void resetGrannyVariant(void);
    static QString unreferenceObj(void ** ptr, granny_data_type_definition * variantType);

private:
    Ui::ExtendedDataView *ui;
    QList<QLabel *> lstLabel;
};


#endif // EXTENDEDDATAVIEW_H
