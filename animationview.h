#ifndef ANIMATIONVIEW_H
#define ANIMATIONVIEW_H

#include <QWidget>
#include "NBCpp/granny.h"

namespace Ui {
class AnimationView;
}

class AnimationView : public QWidget
{
    Q_OBJECT

public:
    explicit AnimationView(QWidget *parent = nullptr);
    ~AnimationView();

public slots:
    void addAnimation(granny_int32 AnimationCount, granny_animation ** Animations);
    void resetAnimations(void);

    void loadAnimationSelected(granny_animation * Animations);
    void loadAnimationSelectedName(granny_animation * Animations);
    void loadAnimationSelectedDuration(granny_animation * Animations);
    void loadAnimationSelectedTimeStep(granny_animation * Animations);
    void loadAnimationSelectedOverSampling(granny_animation * Animations);
    void loadAnimationSelectedLoop(granny_animation * Animations);
    void loadAnimationSelectedFlags(granny_animation * Animations);
private slots:
    void on_tableWidget_animation_cellClicked(int row, int column);

    void on_tableWidget_animation_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

private:
    Ui::AnimationView *ui;
    QString log;
    std::map<int, granny_animation *> mapListAnimation;
    int animationSelected;
};

#endif // ANIMATIONVIEW_H
