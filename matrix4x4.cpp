#include "matrix4x4.h"
#include "ui_matrix4x4.h"

Matrix4x4::Matrix4x4(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Matrix4x4)
{
    ui->setupUi(this);//label_M00
    this->matLabel[0][0] = ui->label_M00;
    this->matLabel[0][1] = ui->label_M01;
    this->matLabel[0][2] = ui->label_M02;
    this->matLabel[0][3] = ui->label_M03;

    this->matLabel[1][0] = ui->label_M10;
    this->matLabel[1][1] = ui->label_M11;
    this->matLabel[1][2] = ui->label_M12;
    this->matLabel[1][3] = ui->label_M13;

    this->matLabel[2][0] = ui->label_M20;
    this->matLabel[2][1] = ui->label_M21;
    this->matLabel[2][2] = ui->label_M22;
    this->matLabel[2][3] = ui->label_M23;

    this->matLabel[3][0] = ui->label_M30;
    this->matLabel[3][1] = ui->label_M31;
    this->matLabel[3][2] = ui->label_M32;
    this->matLabel[3][3] = ui->label_M33;
}
Matrix4x4::~Matrix4x4()
{
    delete ui;
}

void Matrix4x4::reset(void){
    for(unsigned int i = 0; i < 4; i++){
        for(unsigned int j = 0; j < 4; j++){
            this->matLabel[i][j]->setText("0");
        }
    }
}
//
template<>
void Matrix4x4::setValues(granny_matrix_4x4 vals){
    try {
        for(unsigned int i = 0; i < 4; i++){
            for(unsigned int j = 0; j < 4; j++){
                this->matLabel[i][j]->setText(QString::number(vals[i][j]));
            }
        }
    } catch (...) {
        log += "Pointeur de donnée invalide\n";
    }
}
