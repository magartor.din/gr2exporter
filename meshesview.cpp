#include "meshesview.h"
#include "ui_meshesview.h"

MeshesView::MeshesView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MeshesView)
{
    ui->setupUi(this);
    meshSelected = -1;
    MorphSelected = -1;
    MaterialSelected = -1;
    BonesSelected = -1;
}
MeshesView::~MeshesView(){
    delete ui;
}

void MeshesView::addMeshes(granny_int32 MeshCount, granny_mesh ** Meshes){
    //Verifie le pointeur vers le tableau de Meshes
    if(Meshes == nullptr){
        return;
    }
    //Variable iteration Meshes
    granny_int32 index;
    for(index = 0; index < MeshCount;index++){
        //Pour tout les modèles du tableau
        try{
            if(Meshes[index] != nullptr){
                //Si le pointeur est valid
                try {
                    //stock le pointeur dans une variable locale
                    granny_mesh * curMeshes = Meshes[index];
                    if(curMeshes != nullptr){
                        //Si le pointeur du modèle est valide on recupère le nombre de ligne
                        int row = ui->tableWidget_meshes->rowCount();
                        //on ajoute une ligne et on increment le nombre de ligne
                        ui->tableWidget_meshes->insertRow(row);
                        QString meshesName;
                        //Recupère le nombre de colone
                        //int column = ui->tableWidget_meshes->columnCount();
                        QTableWidgetItem *newItem = nullptr;
                        if(curMeshes->Name != nullptr){
                            //Si le nom du modèle est valide
                            meshesName = QString::fromLocal8Bit(curMeshes->Name);
                        }
                        else{
                            //Si le nom du modèle n'est pas valide on genere un nom
                            meshesName = QString::number((int)curMeshes, 16);
                        }
                        //Crée l'objet représentant le modèle dans la liste
                        newItem = new QTableWidgetItem(meshesName);
                        //Ajoute le modèle à la liste
                        ui->tableWidget_meshes->setItem(row, 0, newItem);
                        mapListMeshes[row] = curMeshes;
                    }
                }catch (...){
                    log += "Error accessing meshes " + QString::number(index);
                }
            }
        }catch (...) {
            log += "Error accessing meshes table" + QString::number(index);
        }
    }
}
void MeshesView::resetMeshes(void){
    mapListMeshes.clear();
    ui->tableWidget_morph->clear();
    ui->tableWidget_morph->setHorizontalHeaderItem(0,new QTableWidgetItem("Morph"));

    ui->tableWidget_meshes->clear();
    ui->tableWidget_meshes->setHorizontalHeaderItem(0,new QTableWidgetItem("Name"));

    ui->tableWidget_material->clear();
    for(int i = ui->tableWidget_material->rowCount(); i >=0; i--){
        ui->tableWidget_material->removeRow(i);
    }
    ui->tableWidget_material->setHorizontalHeaderItem(0,new QTableWidgetItem("Materials"));

    ui->tableWidget_triangle->clear();
    ui->tableWidget_triangle->setHorizontalHeaderItem(0,new QTableWidgetItem("Value"));

    ui->tableWidget_biendedBone->clear();
    ui->tableWidget_biendedBone->setHorizontalHeaderItem(0,new QTableWidgetItem("Biended bones"));

    ui->treeWidget_material->clear();
    for(int i = ui->tableWidget_meshes->rowCount(); i >=0; i--){
        ui->tableWidget_meshes->removeRow(i);
    }
    for(int i = ui->tableWidget_biendedBone->rowCount(); i >=0; i--){
        ui->tableWidget_biendedBone->removeRow(i);
    }
    resetMeshesSelected();
    meshSelected = -1;
    MorphSelected = -1;
    MaterialSelected = -1;
    BonesSelected = -1;
    mapListMeshes.clear();
    mapListMorph.clear();
    mapListMaterial.clear();
    mapListBones.clear();

}
void MeshesView::resetMeshesSelected(void){
    ui->label_valName->setText("None");
    ui->label_valVertex->setText("None");
    ui->label_valTritopology->setText("None");
    resetMeshesSelectedMorph();
    resetMeshesSelectedBoneBinded();
}
void MeshesView::resetMeshesSelectedMorph(void){
    ui->label_morphName->setText("None");
    ui->label_valMorphVertex->setText("None");
    ui->label_valMorphDelta->setText("None");
}
void MeshesView::resetMeshesSelectedBoneBinded(void){
    ui->label_valBoneName->setText("None");
    ui->label_boneOBBmin0->setText("None");
    ui->label_boneOBBmin1->setText("None");
    ui->label_boneOBBmin2->setText("None");
    ui->label_boneOBBmax0->setText("None");
    ui->label_boneOBBmax1->setText("None");
    ui->label_boneOBBmax2->setText("None");
}

void MeshesView::loadMeshesSelected(granny_mesh *meshes){
    if(meshes == nullptr){
        return;
    }
    try {
        loadMeshesSelectedName(meshes);
        loadMeshesSelectedVertexAddr(meshes);
        loadMeshesSelectedTriTopo(meshes);
        loadMeshesSelectedMorph(meshes);
        loadMeshesSelectedMaterial(meshes);
        loadMeshesSelectedBoneBinded(meshes);
    } catch (...) {
        log += "Error accessing meshes";
    }
}
//Affiche le nom de la mesh selectionné
void MeshesView::loadMeshesSelectedName(granny_mesh *meshes){
    if(meshes == nullptr){
        return;
    }
    try {
        if(meshes->Name == nullptr){
            ui->label_valName->setText(QString::number((int)meshes, 16));
        }
        else {
            ui->label_valName->setText(meshes->Name);
        }
    } catch (...) {
        log += "Error accessing meshes";
    }
}
//Affiche l'addresse des vertex du mesh selectionné
void MeshesView::loadMeshesSelectedVertexAddr(granny_mesh *meshes){
    if(meshes == nullptr){
        return;
    }
    try {
        if(meshes->PrimaryVertexData == nullptr){
            ui->label_valVertex->setText("nullptr");
        }
        else {
            ui->label_valVertex->setText(QString::number((int)meshes->PrimaryVertexData, 16));
        }
    } catch (...) {
        log += "Error accessing meshes";
    }
}
//Affiche l'addresse de la tritopology de la meshe selectionné
void MeshesView::loadMeshesSelectedTriTopo(granny_mesh *meshes){
    if(meshes == nullptr){
        return;
    }
    try {
        if(meshes->PrimaryTopology == nullptr){
            ui->label_valTritopology->setText("nullptr");
        }
        else {
            ui->label_valTritopology->setText(QString::number((int)meshes->PrimaryVertexData, 16));
        }
    } catch (...) {
        log += "Error accessing meshes";
    }
}
//Charge la liste les morph target
void MeshesView::loadMeshesSelectedMorph(granny_mesh *meshes){
    if(meshes == nullptr){
        return;
    }
    try {
        for(int i = ui->tableWidget_morph->rowCount(); i >= 0; i--){
            ui->tableWidget_morph->removeRow(i);
        }
        for(granny_int32 i = 0; i < meshes->MorphTargetCount; i++){
            int row = ui->tableWidget_morph->rowCount();
            ui->tableWidget_morph->insertRow(row);
            QTableWidgetItem *newItem = new QTableWidgetItem();
            if(meshes->MorphTargets[i].ScalarName != nullptr){
                newItem->setText(meshes->MorphTargets[i].ScalarName);
            }
            else {
                newItem->setText(QString::number((int)&meshes->MorphTargets[i], 16));
            }
            ui->tableWidget_morph->setItem(row, 0, newItem);
            mapListMorph[row] = &meshes->MorphTargets[i];
        }
    } catch (...) {
        log += "Error accessing meshes";
    }
}
//Affiche le morph selectionné
void MeshesView::loadMeshesSelectedMorphSelected(granny_morph_target *morph){
    if(morph == nullptr){
        return;
    }
    try {
        loadMeshesSelectedMorphName(morph);
        loadMeshesSelectedMorphVertex(morph);
        loadMeshesSelectedMorphDelta(morph);
    } catch (...) {
        log += "Error accessing morph";
    }
}
void MeshesView::loadMeshesSelectedMorphName(granny_morph_target * morph){
    if(morph == nullptr){
        return;
    }
    try {
        if(morph->ScalarName == nullptr){
            ui->label_morphName->setText(QString::number((int) morph, 16));
        }
        else {
            ui->label_morphName->setText(morph->ScalarName);
        }
    } catch (...) {
        log += "Error accessing morph name";
    }
}
void MeshesView::loadMeshesSelectedMorphVertex(granny_morph_target * morph){
    if(morph == nullptr){
        return;
    }
    try {
        ui->label_valMorphVertex->setText(QString::number((int) morph->VertexData, 16));
    } catch (...) {
        log += "Error accessing morph vertex";
    }
}
void MeshesView::loadMeshesSelectedMorphDelta(granny_morph_target * morph){
    if(morph == nullptr){
        return;
    }
    try {
        ui->label_valMorphDelta->setText(QString::number(morph->DataIsDeltas));
    } catch (...) {
        log += "Error accessing morph delta";
    }
}
//Charge la liste des materiaux bionded
void MeshesView::loadMeshesSelectedMaterial(granny_mesh *meshes){
    if(meshes == nullptr){
        return;
    }
    if(meshes->MaterialBindings == nullptr){
        return;
    }
    try {
        for(int i = ui->tableWidget_material->rowCount(); i >= 0; i--){
            ui->tableWidget_material->removeRow(i);
        }
        for(granny_int32 i = 0; i < meshes->MaterialBindingCount; i++){
            int row = ui->tableWidget_material->rowCount();
            ui->tableWidget_material->insertRow(row);
            if(meshes->MaterialBindings[i].Material == nullptr){
                continue;
            }
            QTableWidgetItem *newItem = new QTableWidgetItem();
            if(meshes->MaterialBindings[i].Material->Name != nullptr){
                newItem->setText(meshes->MaterialBindings[i].Material->Name);
            }
            else {
                newItem->setText(QString::number((int)&meshes->MaterialBindings[i], 16));
            }
            ui->tableWidget_material->setItem(row, 0, newItem);
            mapListMaterial[row] = &meshes->MaterialBindings[i];

            ui->treeWidget_material->clear();
            try {
                QTreeWidgetItem * curItem = MaterialsView::addElementOnTree(meshes->MaterialBindings[i].Material);
                ui->treeWidget_material->addTopLevelItem(curItem);
            } catch (...) {
                log += "Error accessing materials table" + QString::number(i);
            }

        }

    } catch (...) {
        log += "Error accessing meshes";
    }
}
void MeshesView::loadMeshesSelectedMaterialSelected(granny_material_binding * material){

}
void MeshesView::loadMeshesSelectedMaterialSelectedName(granny_material_binding * material){

}
void MeshesView::loadMeshesSelectedBoneBinded(granny_mesh *meshes){
    if(meshes == nullptr){
        return;
    }
    if(meshes->BoneBindings == nullptr){
        return;
    }
    try{
        for(int i = ui->tableWidget_biendedBone->rowCount(); i >= 0; i--){
            ui->tableWidget_biendedBone->removeRow(i);
        }
        for(granny_int32 i = 0; i < meshes->BoneBindingCount; i++){
            int row = ui->tableWidget_biendedBone->rowCount();
            ui->tableWidget_biendedBone->insertRow(row);
            QTableWidgetItem *newItem = new QTableWidgetItem();
            if(meshes->BoneBindings[i].BoneName != nullptr){
                newItem->setText(meshes->BoneBindings[i].BoneName);
            }
            else {
                newItem->setText(QString::number((int)&meshes->BoneBindings[i], 16));
            }
            ui->tableWidget_biendedBone->setItem(row, 0, newItem);
            mapListBones[row] = &meshes->BoneBindings[i];
        }
    } catch (...) {
        log += "Error accessing meshes";
    }
}
void MeshesView::loadMeshesSelectedBoneBindedSelected(granny_bone_binding *boneBinding){
    if(boneBinding == nullptr){
        return;
    }
    try {
        loadMeshesSelectedBoneBindedSelectedName(boneBinding);
        loadMeshesSelectedBoneBindedSelectedOBB(boneBinding);
        loadMeshesSelectedBoneBindedSelectedTriangle(boneBinding);
    } catch (...) {
        log += "Error accessing BoneBinded";
    }
}
void MeshesView::loadMeshesSelectedBoneBindedSelectedName(granny_bone_binding *boneBinding){
    if(boneBinding == nullptr){
        return;
    }
    try {
        if(boneBinding->BoneName == nullptr){
            ui->label_valBoneName->setText(QString::number((int)boneBinding, 16));
        }
        else{
            ui->label_valBoneName->setText(boneBinding->BoneName);
        }
    } catch (...) {
        log += "Error accessing BoneBinded OBB";
    }
}
void MeshesView::loadMeshesSelectedBoneBindedSelectedOBB(granny_bone_binding *boneBinding){
    if(boneBinding == nullptr){
        return;
    }
    try {
        loadMeshesSelectedBoneBindedSelectedOBBmin(boneBinding);
        loadMeshesSelectedBoneBindedSelectedOBBmax(boneBinding);
    } catch (...) {
        log += "Error accessing BoneBinded OBB";
    }
}
void MeshesView::loadMeshesSelectedBoneBindedSelectedOBBmin(granny_bone_binding *boneBinding){
    if(boneBinding == nullptr){
        return;
    }
    try {
        ui->label_boneOBBmin0->setText(QString::number(boneBinding->OBBMin[0]));
        ui->label_boneOBBmin1->setText(QString::number(boneBinding->OBBMin[1]));
        ui->label_boneOBBmin2->setText(QString::number(boneBinding->OBBMin[2]));
    } catch (...) {
        log += "Error accessing BoneBinded OBB";
    }
}
void MeshesView::loadMeshesSelectedBoneBindedSelectedOBBmax(granny_bone_binding *boneBinding){
    if(boneBinding == nullptr){
        return;
    }
    try {
        ui->label_boneOBBmax0->setText(QString::number(boneBinding->OBBMax[0]));
        ui->label_boneOBBmax1->setText(QString::number(boneBinding->OBBMax[1]));
        ui->label_boneOBBmax2->setText(QString::number(boneBinding->OBBMax[2]));
    } catch (...) {
        log += "Error accessing BoneBinded OBB";
    }
}
void MeshesView::loadMeshesSelectedBoneBindedSelectedTriangle(granny_bone_binding *boneBinding){
    if(boneBinding == nullptr){
        return;
    }
    try {
        for(granny_int32 i = 0; i < boneBinding->TriangleCount; i++){
            int row = ui->tableWidget_triangle->rowCount();
            ui->tableWidget_triangle->insertRow(row);
            QTableWidgetItem *newItem = nullptr;
            newItem = new QTableWidgetItem();
            newItem->setText(QString::number(boneBinding->TriangleIndices[i]));
            ui->tableWidget_triangle->setItem(row, 0, newItem);
        }
    } catch (...) {
        log += "Error accessing Triangle Bone Binded Selected";
    }
}
void MeshesView::on_tableWidget_meshes_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(meshSelected != row){
        meshSelected = row;
        loadMeshesSelected(mapListMeshes[row]);
    }
}
void MeshesView::on_tableWidget_meshes_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(currentRow == previousRow){
        return;
    }
    if(currentColumn < 0){
        if(previousColumn < 0){
            return;
        }
        return;
    }
    if(meshSelected != currentRow){
        meshSelected = currentRow;
        loadMeshesSelected(mapListMeshes[currentRow]);
    }
}
void MeshesView::on_tableWidget_morph_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(MorphSelected != row){
        MorphSelected = row;
        loadMeshesSelectedMorphSelected(mapListMorph[row]);
    }
}
void MeshesView::on_tableWidget_morph_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(currentRow == previousRow){
        return;
    }
    if(currentColumn < 0){
        if(previousColumn < 0){
            return;
        }
        return;
    }
    if(MorphSelected != currentRow){
        MorphSelected = currentRow;
        loadMeshesSelectedMorphSelected(mapListMorph[currentRow]);
    }
}
void MeshesView::on_tableWidget_material_cellChanged(int row, int column){
    if(column < 0){
        return;
    }
    if(MaterialSelected != row){
        MaterialSelected = row;
        loadMeshesSelectedMaterialSelected(mapListMaterial[row]);
    }
}
void MeshesView::on_tableWidget_material_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(currentRow == previousRow){
        return;
    }
    if(currentColumn < 0){
        if(previousColumn < 0){
            return;
        }
        return;
    }
    if(MaterialSelected != currentRow){
        MaterialSelected = currentRow;
        loadMeshesSelectedMaterialSelected(mapListMaterial[currentRow]);
    }
}
void MeshesView::on_tableWidget_biendedBone_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(BonesSelected != row){
        BonesSelected = row;
        loadMeshesSelectedBoneBindedSelected(mapListBones[row]);
    }
}
void MeshesView::on_tableWidget_biendedBone_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(currentRow == previousRow){
        return;
    }
    if(currentColumn < 0){
        if(previousColumn < 0){
            return;
        }
        return;
    }
    if(BonesSelected != currentRow){
        BonesSelected = currentRow;
        loadMeshesSelectedBoneBindedSelected(mapListBones[currentRow]);
    }
}
