#include "modelview.h"
#include "ui_modelview.h"

ModelView::ModelView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ModelView)
{
    ui->setupUi(this);
    scaleMatrix = new Matrix3x3();
    ui->gridLayout_initPlacment->addWidget(scaleMatrix, 3,1,1,1);
    modelSelected = -1;
}
ModelView::~ModelView(){
    delete ui;
    delete scaleMatrix;
}
//Ajoute un modele a la liste des modeles
void ModelView::addModels(granny_int32 ModelCount, granny_model ** Models){
    //Verifie le pointeur vers le tableau de modeles
    if(Models == nullptr){
        return;
    }
    //Variable iteration model
    granny_int32 indexModel;
    for(indexModel = 0; indexModel < ModelCount;indexModel++){
        //Pour tout les modèles du tableau
        try {
            if(Models[indexModel] != nullptr){
                //Si le pointeur est valid
                try {
                    //stock le pointeur dans une variable locale
                    granny_model * curModel = Models[indexModel];
                    if(curModel != nullptr){
                        //Si le pointeur du modèle est valide on recupère le nombre de ligne
                        int row = ui->tableWidget_model->rowCount();
                        //on ajoute une ligne et on increment le nombre de ligne
                        ui->tableWidget_model->insertRow(row);
                        QString ModelName;
                        //Recupère le nombre de colone
                        //int column = ui->tableWidget_model->columnCount();
                        QTableWidgetItem *newItem = nullptr;
                        if(curModel->Name != nullptr){
                            //Si le nom du modèle est valide
                            ModelName = QString::fromLocal8Bit(curModel->Name);
                        }
                        else{
                            //Si le nom du modèle n'est pas valide on genere un nom
                            ModelName = QString::number((int)curModel, 16);
                        }
                        //Crée l'objet représentant le modèle dans la liste
                        newItem = new QTableWidgetItem(ModelName);
                        //Ajoute le modèle à la liste
                        ui->tableWidget_model->setItem(row, 0, newItem);
                        mapListModel[row] = curModel;
                    }
                }catch (...) {
                    log += "Error accessing models " + QString::number(indexModel) + "\n";
                }
            }
        } catch (...) {
            log += "Error accessing models tables " + QString::number(indexModel) + "\n";
        }
    }
}
void ModelView::resetModels(void){
    modelSelected = -1;
    mapListModel.clear();
    for(int i = ui->tableWidget_model->rowCount(); i >= 0; i --){
        ui->tableWidget_model->removeRow(i);
    }
    ui->tableWidget_model->clear();
    ui->tableWidget_model->setHorizontalHeaderItem(0,new QTableWidgetItem("Name"));

    ui->label_valModelSelectedName->setText("None");
    ui->comboBox_modelSkeletonSelected->clear();
    ui->comboBox_modelMeshSelected->clear();
    scaleMatrix->reset();
    resetInitPlacment();
    resetMeshBinded();
    resetSkeleton();
}
void ModelView::resetInitPlacment(void){
    this->ui->label_valInitPlacmentFlag->setText("None");
    ui->label_initPlacmentPos1->setText("None");
    ui->label_initPlacmentPos2->setText("None");
    ui->label_initPlacmentPos3->setText("None");
    ui->label_valInitPlacmentOri1->setText("None");
    ui->label_valInitPlacmentOri2->setText("None");
    ui->label_valInitPlacmentOri3->setText("None");
    ui->label_valInitPlacmentOri4->setText("None");
    scaleMatrix->reset();
}
void ModelView::resetMeshBinded(void){
    ui->comboBox_modelMeshSelected->clear();
    ui->label_valMeshSelectedName->setText("None");
    ui->label_valMeshSelectedMorphCount->setText("None");
    ui->label_valMeshSelectedMaterial->setText("None");
    ui->label_valMeshSelectedBoneBinded->setText("None");
}
void ModelView::resetSkeleton(void){
    ui->comboBox_modelSkeletonSelected->clear();

    ui->label_valModelSkeletonSelectedName->setText("None");
    ui->label_valModelSkeletonSelectedBone->setText("None");
    ui->label_valModelSkeletonSelectedLOD->setText("None");
}
//Lors d'un clique sur un modele de la liste
void ModelView::on_tableWidget_model_cellClicked(int row, int column){
    if(modelSelected != row){
        loadModel(row);
        modelSelected = row;
    }
}
//Lorsque l'utilisateur selectionne une mesh binded
void ModelView::on_comboBox_modelMeshSelected_currentIndexChanged(int index){

}
void ModelView::on_comboBox_modelMeshSelected_activated(int index){
    try {
        if(index > (int)mapListModel[modelSelected]->MeshBindingCount){
            return;
        }
        loadMeshBindedSelected(&mapListModel[modelSelected]->MeshBindings[index]);
    } catch (...) {
        log += "Erreur lecture meshBinded selectionner\n";
    }
}
//Charge un modele de la liste
void ModelView::loadModel(int key){
    granny_model * ptrModel = this->mapListModel[key];
    if(ptrModel == nullptr){
        return;
    }
    //Affiche le nom du modele selectionne dans la liste
    loadModelName(ptrModel);
    //Affiche le placement initial du model
    loadInitPlacment(ptrModel);
    //Charge la liste des skeleton du modele selectionne
    loadModelSkeleton(ptrModel);
    //Charge la liste des MeshBinded du modele selectionne
    loadMeshBinded(ptrModel);
}
//Affiche le nom du modele selectionne
void ModelView::loadModelName(granny_model * model){
    if(model == nullptr){
        return;
    }
    try {
        if(model->Name == nullptr){
            return;
        }
        ui->label_valModelSelectedName->setText(model->Name);
    } catch (...) {
        log += "Erreur lecture paramètre Name du modele selectionne\n";
    }
}
//Charge les combox box
void ModelView::loadModelSkeleton(granny_model * model){
    ui->comboBox_modelSkeletonSelected->clear();
    if(model == nullptr){
        return;
    }
    if(model->Skeleton == nullptr){
        return;
    }
    try {
        if(model->Skeleton->Name == nullptr){
            ui->comboBox_modelSkeletonSelected->insertItem(ui->comboBox_modelSkeletonSelected->count()-1,
                                                           "0x"+QString::number((int)model->Skeleton, 16));
        }
        else {
            ui->comboBox_modelSkeletonSelected->insertItem(ui->comboBox_modelSkeletonSelected->count()-1,
                                                           model->Skeleton->Name);
        }
        ui->comboBox_modelSkeletonSelected->setCurrentIndex(0);
        loadSkeletonSelected(model->Skeleton);
    } catch (...) {
        log += "Erreur lecture Skeleton modele selectionne\n";
    }
}
void ModelView::loadMeshBinded(granny_model * model){
    ui->comboBox_modelMeshSelected->clear();
    if(model == nullptr){
        return;
    }
    if(model->MeshBindings == nullptr){
        return;
    }
    try {
        granny_int32 index;
        for(index = 0; index < model->MeshBindingCount; index++){
            if(model->MeshBindings->Mesh->Name == nullptr){
                ui->comboBox_modelMeshSelected->insertItem(ui->comboBox_modelSkeletonSelected->count(),
                                                               "0x"+QString::number((int)model->MeshBindings[index].Mesh, 16));
            }
            else {
                ui->comboBox_modelMeshSelected->insertItem(ui->comboBox_modelMeshSelected->count(), model->MeshBindings[index].Mesh->Name);
            }
        }
        ui->comboBox_modelMeshSelected->setCurrentIndex(0);
        if(index>=1){
            loadMeshBindedSelected(&model->MeshBindings[0]);
        }
    } catch (...) {
        log += "Erreur lecture MeshBindings modele selectionne\n";
    }

}
//Fonction d'affichage du placement initial
void ModelView::loadInitPlacment(granny_model * model){
    if(model == nullptr){
        return;
    }
    loadInitPlacmentFlags(model);
    loadInitPlacmentPos(model);
    loadInitPlacmentOri(model);
    loadInitPlacmentScale(model);
}
void ModelView::loadInitPlacmentFlags(granny_model * model){
    if(model == nullptr){
        return;
    }
    try {
        QString strFlag;
        switch (model->InitialPlacement.Flags) {
            case GrannyHasPosition:{
                strFlag = "GrannyHasPosition";
                break;
            }
            case GrannyHasOrientation:{
                strFlag = "GrannyHasOrientation";
                break;
            }
            case GrannyHasScaleShear:{
                strFlag = "GrannyHasScaleShear";
                break;
            }
            case Grannytransform_flags_forceint:{
                strFlag = "Grannytransform_flags_forceint";
                break;
            }
            default:{
                strFlag = "Unkown flag (" + QString::number(model->InitialPlacement.Flags) + ")";
            }
        }
        this->ui->label_valInitPlacmentFlag->setText(strFlag);
    } catch (...) {
        log += "Erreur lecture flags initial placment du modele selectionne\n";
    }
}
void ModelView::loadInitPlacmentPos(granny_model * model){
    if(model == nullptr){
        return;
    }
    try {
        ui->label_initPlacmentPos1->setText(QString::number(model->InitialPlacement.Position[0]));
        ui->label_initPlacmentPos2->setText(QString::number(model->InitialPlacement.Position[1]));
        ui->label_initPlacmentPos3->setText(QString::number(model->InitialPlacement.Position[2]));
    } catch (...) {
        log += "Erreur lecture position initial placment du modele selectionne\n";
    }
}
void ModelView::loadInitPlacmentOri(granny_model * model){
    if(model == nullptr){
        return;
    }
    try {
        ui->label_valInitPlacmentOri1->setText(QString::number(model->InitialPlacement.Orientation[0]));
        ui->label_valInitPlacmentOri2->setText(QString::number(model->InitialPlacement.Orientation[1]));
        ui->label_valInitPlacmentOri3->setText(QString::number(model->InitialPlacement.Orientation[2]));
        ui->label_valInitPlacmentOri4->setText(QString::number(model->InitialPlacement.Orientation[3]));
    } catch (...) {
        log += "Erreur lecture orientation initial placment du modele selectionne\n";
    }
}
void ModelView::loadInitPlacmentScale(granny_model * model){
    if(model == nullptr){
        return;
    }
    try {
        scaleMatrix->setValues(model->InitialPlacement.ScaleShear);
    } catch (...) {
        log += "Erreur lecture ScaleShear initial placment du modele selectionne\n";
    }
}
//Fonction d'affichage du skelete selectionnné
void ModelView::loadSkeletonSelected(granny_skeleton * skeleton){
    if(skeleton == nullptr){
        return;
    }
    try {
        loadSkeletonSelectedName(skeleton);
        loadSkeletonSelectedBoneCount(skeleton);
        loadSkeletonSelectedLODType(skeleton);
    } catch (...) {
        log += "Erreur lecture skeleton selectionne du modele selectionne\n";
    }
}
void ModelView::loadSkeletonSelectedName(granny_skeleton * skeleton){
    if(skeleton == nullptr){
        return;
    }
    try {
        if(skeleton->Name == nullptr){
            ui->label_valModelSkeletonSelectedName->setText("0x"+QString::number((int)skeleton, 16));
        }
        else {
            ui->label_valModelSkeletonSelectedName->setText(skeleton->Name);
        }
    } catch (...) {
        log += "Erreur lecture name skeleton selectionne du modele selectionne\n";
    }
}
void ModelView::loadSkeletonSelectedBoneCount(granny_skeleton * skeleton){
    if(skeleton == nullptr){
        return;
    }
    try {
        ui->label_valModelSkeletonSelectedBone->setText(QString::number(skeleton->BoneCount));
    } catch (...) {
        log += "Erreur lecture BoneCount skeleton selectionne du modele selectionne\n";
    }
}
void ModelView::loadSkeletonSelectedLODType(granny_skeleton * skeleton){
    if(skeleton == nullptr){
        return;
    }
    try {
        ui->label_valModelSkeletonSelectedLOD->setText(QString::number(skeleton->LODType));
    } catch (...) {
        log += "Erreur lecture BoneCount skeleton selectionne du modele selectionne\n";
    }
}
//Fonction d'affichage de la mesh selectionné
void ModelView::loadMeshBindedSelected(granny_model_mesh_binding * meshBinded){
    if(meshBinded == nullptr){
        return;
    }
    try {
        loadMeshBindedSelectedName(meshBinded);
        loadMeshBindedSelectedMorphCount(meshBinded);
        loadMeshBindedSelectedMaterialBindedCount(meshBinded);
        loadMeshBindedSelectedBoneBindedCount(meshBinded);
    } catch (...) {
        log += "Erreur lecture mesh binded selectionné du modele selectionne\n";
    }
}
void ModelView::loadMeshBindedSelectedName(granny_model_mesh_binding * meshBinded){
    if(meshBinded == nullptr){
        return;
    }
    try {
        ui->label_valMeshSelectedName->setText(meshBinded->Mesh->Name);
    } catch (...) {
        log += "Erreur lecture Name mesh binded selectionné du modele selectionne\n";
    }
}
void ModelView::loadMeshBindedSelectedMorphCount(granny_model_mesh_binding * meshBinded){
    if(meshBinded == nullptr){
        return;
    }
    try {
        ui->label_valMeshSelectedMorphCount->setText(QString::number(meshBinded->Mesh->MorphTargetCount));
    } catch (...) {
        log += "Erreur lecture MorphCoun mesh binded selectionné du modele selectionne\n";
    }
}
void ModelView::loadMeshBindedSelectedMaterialBindedCount(granny_model_mesh_binding * meshBinded){
    if(meshBinded == nullptr){
        return;
    }
    try {
        ui->label_valMeshSelectedMaterial->setText(QString::number(meshBinded->Mesh->MaterialBindingCount));
    } catch (...) {
        log += "Erreur lecture BindedCount mesh binded selectionné du modele selectionne\n";
    }
}
void ModelView::loadMeshBindedSelectedBoneBindedCount(granny_model_mesh_binding * meshBinded){
    if(meshBinded == nullptr){
        return;
    }
    try {
        ui->label_valMeshSelectedBoneBinded->setText(QString::number(meshBinded->Mesh->BoneBindingCount));
    } catch (...) {
        log += "Erreur lecture BoneBindedCount mesh binded selectionné du modele selectionne\n";
    }
}


void ModelView::on_tableWidget_model_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(currentColumn < 0){
        return;
    }
    if(currentRow == previousRow){
        //anti warning
        if(previousColumn)
            return;
        return;
    }
    if(currentRow != modelSelected){
        modelSelected = currentRow;
        loadModel(currentRow);
    }
}
