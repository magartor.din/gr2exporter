#include "arttoolviewer.h"
#include "ui_arttoolviewer.h"

ArtToolViewer::ArtToolViewer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ArtToolViewer)
{
    ui->setupUi(this);
    artToolSelected = -1;
}

ArtToolViewer::~ArtToolViewer()
{
    delete ui;
}

void ArtToolViewer::addArtTool(granny_art_tool_info * artTool){
    if(artTool == nullptr){
        return;
    }
    try {
        int row = ui->tableWidget_arttool->rowCount();
        ui->tableWidget_arttool->insertRow(row);
        ui->tableWidget_arttool->setItem(row, 0, new QTableWidgetItem(artTool->FromArtToolName));
        mapListArtTool[row] = artTool;
    } catch (...) {
        log += "Error accessing art tool";
    }
}

void ArtToolViewer::resetArtTool(void){
    ui->label_valArtToolName->setText("None");
    ui->label_valMajorVersion->setText("None");
    ui->label_valMinorVersion->setText("None");
    ui->label_valPointeurSize->setText("None");
    ui->label_valUnitPerMeter->setText("None");
    ui->label_valOrigin->setText("None");
    ui->label_valRightVector->setText("None");
    ui->label_valUpVector->setText("None");
    ui->label_valBackVector->setText("None");
    artToolSelected = -1;
    mapListArtTool.clear();
    for(int i = ui->tableWidget_arttool->rowCount(); i >= 0; i--){
        ui->tableWidget_arttool->removeRow(i);
    }
}
void ArtToolViewer::loadArtToolSelected(granny_art_tool_info * artTool){
    if(artTool == nullptr){
        return;
    }
    try {
        loadArtToolSelectedName(artTool);
        loadArtToolSelectedMajorVersion(artTool);
        loadArtToolSelectedMinorVersion(artTool);
        loadArtToolSelectedPointerSize(artTool);
        loadArtToolSelectedUnitPerMeter(artTool);
        loadArtToolSelectedOrigin(artTool);
        loadArtToolSelectedRightVector(artTool);
        loadArtToolSelectedUpVector(artTool);
        loadArtToolSelectedBackVector(artTool);
    } catch (...) {
        log += "Error accessing art tool";
    }
}
void ArtToolViewer::loadArtToolSelectedName(granny_art_tool_info * artTool){
    if(artTool == nullptr){
        return;
    }
    try {
        ui->label_valArtToolName->setText(artTool->FromArtToolName);
    } catch (...) {
        log += "Error accessing art tool";
    }
}
void ArtToolViewer::loadArtToolSelectedMajorVersion(granny_art_tool_info * artTool){
    if(artTool == nullptr){
        return;
    }
    try {
        ui->label_valMajorVersion->setText(QString::number(artTool->ArtToolMajorRevision));
    } catch (...) {
        log += "Error accessing art tool";
    }
}
void ArtToolViewer::loadArtToolSelectedMinorVersion(granny_art_tool_info * artTool){
    if(artTool == nullptr){
        return;
    }
    try {
        ui->label_valMinorVersion->setText(QString::number(artTool->ArtToolMinorRevision));
    } catch (...) {
        log += "Error accessing art tool";
    }
}
void ArtToolViewer::loadArtToolSelectedPointerSize(granny_art_tool_info * artTool){
    if(artTool == nullptr){
        return;
    }
    try {
        ui->label_valPointeurSize->setText(QString::number(artTool->ArtToolPointerSize));
    } catch (...) {
        log += "Error accessing art tool";
    }
}
void ArtToolViewer::loadArtToolSelectedUnitPerMeter(granny_art_tool_info * artTool){
    if(artTool == nullptr){
        return;
    }
    try {
        ui->label_valUnitPerMeter->setText(QString::number(artTool->UnitsPerMeter));
    } catch (...) {
        log += "Error accessing art tool";
    }
}
void ArtToolViewer::loadArtToolSelectedOrigin(granny_art_tool_info * artTool){
    if(artTool == nullptr){
        return;
    }
    try {
        QString str = QString::number(artTool->Origin[0]) + ", " +\
                      QString::number(artTool->Origin[1]) + ", " +\
                      QString::number(artTool->Origin[2]);

        ui->label_valOrigin->setText(str);
    } catch (...) {
        log += "Error accessing art tool";
    }
}
void ArtToolViewer::loadArtToolSelectedRightVector(granny_art_tool_info * artTool){
    if(artTool == nullptr){
        return;
    }
    try {
        QString str = QString::number(artTool->RightVector[0]) + ", " +\
                      QString::number(artTool->RightVector[1]) + ", " +\
                      QString::number(artTool->RightVector[2]);

        ui->label_valRightVector->setText(str);
    } catch (...) {
        log += "Error accessing art tool";
    }
}
void ArtToolViewer::loadArtToolSelectedUpVector(granny_art_tool_info * artTool){
    if(artTool == nullptr){
        return;
    }
    try {
        QString str = QString::number(artTool->UpVector[0]) + ", " +\
                      QString::number(artTool->UpVector[1]) + ", " +\
                      QString::number(artTool->UpVector[2]);

        ui->label_valUpVector->setText(str);
    } catch (...) {
        log += "Error accessing art tool";
    }
}
void ArtToolViewer::loadArtToolSelectedBackVector(granny_art_tool_info * artTool){
    if(artTool == nullptr){
        return;
    }
    try {
        QString str = QString::number(artTool->BackVector[0]) + ", " +\
                      QString::number(artTool->BackVector[1]) + ", " +\
                      QString::number(artTool->BackVector[2]);

        ui->label_valBackVector->setText(str);
    } catch (...) {
        log += "Error accessing art tool";
    }
}

void ArtToolViewer::ArtToolViewer::on_tableWidget_arttool_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(currentColumn < 0){
        if(previousColumn < 0){
            return;
        }
        return;
    }
    if(currentRow != previousRow){
        if(currentRow != artToolSelected){
            artToolSelected = currentRow;
            loadArtToolSelected(mapListArtTool[currentRow]);
        }
    }
}
void ArtToolViewer::on_tableWidget_arttool_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(row != artToolSelected){
        artToolSelected = row;
        loadArtToolSelected(mapListArtTool[row]);
    }
}
