#ifndef TRACKGROUPVIEW_H
#define TRACKGROUPVIEW_H

#include <QWidget>
#include <QTableView>

#include "matrix3x3.h"
#include "NBCpp/granny.h"
#include "GridLayoutUtil.h"
#include "curve2.h"

namespace Ui {
class TrackgroupView;
}

class TrackgroupView : public QWidget
{
    Q_OBJECT

public:
    explicit TrackgroupView(QWidget *parent = nullptr);
    ~TrackgroupView();
    void addTrackgroups(granny_int32 TrackGroupCount, granny_track_group ** TrackGroups);

    void resetTrackgroups(void);

    template<typename T>
    void loadTransformerTrackSelectedPos(T *curve);
    template<typename T>
    void loadTransformerTrackSelectedOri(T *curve);
    template<typename T>
    void loadTransformerTrackSelectedScale(T *curve);
    template<typename T>
    void loadTransformerTrackSelectedParam(T *curvePos,T *curveOri,T *curveScale);
public slots:
    //Affiche le trackgroup selectionné
    void loadTrackgroupSelected(granny_track_group *Trackgroups);
    //Affiche le flag du trackgroup selectionné
    void loadTrackgroupSelectedFlags(granny_track_group *Trackgroups);
    //Affiche la loop translation du trackgroup selectionné
    void loadTrackgroupSelectedLoopTranslation(granny_track_group *Trackgroups);
    //Affiche l'init placement du trackgroup selectionné
    void loadTrackgroupSelectedInitPlacment(granny_track_group *Trackgroups);
    //Affiche le flag de l'init placement
    void loadTrackgroupSelectedInitPlacmentFlags(granny_track_group *Trackgroups);
    //Affiche la position de l'init placement
    void loadTrackgroupSelectedInitPlacmentPos(granny_track_group *Trackgroups);
    //Affiche l'origine de l'init placement
    void loadTrackgroupSelectedInitPlacmentOri(granny_track_group *Trackgroups);
    //Affiche la scale de l'init placement
    void loadTrackgroupSelectedInitPlacmentScales(granny_track_group *Trackgroups);
    //Affiche la periodic loop du trackgroup selectionné
    void loadTrackgroupSelectedPeriodicLoop(granny_track_group *Trackgroups);
    //Affiche le radius de la periodic loop
    void loadTrackgroupSelectedPeriodicLoopRadius(granny_track_group *Trackgroups);
    //Affiche le DAngle de la periodic loop
    void loadTrackgroupSelectedPeriodicLoopDAngle(granny_track_group *Trackgroups);
    //Affiche le DZ de la periodic loop
    void loadTrackgroupSelectedPeriodicLoopDZ(granny_track_group *Trackgroups);
    //Affiche le BasisX de la periodic loop
    void loadTrackgroupSelectedPeriodicLoopBasisX(granny_track_group *Trackgroups);
    //Affiche le BasisY de la periodic loop
    void loadTrackgroupSelectedPeriodicLoopBasisY(granny_track_group *Trackgroups);
    //Affiche le Axis de la periodic loop
    void loadTrackgroupSelectedPeriodicLoopAxis(granny_track_group *Trackgroups);
    //Charge les vecteurs trackgroup dans le tableau
    void loadVectorTrack(granny_track_group *Trackgroups);
    void loadVectorTrackSelected(granny_vector_track *VectorTrack);
    //Charge les tranform track dans le tableau
    void loadTransformerTrack(granny_track_group *Trackgroups);
    void loadTransformerTrackSelected(granny_transform_track *TransformTrack);

    //Charge les track LOD dans le tableau
    void loadTrackLOD(granny_track_group *Trackgroups);
    //Charge les text track dans le tableau
    void loadTextTrack(granny_track_group *Trackgroups);
    //Charge les info du text track selectionné
    void loadTextTrackSelected(granny_text_track *TextTrack);

    void resetTransformerTrack();
    void resetTrackLOD();
    void resetTextTrack();
    void resetVectorTrack();

private slots:
    void on_tableWidget_trackgroups_cellClicked(int row, int column);

    void on_tableWidget_trackgroups_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

    void on_tableWidget_vectorTrack_cellClicked(int row, int column);

    void on_tableWidget_vectorTrack_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

    void on_tableWidget_trackgroupsTransform_cellClicked(int row, int column);

    void on_tableWidget_trackgroupsTransform_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

    void on_tableWidget_trackgroupsText_cellClicked(int row, int column);


    void on_tableWidget_trackgroupsText_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

private:
    Ui::TrackgroupView *ui;
    Matrix3x3 * matrixInitPlacmentScale;
    QString log;


    std::map<int, granny_track_group *> mapListTrackgroup;
    std::map<int, granny_vector_track *> mapListVectorTrack;
    std::map<int, granny_transform_track *> mapListTransformTrack;
    std::map<int, granny_text_track *> mapListTextTrack;

    curve2 * vectorCurve;

    curve2 * transformTrackPosCurve;
    curve2 * transformTrackOriCurve;
    curve2 * transformTrackScaleCurve;

    int TrackgroupSelected;
    int VectorSelected;
    int TransformSelected;
    int TextSelected;
};


#endif // TRACKGROUPVIEW_H
