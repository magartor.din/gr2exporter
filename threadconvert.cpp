#include "threadconvert.h"
#include <QDir>
#include <QProcess>
#include <QtCore>

ThreadConvert::ThreadConvert(QString dirName, QStringList &msmFile, QStringList &txtFile, FileConfig configObj):
    m_dirName(dirName), m_msmFile(msmFile), m_txtFile(txtFile), m_configObj(configObj)
{


}
bool ThreadConvert::readTxtFileSA(std::string fileNameStd, Exporter * exporterObjSA){
    QString fileName = QString::fromStdString(fileNameStd);
    QFile txtFile(fileName);
    if(txtFile.open(QIODevice::ReadOnly | QIODevice::Text) == false){
        //Handle error
        return false;
    }
    std::string FolderFile(ThreadConvert::getFolderFromPath(fileName.toStdString()));

    std::vector<std::string> animFilesSA;
    std::vector<std::string> animNameSA;
    while (!txtFile.atEnd()) {
        //Get new line
        QByteArray line = txtFile.readLine();
        //GENERAL NAME file.msa number
        //Split the line
        std::string text = line.data();
        std::istringstream iss(text);
        std::vector<std::string> textSplited(std::istream_iterator<std::string>{iss},
                                         std::istream_iterator<std::string>());
        std::string msaPath(FolderFile + textSplited.at(2));
        //Check if msa file exist
        if(QFile::exists(QString::fromStdString(msaPath)) == false){
            //File doesn't ignore the line
            continue;
        }
        //msa file found
        QFile msaFile(QString::fromStdString(msaPath));
        //Try open it
        if(msaFile.open(QIODevice::ReadOnly | QIODevice::Text) == false){
            //cannot open the msa file, ignore it
            continue;
        }
        //msa file open
        bool foundMotionFileName = false;
        std::string motionFileName;

        while (!msaFile.atEnd()) {
            //Parse file until get MotionFileName
            std::string msaText = msaFile.readLine().data();
            if(strncmp(msaText.c_str(), m_configObj.nameFieldAnimTxt.c_str(), m_configObj.nameFieldAnimTxt.size()) != 0){
                //It's not so gonext line
                continue;
            }
            //MotionFileName found
            //Skip MotionFileName
            const char * lineData = msaText.c_str();
            int indexLine = 0;
            int length = msaText.size();

            while((lineData[indexLine] != '\"')&&(indexLine < length)){
                indexLine++;
            }
            indexLine++;
            //Get content of " caracters
            while((lineData[indexLine] != '\"')&&(indexLine < length)){
                motionFileName.push_back(lineData[indexLine++]);
            }
            //Get file name
            motionFileName = ThreadConvert::getFileNameFromPath(motionFileName);
            //Add current directory
            motionFileName = FolderFile + motionFileName;
            foundMotionFileName = true;
            break;
        }
        msaFile.close();
        //Check if field motionFileName found
        if(!foundMotionFileName){
            continue;   //If not ignore the line
        }
        animFilesSA.push_back(motionFileName);
        animNameSA.push_back(textSplited.at(1));
    }
    //Close the txt file
    txtFile.close();

    while(animFilesSA.size() != 0){
        //For all animation file found
        //Reset memory
        if(exporterObjSA->m_animFileInfo != nullptr){
            GrannyFreeFile(exporterObjSA->m_grannyAnimFile);
            exporterObjSA->m_animFileInfo = nullptr;
        }

        //Initiate value of exporter
        exporterObjSA->loadedAnimFile = animFilesSA.back();
        exporterObjSA->animName = animNameSA.back();
        animFilesSA.pop_back();
        animNameSA.pop_back();

        //Open file with granny dll
        exporterObjSA->m_grannyAnimFile = exporterObjSA->openFile(exporterObjSA->loadedAnimFile);
        exporterObjSA->m_animFileInfo =  exporterObjSA->onAnimFileOpen(exporterObjSA->m_grannyAnimFile);
        if (exporterObjSA->m_animFileInfo == nullptr) {
            //Cannot open files handle error
            continue;
        }
        else{
            //If successfully open
            exporterObjSA->exportNA2Anim(exporterObjSA->m_animFileInfo, 0, exporterObjSA->m_fileInfo);
        }
    }
    return true;
}
std::string ThreadConvert::getFileNameFromPath(std::string str){
    int i;
    if(str.size()==0)
        return std::string("");
    for(i = str.size()-1; (str.at(i) != '\\')&&(str.at(i) != '/')&&(i>0); i--);
    return std::string(str.substr(i+1, str.size()-i));
}
std::string ThreadConvert::getFolderFromPath(std::string str){
    int i;
    if(str.size()==0)
        return std::string("");
    for(i = str.size()-1; (str.at(i) != '\\')&&(str.at(i) != '/')&&(i>0); i--);
    return std::string(str.substr(0, i+1));
}
bool ThreadConvert::readMsmFileSA(std::string fileName, Exporter * exporterObjSA){
    QFile msmFile(QString::fromStdString(fileName));

    if(msmFile.open(QIODevice::ReadOnly | QIODevice::Text) == false){
        //Handle error
        return false;
    }
    //File opened
    //Looking for BaseModelFileName

    std::string gr2PathFile("");
    bool fieldFound = false;
    while (!msmFile.atEnd()) {
        //Get new line
        QByteArray line = msmFile.readLine();
        char * lineData = line.data();
        //Check it's line we are looking for

        if(strncmp(lineData, m_configObj.nameFieldModelMsm.c_str(), m_configObj.nameFieldModelMsm.size()) != 0){
            //It's not so gonext line
            continue;
        }
        // Good line found
        int indexLine = 0;
        int length = line.length();
        //Skip BaseModelFileName
        while((lineData[indexLine] != '\"')&&(indexLine < length)){
            indexLine++;
        }
        indexLine++;
        //Get content of " caracters
        while((lineData[indexLine] != '\"')&&(indexLine < length)){
            gr2PathFile.push_back(lineData[indexLine++]);
        }
        //Break when finished
        fieldFound = true;
        break;
    }
    //Check if field found
    msmFile.close();
    if(!fieldFound){
        //Field not found quit
        return false;
    }
    //Field found check if file exist

    std::string gr2FolderFile(ThreadConvert::getFolderFromPath(fileName));
    std::string gr2FileName(ThreadConvert::getFileNameFromPath(gr2PathFile));

    if(QFile::exists(QString::fromStdString(gr2FolderFile + gr2FileName)) == false){
        //File doesn't exist handle error
        return false;
    }
    if(exporterObjSA->m_grannyFile != nullptr){
        GrannyFreeFile(exporterObjSA->m_grannyFile);
        exporterObjSA->m_grannyFile = nullptr;
    }
    exporterObjSA->loadedFilename = gr2FolderFile + gr2FileName;
    std::string Filename(exporterObjSA->loadedFilename);

    exporterObjSA->m_grannyFile =   exporterObjSA->openFile(Filename);
    exporterObjSA->m_fileInfo =  exporterObjSA->onFileOpen(exporterObjSA->m_grannyFile);
    if((exporterObjSA->m_grannyFile != nullptr)&&(exporterObjSA->m_fileInfo != nullptr)){

    }
    return true;
}
bool ThreadConvert::convertMultipleDir(QString dirName, QStringList &msmFile, QStringList &txtFile){
    Exporter * exporterObjSA = new Exporter;

    //Vector msm et txt files
    std::vector<std::string> msmFiles;
    std::vector<std::string> txtFiles;

    //Creat Dir object
    QDir directory(dirName);
    //Read *.msm file in the directory
    //QStringList msmFile = directory.entryList(QStringList() << "*.msm" << "*.MSM", QDir::Files);
    foreach(QString filename, msmFile) {
        //Add the file to the vector
        msmFiles.push_back(dirName.toStdString() + std::string("\\") + filename.toStdString());
    }
    //Check if the vector isn't empty
    if(msmFiles.size() > 0){
        //Process msmFile, multi msm file unsupported for now
        readMsmFileSA(msmFiles.at(0), exporterObjSA);
        exporterObjSA->exportAllModelsToCN6(exporterObjSA->m_fileInfo, false);
    }
    else{
        //no msm file leave
        delete exporterObjSA;
        return false;
    }
    foreach(QString filename, txtFile) {
        //Check that the name of file is motlist.txt
        if(filename != m_configObj.txtFileName.c_str()){//"motlist.txt"){
            continue;
        }
        txtFiles.push_back(dirName.toStdString() + std::string("\\") + filename.toStdString());
    }
    //Check the vector isn't empty
    if(txtFiles.size() > 0){
        readTxtFileSA(txtFiles.at(0), exporterObjSA);
    }
    if(exporterObjSA->m_cn6FileGenerated.size() <= 0){
        delete exporterObjSA;
        return false;
    }
    std::stringstream commandShell("");
    QString currDir = QDir::currentPath();
    std::string blenderAbsolutePath(currDir.toStdString());

    blenderAbsolutePath += m_configObj.folderNameBlender; //"\\blender-2.79b-windows64\\";

    commandShell << blenderAbsolutePath << m_configObj.blenderExeName;//"blender.exe ";  //Path to blender executable
    commandShell << " --background -P ";                     //Parameter to run blender in background
    commandShell << blenderAbsolutePath << m_configObj.convScript; // "ExeImporter.py";//Link to script that import na2, cn6 in .blend
    commandShell << " -- ";     // Marque start of py arg

    //First arg is output file name, shall contain .blend
    commandShell << m_configObj.outputFileName;//"out.blend ";
    //Second arg is the working directory
    commandShell << " " << dirName.toStdString() << " ";
    //third arg is cn6 file
    commandShell << ThreadConvert::getFileNameFromPath(exporterObjSA->m_cn6FileGenerated.at(0)) << " ";
    //other parameter are na2 file
    for(unsigned int indexNa2Files = 0; indexNa2Files < exporterObjSA->m_na2FileGenerated.size(); indexNa2Files++){
        commandShell << ThreadConvert::getFileNameFromPath(exporterObjSA->m_na2FileGenerated.at(indexNa2Files)) << " ";
    }

    QProcess blenderProc;
    //const char * tmp = commandShell.str().c_str();
    qDebug() << commandShell.str().c_str() << endl;
    blenderProc.start(commandShell.str().c_str());
    while(!blenderProc.waitForFinished()){
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
        QThread::msleep(100);
        /*QByteArray line = blenderProc.readLine();
        if(line.size() > 0){
            qDebug() << line << endl;
        }*/
    }
    std::string resultCmd = blenderProc.readAll().toStdString();
    //clean the generated file
    for(unsigned int indexCn6Files = 0; indexCn6Files < exporterObjSA->m_cn6FileGenerated.size(); indexCn6Files++){
        QFile file2remove(QString::fromStdString(exporterObjSA->m_cn6FileGenerated.at(indexCn6Files)));
        file2remove.remove();
    }
    for(unsigned int indexNa2Files = 0; indexNa2Files < exporterObjSA->m_na2FileGenerated.size(); indexNa2Files++){
        QFile file2remove(QString::fromStdString(exporterObjSA->m_na2FileGenerated.at(indexNa2Files)));
        file2remove.remove();
    }
    delete exporterObjSA;
    return true;
}
