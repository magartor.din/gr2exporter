#include "fileconfig.h"

#include <algorithm>
#include <cctype>
#include <memory>
#include <string>
#include <unistd.h>
#include <sstream>
#include <iostream>

FileConfig::FileConfig(){
    folderNameBlender = "/blender-2.79b-windows64/";
    nameFieldModelMsm = "BaseModelFileName";
    nameFieldAnimTxt = "MotionFileName";
    blenderExeName = "blender.exe";
    blenderParameter = "--background -P";
    convScript = "ExeImporter.py";
    outputFileName = "out.blend";
    txtFileName = "motlist.txt";

    grannyFileExtension = "gr2 file (*.gr2 *.GR2)";
    msmFileExtension = "msm file (*.msm *.MSM)";
    txtFileExtension = "text file (*.txt)";

    txtFileExtensionShort = "txt";
    grannyFileExtensionShort = "gr2";
    msmFileExtensionShort = "msm";
}

FileConfig::FileConfig(FileConfig &cpy){

    folderNameBlender   = cpy.folderNameBlender;
    nameFieldModelMsm   = cpy.nameFieldModelMsm;
    nameFieldAnimTxt    = cpy.nameFieldAnimTxt;
    blenderExeName      = cpy.blenderExeName;
    blenderParameter    = cpy.blenderParameter;
    convScript          = cpy.convScript;
    outputFileName      = cpy.outputFileName;
    txtFileName         = cpy.txtFileName;

    grannyFileExtension = cpy.grannyFileExtension;
    msmFileExtension    = cpy.msmFileExtension;
    txtFileExtension    = cpy.txtFileExtension;

    txtFileExtensionShort       = cpy.txtFileExtensionShort;
    grannyFileExtensionShort    = cpy.grannyFileExtensionShort;
    msmFileExtensionShort       = cpy.msmFileExtensionShort;

}
std::string FileConfig::getExtension(std::string extName){
    //"gr2 file (*.gr2 *.GR2)"
    std::string lowerStr(extName);
    std::string upperStr(extName);
    char strTempo[256];

    std::transform(lowerStr.begin(), lowerStr.end(), lowerStr.begin(),
        [](unsigned char c){ return std::tolower(c); });
    std::transform(upperStr.begin(), upperStr.end(), upperStr.begin(),
        [](unsigned char c){ return std::toupper(c); });

    std::snprintf(strTempo, 256, "%s file (*.%s *.%s)", extName.c_str(), lowerStr.c_str(), upperStr.c_str());

    return std::string(strTempo);
}
void FileConfig::resetConfig(void){
    folderNameBlender = "\\blender-2.79b-windows64\\";
    nameFieldModelMsm = "BaseModelFileName";
    nameFieldAnimTxt = "MotionFileName";
    blenderExeName = "blender.exe";
    blenderParameter = "--background -P";
    convScript = "ExeImporter.py";
    outputFileName = "out.blend";
    txtFileName = "motlist.txt";

    grannyFileExtension = "gr2 file (*.gr2 *.GR2)";
    msmFileExtension = "msm file (*.msm *.MSM)";
    txtFileExtension = "text file (*.txt)";

    txtFileExtensionShort = "txt";
    grannyFileExtensionShort = "gr2";
    msmFileExtensionShort = "msm";
}
bool FileConfig::loadConfig(std::string filePath){
    QFile file(QString::fromStdString(filePath));
    if(!file.exists()){
        return false;
    }
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        return false;
    }
    QByteArray line;
    while(!file.atEnd()){
        line = file.readLine();
        std::string strLine(line.data());
        std::istringstream iss(strLine);
        std::vector<std::string> textSplited(std::istream_iterator<std::string>{iss},
                                         std::istream_iterator<std::string>());
        if(textSplited.size() != 2){
            continue;
        }
        if(!setByName(textSplited.at(0), textSplited.at(1))){
            return false;
        }
    }
    return true;
}
bool FileConfig::setByName(std::string name, std::string val){
    const char * strName = name.c_str();
    if(strcmp(strName, "folderNameBlender") == 0){
        folderNameBlender = val;
        return true;
    }
    else if(strcmp(strName, "grannyFileExtension") == 0){
        grannyFileExtensionShort = val;
        grannyFileExtension = getExtension(val);
        return true;
    }
    else if(strcmp(strName, "msmFileExtension") == 0){
        msmFileExtensionShort = val;
        msmFileExtension = getExtension(val);
        return true;
    }
    else if(strcmp(strName, "txtFileExtension") == 0){
        txtFileExtensionShort = val;
        txtFileExtension = getExtension(val);
        return true;
    }
    else if(strcmp(strName, "nameFieldModelMsm") == 0){
        nameFieldModelMsm = val;
        return true;
    }
    else if(strcmp(strName, "nameFieldAnimTxt") == 0){
        nameFieldAnimTxt = val;
        return true;
    }
    else if(strcmp(strName, "blenderExeName") == 0){
        blenderExeName = val;
        return true;
    }
    else if(strcmp(strName, "blenderParameter") == 0){
        blenderParameter = val;
        return true;
    }
    else if(strcmp(strName, "convScript") == 0){
        convScript = val;
        return true;
    }
    else if(strcmp(strName, "outputFileName") == 0){
        outputFileName = val;
        return true;
    }
    else if(strcmp(strName, "txtFileName") == 0){
        txtFileName = val;
        return true;
    }
    else{
        return false;
    }
}
bool FileConfig::saveConfig(std::string filePath){
    QFile file(QString::fromStdString(filePath));
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)){
        return false;
    }
    std::stringstream outStr("");
    outStr << "folderNameBlender"   << " " << folderNameBlender         << std::endl;
    outStr << "grannyFileExtension" << " " << grannyFileExtensionShort  << std::endl;
    outStr << "msmFileExtension"    << " " << msmFileExtensionShort     << std::endl;
    outStr << "txtFileExtension"    << " " << txtFileExtensionShort     << std::endl;
    outStr << "nameFieldModelMsm"   << " " << nameFieldModelMsm         << std::endl;
    outStr << "nameFieldAnimTxt"    << " " << nameFieldAnimTxt          << std::endl;
    outStr << "blenderExeName"      << " " << blenderExeName            << std::endl;
    outStr << "blenderParameter"    << " " << blenderParameter          << std::endl;
    outStr << "convScript"          << " " << convScript                << std::endl;
    outStr << "outputFileName"      << " " << outputFileName            << std::endl;
    outStr << "txtFileName"         << " " << txtFileName               << std::endl;

    file.write(outStr.str().c_str(), outStr.str().size());
    file.close();
    return true;
}

