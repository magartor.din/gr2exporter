#ifndef MAINWINDOWVIEWER_H
#define MAINWINDOWVIEWER_H

#include <QMainWindow>
#include <QFile>
#include <QFileDialog>
#include <QString>
#include <QStringList>

#include "animationview.h"
#include "materialsview.h"
#include "meshesview.h"
#include "modelview.h"
#include "skeletonview.h"
#include "texturesview.h"
#include "trackgroupview.h"
#include "tritopologyview.h"
#include "tritopologyview.h"
#include "vertexview.h"
#include "arttoolviewer.h"

#include "NBCpp/granny.h"
#include "fileconfig.h"



namespace Ui {
class MainWindowViewer;
}

class MainWindowViewer : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindowViewer(QWidget *parent = nullptr);
    ~MainWindowViewer();

public slots:
    void resetOnglet(void);
    void reset(void);
private slots:
    void on_actionOpen_triggered();

    void on_actionAddFile_triggered();

    void removeAllFiles();

    void addFileInfo(granny_file_info * fileInfo);
    void addFileFromName(QString const &filePath);

    void updateFileStat(granny_file_info * fileInfo);
    //void updateTabs(granny_file_info * fileInfo);
    void on_actionClose_triggered();

    void on_tableWidget_file_cellChanged(int row, int column);

    void on_tableWidget_file_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

    void on_actionSingle_File_Mode_toggled(bool arg1);

    void on_tableWidget_file_cellActivated(int row, int column);

    void on_actionOpen_Folder_triggered();

    void on_actionWorking_directory_triggered();

    void on_actionFBX_triggered();

    void on_actionMetin2_Folder_triggered();

private:
    Ui::MainWindowViewer *ui;

    AnimationView * animationWidget;
    MaterialsView * materialWidget;
    MeshesView * meshWidget;
    ModelView * modelWidget;
    SkeletonView * skeletonWidget;
    TexturesView * textureWidget;
    TrackgroupView * trackgroupWidget;
    TritopologyView * tritopologyWidget;
    VertexView * vertexWidget;
    ArtToolViewer * ArtToolWidget;

    FileConfig configObj;

    QString logErr;

    QStringList lstFile;
    std::list<granny_file *> lstGrannyFile;
    std::list<granny_file_info *> lstGrannyFileInfo;
    std::map<int, granny_file_info *> mapListFileInfo;
    std::map<int, granny_file *> mapListFile;
    std::map<int, QString> mapListFileName;
    std::map<QString, int> mapListFileNameInv;

    bool singleFileMode;
    int fileSelected;

};

#endif // MAINWINDOWVIEWER_H
