#ifndef TRITOPOLOGYVIEW_H
#define TRITOPOLOGYVIEW_H

#include <QWidget>

#include "NBCpp/granny.h"

namespace Ui {
class TritopologyView;
}

class TritopologyView : public QWidget
{
    Q_OBJECT

public:
    explicit TritopologyView(QWidget *parent = nullptr);
    ~TritopologyView();
public slots:
    void addTritopology(granny_int32 TriTopologyCount, granny_tri_topology ** TriTopologies);

    void resetTritopology(void);

    void loadTritopologySelected(granny_tri_topology * TriTopologies);
    void loadTritopologySelectedMaterialGroup(granny_tri_topology * TriTopologies);
    void loadTritopologySelectedVertexToTri(granny_tri_topology * TriTopologies);
    void loadTritopologySelectedIndex(granny_tri_topology * TriTopologies);
    void loadTritopologySelectedSideToNeighbor(granny_tri_topology * TriTopologies);
    void loadTritopologySelectedIndex16(granny_tri_topology * TriTopologies);
    void loadTritopologySelectedBoneForTri(granny_tri_topology * TriTopologies);
    void loadTritopologySelectedVertexToVertex(granny_tri_topology * TriTopologies);
    void loadTritopologySelectedTriToBone(granny_tri_topology * TriTopologies);
    void loadTritopologySelectedAnnotationList(granny_tri_topology * TriTopologies);
private slots:
    void on_tableWidget_tritopology_cellClicked(int row, int column);

    void on_tableWidget_tritopology_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

private:
    Ui::TritopologyView *ui;
    QString log;
    std::map<int, granny_tri_topology *> mapListTritopology;

    int TritopologySelected;
};

#endif // TRITOPOLOGYVIEW_H
