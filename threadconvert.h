#ifndef THREADCONVERT_H
#define THREADCONVERT_H

#include <QThread>
#include <QString>
#include "exporter.h"
#include "fileconfig.h"

class ThreadConvert : public QThread
{
    Q_OBJECT
    void run() override {
        QString result;
        convertMultipleDir(m_dirName, m_msmFile, m_txtFile);
        emit resultReady(this);
    }
signals:
    void resultReady(ThreadConvert * s);
private:
    bool readMsmFileSA(std::string fileName, Exporter * exporterObjSA);
    bool readTxtFileSA(std::string fileNameStd, Exporter * exporterObjSA);
    std::string getFileNameFromPath(std::string str);
    std::string getFolderFromPath(std::string str);
public:
    QString m_dirName;
    QStringList m_msmFile;
    QStringList m_txtFile;
    FileConfig m_configObj;

    ThreadConvert(QString dirName, QStringList &msmFile, QStringList &txtFile, FileConfig configObj);
    bool convertMultipleDir(QString dirName, QStringList &msmFile, QStringList &txtFile);
};

#endif // THREADCONVERT_H
