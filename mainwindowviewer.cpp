#include "mainwindowviewer.h"
#include "ui_mainwindowviewer.h"

MainWindowViewer::MainWindowViewer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindowViewer)
{
    ui->setupUi(this);

    animationWidget = new AnimationView();
    ui->verticalLayout_animations->addWidget(animationWidget);
    materialWidget = new MaterialsView();
    ui->verticalLayout_material->addWidget(materialWidget);
    meshWidget = new MeshesView();
    ui->verticalLayout_meshes->addWidget(meshWidget);
    modelWidget = new ModelView();
    ui->verticalLayout_model->addWidget(modelWidget);
    skeletonWidget = new SkeletonView();
    ui->verticalLayout_skeleton->addWidget(skeletonWidget);
    textureWidget = new TexturesView();
    ui->verticalLayout_texture->addWidget(textureWidget);
    trackgroupWidget = new TrackgroupView();
    ui->verticalLayout_trackgroups->addWidget(trackgroupWidget);
    tritopologyWidget = new TritopologyView();
    ui->verticalLayout_tritopology->addWidget(tritopologyWidget);
    vertexWidget = new VertexView();
    ui->verticalLayout_vertex->addWidget(vertexWidget);
    ArtToolWidget = new ArtToolViewer();
    ui->verticalLayout_artTool->addWidget(ArtToolWidget);

    singleFileMode = false;
    fileSelected = -1;
}
MainWindowViewer::~MainWindowViewer(){
    delete ui;
    for(std::list<granny_file *>::iterator it=lstGrannyFile.begin(); it!=lstGrannyFile.end(); ++it){
        granny_file* grannyFile = *it;
        if(grannyFile != nullptr){
            GrannyFreeFile(grannyFile);
        }
    }
}
void MainWindowViewer::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open"), "", configObj.grannyFileExtension.c_str());//tr("gr2 file (*.gr2 *.GR2)"));
    if(fileName == ""){
        return;
    }
    this->removeAllFiles();
    this->lstFile.append(fileName);


    granny_file* grannyFile =  GrannyReadEntireFile(fileName.toLocal8Bit().data());
    if (grannyFile == nullptr) {
        logErr += "Could not load %s\n";
        logErr += fileName.toLocal8Bit().data();
        return;
    }
    granny_file_info * fileInfo =  GrannyGetFileInfo(grannyFile);
    if (fileInfo == nullptr) {
        logErr += "Could not get file info from %s\n";
        logErr += fileName.toLocal8Bit().data();
        return;
    }
    ui->lineEdit_fileName->setText(fileName);
    resetOnglet();
    for(int i = ui->tableWidget_file->rowCount(); i >= 0; i--){
        ui->tableWidget_file->removeRow(i);
    }
    int row = ui->tableWidget_file->rowCount();
    mapListFileName[row] = fileName;
    mapListFileNameInv[fileName] = row;
    ui->tableWidget_file->insertRow(row);
    ui->tableWidget_file->setItem(row, 0, new QTableWidgetItem(fileName));
    mapListFileInfo[row] = fileInfo;
    mapListFile[row] = grannyFile;

    updateFileStat(fileInfo);
    addFileInfo(fileInfo);
    lstGrannyFile.push_back(grannyFile);
    lstGrannyFileInfo.push_back(fileInfo);
}
void MainWindowViewer::updateFileStat(granny_file_info * fileInfo){
    if(fileInfo == nullptr){
        return;
    }

    int idx = 0;
    for(std::map<int, granny_file_info *>::iterator it = mapListFileInfo.begin(); it != mapListFileInfo.end(); it++){
        if(it->second == fileInfo){
            idx = it->first;
        }
    }
    ui->lineEdit_fileName->setText(mapListFileName[idx]);

    ui->label_valAnnimationCount->setText(QString::number(fileInfo->AnimationCount));
    ui->label_valMaterialsCount->setText(QString::number(fileInfo->MaterialCount));
    ui->label_valMeshCount->setText(QString::number(fileInfo->MeshCount));
    ui->label_valModelCount->setText(QString::number(fileInfo->ModelCount));
    ui->label_valSkeletonCount->setText(QString::number(fileInfo->SkeletonCount));
    ui->label_valTextureCount->setText(QString::number(fileInfo->TextureCount));
    ui->label_valTrackGroup->setText(QString::number(fileInfo->TrackGroupCount));
    ui->label_valVertexCount->setText(QString::number(fileInfo->TriTopologyCount));

}
void MainWindowViewer::removeAllFiles(){
    animationWidget->resetAnimations();
    materialWidget->removeMaterial();
    meshWidget->resetMeshes();
    modelWidget->resetModels();
    skeletonWidget->resetSkeletons();
    textureWidget->resetTextures();
    trackgroupWidget->resetTrackgroups();
    tritopologyWidget->resetTritopology();
    vertexWidget->resetVertex();
    ArtToolWidget->resetArtTool();
}
void MainWindowViewer::addFileInfo(granny_file_info * fileInfo){
    if(fileInfo == nullptr){
        return;
    }
    animationWidget->addAnimation(fileInfo->AnimationCount, fileInfo->Animations);
    materialWidget->addMaterials(fileInfo->MaterialCount, fileInfo->Materials);
    meshWidget->addMeshes(fileInfo->MeshCount, fileInfo->Meshes);
    modelWidget->addModels(fileInfo->ModelCount, fileInfo->Models);
    skeletonWidget->addSkeletons(fileInfo->SkeletonCount, fileInfo->Skeletons);
    textureWidget->addTextures(fileInfo->TextureCount, fileInfo->Textures);
    trackgroupWidget->addTrackgroups(fileInfo->TrackGroupCount, fileInfo->TrackGroups);
    tritopologyWidget->addTritopology(fileInfo->TriTopologyCount, fileInfo->TriTopologies);
    vertexWidget->addVertex(fileInfo->VertexDataCount, fileInfo->VertexDatas);
    ArtToolWidget->addArtTool(fileInfo->ArtToolInfo);

}
void MainWindowViewer::on_actionAddFile_triggered(){
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open"), "", configObj.grannyFileExtension.c_str());//tr("gr2 file (*.gr2 *.GR2)"));
    if(fileName == ""){
        return;
    }
    //this->removeAllFiles();
    this->lstFile.append(fileName);


    granny_file* grannyFile =  GrannyReadEntireFile(fileName.toLocal8Bit().data());
    if (grannyFile == nullptr) {
        logErr += "Could not load %s\n";
        logErr += fileName.toLocal8Bit().data();
        return;
    }
    granny_file_info * fileInfo =  GrannyGetFileInfo(grannyFile);
    if (fileInfo == nullptr) {
        logErr += "Could not get file info from %s\n";
        logErr += fileName.toLocal8Bit().data();
        return;
    }

    int row = ui->tableWidget_file->rowCount();
    mapListFileName[row] = fileName;
    mapListFileNameInv[fileName] = row;
    ui->tableWidget_file->insertRow(row);
    ui->tableWidget_file->setItem(row, 0, new QTableWidgetItem(fileName));
    mapListFileInfo[row] = fileInfo;
    mapListFile[row] = grannyFile;


    if(singleFileMode){
        resetOnglet();
        addFileInfo(mapListFileInfo[row]);
        updateFileStat(mapListFileInfo[row]);
    }
    else{
        addFileInfo(mapListFileInfo[row]);
        updateFileStat(mapListFileInfo[row]);
    }
    lstGrannyFile.push_back(grannyFile);
    lstGrannyFileInfo.push_back(fileInfo);
}

void MainWindowViewer::on_actionOpen_Folder_triggered(){
    QString dirName = QFileDialog::getExistingDirectory(this, tr("Open"), "D:/", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    //Exemple dirName == "C:/Travail/Tiger"
    if(dirName == ""){
        return;
    }
    QDir directory(dirName);

    QStringList gr2Files;
    gr2Files << directory.entryList(QStringList() << "*.gr2" << "*.GR2" << "*.gR2" << "*.Gr2", QDir::Files);

    for(int i = 0; i < gr2Files.size();i++){
        addFileFromName(dirName + "/" + gr2Files.at(i));
    }
}
void MainWindowViewer::addFileFromName(QString const &fileName){
    this->lstFile.append(fileName);


    granny_file* grannyFile =  GrannyReadEntireFile(fileName.toLocal8Bit().data());
    if (grannyFile == nullptr) {
        logErr += "Could not load %s\n";
        logErr += fileName.toLocal8Bit().data();
        return;
    }
    granny_file_info * fileInfo =  GrannyGetFileInfo(grannyFile);
    if (fileInfo == nullptr) {
        logErr += "Could not get file info from %s\n";
        logErr += fileName.toLocal8Bit().data();
        return;
    }

    int row = ui->tableWidget_file->rowCount();
    mapListFileName[row] = fileName;
    mapListFileNameInv[fileName] = row;
    ui->tableWidget_file->insertRow(row);
    ui->tableWidget_file->setItem(row, 0, new QTableWidgetItem(fileName));
    mapListFileInfo[row] = fileInfo;
    mapListFile[row] = grannyFile;


    if(singleFileMode){
        resetOnglet();
        addFileInfo(mapListFileInfo[row]);
        updateFileStat(mapListFileInfo[row]);
    }
    else{
        addFileInfo(mapListFileInfo[row]);
        updateFileStat(mapListFileInfo[row]);
    }
    lstGrannyFile.push_back(grannyFile);
    lstGrannyFileInfo.push_back(fileInfo);
}
void MainWindowViewer::on_actionClose_triggered(){
    reset();
}
void MainWindowViewer::reset(void){
    for(int i = ui->tableWidget_file->rowCount(); i >= 0; i--){
        ui->tableWidget_file->removeRow(i);
    }
    ui->lineEdit_fileName->setText("");
    ui->label_valMeshCount->setText("0");
    ui->label_valModelCount->setText("0");
    ui->label_valTrackGroup->setText("0");
    ui->label_valVertexCount->setText("0");
    ui->label_valTextureCount->setText("0");
    ui->label_valSkeletonCount->setText("0");
    ui->label_valMaterialsCount->setText("0");
    ui->label_valAnnimationCount->setText("0");

    lstFile.clear();

    lstGrannyFile.clear();
    lstGrannyFileInfo.clear();
    mapListFileInfo.clear();
    mapListFile.clear();

    resetOnglet();
}
void MainWindowViewer::resetOnglet(void){
    animationWidget->resetAnimations();
    materialWidget->removeMaterial();
    meshWidget->resetMeshes();
    modelWidget->resetModels();
    skeletonWidget->resetSkeletons();
    textureWidget->resetTextures();
    trackgroupWidget->resetTrackgroups();
    tritopologyWidget->resetTritopology();
    vertexWidget->resetVertex();
    ArtToolWidget->resetArtTool();
}
void MainWindowViewer::on_tableWidget_file_cellChanged(int row, int column){
    if(column < 0){
        return;
    }
    if(singleFileMode){
        resetOnglet();
        addFileInfo(mapListFileInfo[row]);
        updateFileStat(mapListFileInfo[row]);
    }
    else{
        updateFileStat(mapListFileInfo[row]);
    }
}
void MainWindowViewer::on_tableWidget_file_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(currentColumn < 0){
        if(previousColumn < 0){
            return;
        }
        return;
    }
    if(currentRow != previousRow){
        if(fileSelected != currentRow){
            fileSelected = currentRow;
            if(singleFileMode){
                resetOnglet();
                addFileInfo(mapListFileInfo[currentRow]);
                updateFileStat(mapListFileInfo[currentRow]);
            }
            else{
                updateFileStat(mapListFileInfo[currentRow]);
            }
        }
    }
}
void MainWindowViewer::on_actionSingle_File_Mode_toggled(bool arg1){
    singleFileMode = arg1;
    if(!singleFileMode){
        //Changement de mode pour mode multi file
        //Reset tous les onglet pour affichage
        resetOnglet();
        //Affichage de tous les fichier
        for(std::list<granny_file_info *>::iterator it = lstGrannyFileInfo.begin(); it != lstGrannyFileInfo.end(); it++){
            addFileInfo(*it);
        }
    }
    else{
        resetOnglet();
        addFileInfo(mapListFileInfo[fileSelected]);
    }
}
void MainWindowViewer::on_tableWidget_file_cellActivated(int row, int column){
    if(column < 0){
        return;
    }
    if(singleFileMode){
        resetOnglet();
        addFileInfo(mapListFileInfo[row]);
        updateFileStat(mapListFileInfo[row]);
    }
    else{
        updateFileStat(mapListFileInfo[row]);
    }
}
void MainWindowViewer::on_actionWorking_directory_triggered(){

}

void MainWindowViewer::on_actionFBX_triggered(){

}

void MainWindowViewer::on_actionMetin2_Folder_triggered(){
    QString dirName = QFileDialog::getExistingDirectory(this, tr("Open"), "D:/", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    //Exemple dirName == "C:/Travail/Tiger"
    if(dirName == ""){
        return;
    }
    QDir directory(dirName);

    QStringList gr2Files;
    gr2Files << directory.entryList(QStringList() << "*.gr2" << "*.GR2" << "*.gR2" << "*.Gr2", QDir::Files);

    for(int i = 0; i < gr2Files.size();i++){
        addFileFromName(dirName + "/" + gr2Files.at(i));
    }
}
