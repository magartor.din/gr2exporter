#include "texturesview.h"
#include "ui_texturesview.h"

TexturesView::TexturesView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TexturesView)
{
    ui->setupUi(this);
    ui->tableWidget_texture->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    textureSelected = -1;
}

TexturesView::~TexturesView(){
    delete ui;
}

void TexturesView::resetTextures(void){
    ui->tableWidget_texture->clear();
    ui->tableWidget_texture->setHorizontalHeaderItem(0,new QTableWidgetItem("File name"));
    for(int i = ui->tableWidget_texture->rowCount(); i >= 0; i--){
        ui->tableWidget_texture->removeRow(i);
    }
    resetTextureSelected();
}
void TexturesView::addTextures(granny_int32 TextureCount, granny_texture ** Textures){
    //Verifie le pointeur vers le tableau de Textures
    if(Textures == nullptr){
        return;
    }
    //Variable iteration Texture
    granny_int32 indexTexture;
    for(indexTexture = 0; indexTexture < TextureCount;indexTexture++){
        //Pour tout les modèles du tableau
        try {
            if(Textures[indexTexture] != nullptr){
                //Si le pointeur est valid
                try {
                    //stock le pointeur dans une variable locale
                    granny_texture * curTexture = Textures[indexTexture];
                    if(curTexture != nullptr){
                        //Si le pointeur du modèle est valide on recupère le nombre de ligne
                        int row = ui->tableWidget_texture->rowCount();
                        //on ajoute une ligne et on increment le nombre de ligne
                        ui->tableWidget_texture->insertRow(row);
                        QString TextureName;
                        //Recupère le nombre de colone
                        //int column = ui->tableWidget_texture->columnCount();
                        QTableWidgetItem *newItem = nullptr;
                        if(curTexture->FromFileName != nullptr){
                            //Si le nom du modèle est valide
                            TextureName = QString::fromLocal8Bit(curTexture->FromFileName);
                        }
                        else{
                            //Si le nom du modèle n'est pas valide on genere un nom
                            TextureName = QString::number((int)curTexture, 16);
                        }
                        //Crée l'objet représentant le modèle dans la liste
                        newItem = new QTableWidgetItem(TextureName);
                        //Ajoute le modèle à la liste
                        ui->tableWidget_texture->setItem(row, 0, newItem);
                        mapListTexture[row] = curTexture;
                    }
                }catch (...) {
                    log += "Error accessing Texture " + QString::number(indexTexture);
                }
            }
        } catch (...) {
            log += "Error accessing Textures tables " + QString::number(indexTexture);
        }
    }
}


void TexturesView::resetTextureSelected(void){
    resetTextureSelectedFileName();
    resetTextureSelectedType();
    resetTextureSelectedWidth();
    resetTextureSelectedHeight();
    resetTextureSelectedEncoding();
    resetTextureSelectedSubFormat();
    resetTextureSelectedLayout();
    resetTextureSelectedImageCount();
}
void TexturesView::resetTextureSelectedFileName(void){
    ui->label_valTextureSelectedFileName->setText("None");
}
void TexturesView::resetTextureSelectedType(void){
    ui->label_valTextureSelectedTextureType->setText("None");
}
void TexturesView::resetTextureSelectedWidth(void){
    ui->label_valTextureSelectedWidth->setText("None");
}
void TexturesView::resetTextureSelectedHeight(void){
    ui->label_valTextureSelectedHeight->setText("None");
}
void TexturesView::resetTextureSelectedEncoding(void){
    ui->label_valTextureSelectedEncoding->setText("None");
}
void TexturesView::resetTextureSelectedSubFormat(void){
    ui->label_valTextureSelectedSubformat->setText("None");
}
void TexturesView::resetTextureSelectedLayout(void){
    resetTextureSelectedLayoutBytePerPixel();
    resetTextureSelectedLayoutShiftForCompo();
    resetTextureSelectedLayoutBitsForCompo();
}
void TexturesView::resetTextureSelectedLayoutBytePerPixel(void){
    ui->label_valTextureSelectedBytePerPixel->setText("None");
}
void TexturesView::resetTextureSelectedLayoutShiftForCompo(void){
    ui->label_valShiftForComponent0->setText("0");
    ui->label_valShiftForComponent1->setText("0");
    ui->label_valShiftForComponent2->setText("0");
    ui->label_valShiftForComponent3->setText("0");
}
void TexturesView::resetTextureSelectedLayoutBitsForCompo(void){
    ui->label_valBitsForCompo0->setText("0");
    ui->label_valBitsForCompo1->setText("0");
    ui->label_valBitsForCompo2->setText("0");
    ui->label_valBitsForCompo3->setText("0");
}
void TexturesView::resetTextureSelectedImageCount(void){
    ui->label_valTextureSelectedImageCount->setText("None");
}

void TexturesView::loadTextureSelected(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        loadTextureSelectedFileName(texture);
        loadTextureSelectedType(texture);
        loadTextureSelectedWidth(texture);
        loadTextureSelectedHeight(texture);
        loadTextureSelectedEncoding(texture);
        loadTextureSelectedSubFormat(texture);
        loadTextureSelectedLayout(texture);
        loadTextureSelectedImageCount(texture);
    } catch (...) {
        log += "Erreur lecture Texture selectionne\n";
    }
}
void TexturesView::loadTextureSelectedFileName(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    if(texture->FromFileName == nullptr){
        return;
    }
    try {
        ui->label_valTextureSelectedFileName->setText(texture->FromFileName);
    } catch (...) {
        log += "Erreur lecture FileName Texture selectionne material selectionne\n";
    }
}
void TexturesView::loadTextureSelectedType(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        QString strType(GrannyGetTextureTypeName(texture->TextureType));
        ui->label_valTextureSelectedTextureType->setText(strType);
    } catch (...) {
        log += "Erreur lecture Type Texture selectionne material selectionne\n";
    }
}
void TexturesView::loadTextureSelectedWidth(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        ui->label_valTextureSelectedWidth->setText(QString::number(texture->Width));
    } catch (...) {
        log += "Erreur lecture Width Texture selectionne material selectionne\n";
    }
}
void TexturesView::loadTextureSelectedHeight(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        ui->label_valTextureSelectedHeight->setText(QString::number(texture->Height));
    } catch (...) {
        log += "Erreur lecture Height Texture selectionne material selectionne\n";
    }
}
void TexturesView::loadTextureSelectedEncoding(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        QString strType(GrannyGetTextureEncodingName(texture->Encoding));
        ui->label_valTextureSelectedEncoding->setText(strType);
    } catch (...) {
        log += "Erreur lecture Encoding Texture selectionne material selectionne\n";
    }
}
void TexturesView::loadTextureSelectedSubFormat(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        ui->label_valTextureSelectedSubformat->setText(QString::number(texture->SubFormat));
    } catch (...) {
        log += "Erreur lecture Sub format Texture selectionne material selectionne\n";
    }
}
void TexturesView::loadTextureSelectedLayout(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        loadTextureSelectedLayoutBytePerPixel(texture);
        loadTextureSelectedLayoutShiftForCompo(texture);
        loadTextureSelectedLayoutBitsForCompo(texture);
    } catch (...) {
        log += "Erreur lecture Layout Texture selectionne material selectionne\n";
    }
}
void TexturesView::loadTextureSelectedLayoutBytePerPixel(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        ui->label_valTextureSelectedBytePerPixel->setText(QString::number(texture->Layout.BytesPerPixel));
    } catch (...) {
        log += "Erreur lecture Layout Texture selectionne material selectionne\n";
    }
}
void TexturesView::loadTextureSelectedLayoutShiftForCompo(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        ui->label_valShiftForComponent0->setText(QString::number(texture->Layout.ShiftForComponent[0]));
        ui->label_valShiftForComponent1->setText(QString::number(texture->Layout.ShiftForComponent[1]));
        ui->label_valShiftForComponent2->setText(QString::number(texture->Layout.ShiftForComponent[2]));
        ui->label_valShiftForComponent3->setText(QString::number(texture->Layout.ShiftForComponent[3]));
    } catch (...) {
        log += "Erreur lecture Layout Texture selectionne material selectionne\n";
    }
}
void TexturesView::loadTextureSelectedLayoutBitsForCompo(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        ui->label_valBitsForCompo0->setText(QString::number(texture->Layout.BitsForComponent[0]));
        ui->label_valBitsForCompo1->setText(QString::number(texture->Layout.BitsForComponent[1]));
        ui->label_valBitsForCompo2->setText(QString::number(texture->Layout.BitsForComponent[2]));
        ui->label_valBitsForCompo3->setText(QString::number(texture->Layout.BitsForComponent[3]));
    } catch (...) {
        log += "Erreur lecture Layout Texture selectionne material selectionne\n";
    }
}
void TexturesView::loadTextureSelectedImageCount(granny_texture * texture){
    if(texture == nullptr){
        return;
    }
    try {
        ui->label_valTextureSelectedImageCount->setText(QString::number(texture->ImageCount));
    } catch (...) {
        log += "Erreur lecture ImageCount Texture selectionne material selectionne\n";
    }
}

void TexturesView::on_tableWidget_texture_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(textureSelected != row){
        loadTextureSelected(mapListTexture[row]);
        textureSelected = row;
    }
}

void TexturesView::on_tableWidget_texture_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(currentColumn < 0){
        return;
    }
    if(currentRow == previousRow){
        //anti warning
        if(previousColumn)
            return;
        return;
    }
    if(currentRow != textureSelected){
        textureSelected = currentRow;
        loadTextureSelected(mapListTexture[currentRow]);
    }
}
