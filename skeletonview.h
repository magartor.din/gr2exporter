#ifndef SKELETONVIEW_H
#define SKELETONVIEW_H

#include "ui_skeletonview.h"
#include <QWidget>

#include "matrix3x3.h"
#include "matrix4x4.h"
#include "NBCpp/granny.h"
#include "extendeddataview.h"

namespace Ui {
class SkeletonView;
}

class SkeletonView : public QWidget
{
    Q_OBJECT

public:
    explicit SkeletonView(QWidget *parent = nullptr);
    ~SkeletonView();
public slots:
    void addSkeletons(granny_int32 SkeletonCount, granny_skeleton ** Skeletons);

    void resetSkeletons(void);
    void resetBones(void);


    //Affiche le skeleton selectionné
    void loadSkeletonSelected(granny_skeleton * skeleton);
    //Affiche le nom de skelete selectionné
    void loadSkeletonSelectedName(granny_skeleton * skeleton);
    //Affiche le LOD du skelete selectionné
    void loadSkeletonSelectedLODError(granny_skeleton * skeleton);

    //Charge la liste des bones du skelete selectionné
    void loadSkeletonSelectedBones(granny_skeleton * skeleton);
    //Affiche le bone selectionné
    void loadBoneSelected(granny_bone * skeleton);
    //Affiche le nom du bone selectionné
    void loadBoneSelectedName(granny_bone * skeleton);
    //Affiche l'index du parent du bone selectionné
    void loadBoneSelectedParentIndex(granny_bone * skeleton);
    //Affiche le LOD du bone selectionné
    void loadBoneSelectedLODError(granny_bone * skeleton);
    //Affiche la matrice world du bone selectionné
    void loadBoneSelectedInverseWorldMatrix(granny_bone * skeleton);
    //Affiche la transformation local du bone selectionné
    void loadBoneSelectedLocalTransform(granny_bone * skeleton);
    //Affiche le flag de la transformation local du bone selectionné
    void loadBoneSelectedLocalTransformFlag(granny_bone * skeleton);
    //Affiche le Position de la transformation local du bone selectionné
    void loadBoneSelectedLocalTransformPos(granny_bone * skeleton);
    //Affiche l'orientation de la transformation local du bone selectionné
    void loadBoneSelectedLocalTransformOri(granny_bone * skeleton);
    //Affiche le scale de la transformation local du bone selectionné
    void loadBoneSelectedLocalTransformScale(granny_bone * skeleton);
    //Affiche les donnée étendu du bone selectionné
    void loadBoneSelectedExtendData(granny_bone * skeleton);

private slots:
    void on_tableWidget_skeleton_cellClicked(int row, int column);

    void on_tableWidget_bones_cellClicked(int row, int column);

    void on_tableWidget_bones_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

private:
    Ui::SkeletonView *ui;
    Matrix3x3 * localTransform;
    Matrix4x4 * boneInvWorld;
    ExtendedDataView * extendedDateBone;
    QString log;
    std::map<int, granny_skeleton *> mapListSkeleton;
    std::map<int, granny_bone *> mapListBone;

    int skeletonSelected;
    int boneSelected;
};

#endif // SKELETONVIEW_H
