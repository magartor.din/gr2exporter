#ifndef VARIANTVIEW_H
#define VARIANTVIEW_H

#include <QWidget>
#include "NBCpp/granny.h"

namespace Ui {
class variantView;
}

class variantView : public QWidget
{
    Q_OBJECT

public:
    explicit variantView(QWidget *parent = nullptr);
    ~variantView();

private:
    Ui::variantView *ui;
};

#endif // VARIANTVIEW_H
