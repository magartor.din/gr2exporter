#include "trackgroupview.h"
#include "ui_trackgroupview.h"


TrackgroupView::TrackgroupView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TrackgroupView)
{
    ui->setupUi(this);
    //InitPlacmentScaleMatrix
    matrixInitPlacmentScale = new Matrix3x3();
/*
    vectorCurve = new curve2();
    ui->verticalLayout_trackgroupsVector->addWidget(vectorCurve);

    transformTrackPosCurve = new curve2();
    transformTrackOriCurve = new curve2();
    transformTrackScaleCurve = new curve2();

    ui->verticalLayout_trackgroupsTransform->addWidget(transformTrackPosCurve);
    ui->verticalLayout_trackgroupsTransform->addWidget(transformTrackOriCurve);
    ui->verticalLayout_trackgroupsTransform->addWidget(transformTrackScaleCurve);
*/
    ui->horizontalLayout_InitPlacementScale->insertWidget(0, matrixInitPlacmentScale, Qt::AlignLeft);

    //ui->horizontalLayout_trackgroupsSelectedInitPlacmentScale->insertWidget(ui->horizontalLayout_trackgroupsSelectedInitPlacmentScale->count()-1,matrixInitPlacmentScale);

    ui->tableWidget_trackgroupsTransform->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    ui->tableWidget_vectorTrack->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    ui->tableWidget_trackgroupsText->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    ui->tableWidget_trackgroupsTextEntries->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);

    TrackgroupSelected = -1;
    VectorSelected = -1;
    TransformSelected = -1;
    TextSelected = -1;
}

TrackgroupView::~TrackgroupView(){
    delete ui;
    delete matrixInitPlacmentScale;
}

template<>
void TrackgroupView::loadTransformerTrackSelectedPos(granny_curve_data_da_k32f_c32f *curve){
    for(int i = ui->tableWidget_transformSelectedPos->rowCount(); i >= 0; i--){
        ui->tableWidget_transformSelectedPos->removeRow(i);
    }
    if(curve == nullptr){
        return;
    }
    if(curve->KnotCount){
        for(int i = ui->tableWidget_transformSelectedPos->columnCount(); i >= 0; i--){
            ui->tableWidget_transformSelectedPos->removeColumn(i);
        }

        ui->tableWidget_transformSelectedPos->insertColumn(0);
        QTableWidgetItem * newItem = new QTableWidgetItem();
        newItem->setText("Time");
        ui->tableWidget_transformSelectedPos->setHorizontalHeaderItem(0, newItem);

        ui->tableWidget_transformSelectedPos->insertColumn(1);
        newItem = new QTableWidgetItem();
        newItem->setText("X");
        ui->tableWidget_transformSelectedPos->setHorizontalHeaderItem(1, newItem);

        ui->tableWidget_transformSelectedPos->insertColumn(2);
        newItem = new QTableWidgetItem();
        newItem->setText("Y");
        ui->tableWidget_transformSelectedPos->setHorizontalHeaderItem(2, newItem);

        ui->tableWidget_transformSelectedPos->insertColumn(3);
        newItem = new QTableWidgetItem();
        newItem->setText("Z");
        ui->tableWidget_transformSelectedPos->setHorizontalHeaderItem(3, newItem);

        granny_int32 ctrlPerKnot = curve->ControlCount/curve->KnotCount;
        granny_int32 indexControl = 0;
        for(granny_int32 i = 0; i < curve->KnotCount;i++){
            int row = ui->tableWidget_transformSelectedPos->rowCount();
            ui->tableWidget_transformSelectedPos->insertRow(row);

            QTableWidgetItem * newItemKnot = new QTableWidgetItem(QString::number(curve->Knots[i]));
            QTableWidgetItem * newItemPosX = new QTableWidgetItem(QString::number(curve->Controls[indexControl]));
            QTableWidgetItem * newItemPosY = new QTableWidgetItem(QString::number(curve->Controls[indexControl+1]));
            QTableWidgetItem * newItemPosZ = new QTableWidgetItem(QString::number(curve->Controls[indexControl+2]));

            ui->tableWidget_transformSelectedPos->setItem(row, 0, newItemKnot);
            ui->tableWidget_transformSelectedPos->setItem(row, 1, newItemPosX);
            ui->tableWidget_transformSelectedPos->setItem(row, 2, newItemPosY);
            ui->tableWidget_transformSelectedPos->setItem(row, 3, newItemPosZ);

            indexControl += ctrlPerKnot;
        }
        ui->tableWidget_transformSelectedPos->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    }
}
template<>
void TrackgroupView::loadTransformerTrackSelectedOri(granny_curve_data_da_k32f_c32f *curve){
    for(int i = ui->tableWidget_transformSelectedOri->rowCount(); i >= 0; i--){
        ui->tableWidget_transformSelectedOri->removeRow(i);
    }
    if(curve == nullptr){
        return;
    }
    if(curve->KnotCount != 0){
        for(int i = ui->tableWidget_transformSelectedOri->columnCount(); i >= 0; i--){
            ui->tableWidget_transformSelectedOri->removeColumn(i);
        }

        ui->tableWidget_transformSelectedOri->insertColumn(0);
        QTableWidgetItem * newItem = new QTableWidgetItem();
        newItem->setText("Time");
        ui->tableWidget_transformSelectedOri->setHorizontalHeaderItem(0, newItem);

        ui->tableWidget_transformSelectedOri->insertColumn(1);
        newItem = new QTableWidgetItem();
        newItem->setText("A");
        ui->tableWidget_transformSelectedOri->setHorizontalHeaderItem(1, newItem);

        ui->tableWidget_transformSelectedOri->insertColumn(2);
        newItem = new QTableWidgetItem();
        newItem->setText("B");
        ui->tableWidget_transformSelectedOri->setHorizontalHeaderItem(2, newItem);

        ui->tableWidget_transformSelectedOri->insertColumn(3);
        newItem = new QTableWidgetItem();
        newItem->setText("C");
        ui->tableWidget_transformSelectedOri->setHorizontalHeaderItem(3, newItem);

        ui->tableWidget_transformSelectedOri->insertColumn(4);
        newItem = new QTableWidgetItem();
        newItem->setText("D");
        ui->tableWidget_transformSelectedOri->setHorizontalHeaderItem(4, newItem);

        granny_int32 ctrlPerKnot = curve->ControlCount/curve->KnotCount;
        granny_int32 indexControl = 0;
        for(granny_int32 i = 0; i < curve->KnotCount;i++){
            int row = ui->tableWidget_transformSelectedOri->rowCount();
            ui->tableWidget_transformSelectedOri->insertRow(row);

            QTableWidgetItem * newItemKnot = new QTableWidgetItem(QString::number(curve->Knots[i]));
            QTableWidgetItem * newItemPosA = new QTableWidgetItem(QString::number(curve->Controls[indexControl]));
            QTableWidgetItem * newItemPosB = new QTableWidgetItem(QString::number(curve->Controls[indexControl+1]));
            QTableWidgetItem * newItemPosC = new QTableWidgetItem(QString::number(curve->Controls[indexControl+2]));
            QTableWidgetItem * newItemPosD = new QTableWidgetItem(QString::number(curve->Controls[indexControl+3]));

            ui->tableWidget_transformSelectedOri->setItem(row, 0, newItemKnot);
            ui->tableWidget_transformSelectedOri->setItem(row, 1, newItemPosA);
            ui->tableWidget_transformSelectedOri->setItem(row, 2, newItemPosB);
            ui->tableWidget_transformSelectedOri->setItem(row, 3, newItemPosC);
            ui->tableWidget_transformSelectedOri->setItem(row, 4, newItemPosD);

            indexControl += ctrlPerKnot;
        }
        ui->tableWidget_transformSelectedOri->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    }
}
template<>
void TrackgroupView::loadTransformerTrackSelectedScale(granny_curve_data_da_k32f_c32f *curve){
    for(int i = ui->tableWidget_transformSelectedScale->rowCount(); i >= 0; i--){
        ui->tableWidget_transformSelectedScale->removeRow(i);
    }
    if(curve == nullptr){
        return;
    }
    if(curve->KnotCount != 0){
        for(int i = ui->tableWidget_transformSelectedScale->columnCount(); i >= 0; i--){
            ui->tableWidget_transformSelectedScale->removeColumn(i);
        }
        unsigned int i = 0;
        ui->tableWidget_transformSelectedScale->insertColumn(i);
        QTableWidgetItem * newItem = new QTableWidgetItem();
        newItem->setText("Time");
        ui->tableWidget_transformSelectedScale->setHorizontalHeaderItem(i++, newItem);

        ui->tableWidget_transformSelectedScale->insertColumn(i);
        newItem = new QTableWidgetItem();
        newItem->setText("XX");
        ui->tableWidget_transformSelectedScale->setHorizontalHeaderItem(i++, newItem);

        ui->tableWidget_transformSelectedScale->insertColumn(i);
        newItem = new QTableWidgetItem();
        newItem->setText("XY");
        ui->tableWidget_transformSelectedScale->setHorizontalHeaderItem(i++, newItem);

        ui->tableWidget_transformSelectedScale->insertColumn(i);
        newItem = new QTableWidgetItem();
        newItem->setText("XZ");
        ui->tableWidget_transformSelectedScale->setHorizontalHeaderItem(i++, newItem);

        ui->tableWidget_transformSelectedScale->insertColumn(i);
        newItem = new QTableWidgetItem();
        newItem->setText("YX");
        ui->tableWidget_transformSelectedScale->setHorizontalHeaderItem(i++, newItem);

        ui->tableWidget_transformSelectedScale->insertColumn(i);
        newItem = new QTableWidgetItem();
        newItem->setText("YY");
        ui->tableWidget_transformSelectedScale->setHorizontalHeaderItem(i++, newItem);

        ui->tableWidget_transformSelectedScale->insertColumn(i);
        newItem = new QTableWidgetItem();
        newItem->setText("YZ");
        ui->tableWidget_transformSelectedScale->setHorizontalHeaderItem(i++, newItem);

        ui->tableWidget_transformSelectedScale->insertColumn(i);
        newItem = new QTableWidgetItem();
        newItem->setText("ZX");
        ui->tableWidget_transformSelectedScale->setHorizontalHeaderItem(i++, newItem);

        ui->tableWidget_transformSelectedScale->insertColumn(i);
        newItem = new QTableWidgetItem();
        newItem->setText("ZY");
        ui->tableWidget_transformSelectedScale->setHorizontalHeaderItem(i++, newItem);

        ui->tableWidget_transformSelectedScale->insertColumn(i);
        newItem = new QTableWidgetItem();
        newItem->setText("ZZ");
        ui->tableWidget_transformSelectedScale->setHorizontalHeaderItem(i++, newItem);

        granny_int32 ctrlPerKnot = curve->ControlCount/curve->KnotCount;
        granny_int32 indexControl = 0;
        for(granny_int32 i = 0; i < curve->KnotCount;i++){
            int row = ui->tableWidget_transformSelectedScale->rowCount();
            ui->tableWidget_transformSelectedScale->insertRow(row);

            QTableWidgetItem * newItemKnot = new QTableWidgetItem(QString::number(curve->Knots[i]));
            QTableWidgetItem * newItemPosXX = new QTableWidgetItem(QString::number(curve->Controls[indexControl]));
            QTableWidgetItem * newItemPosXY = new QTableWidgetItem(QString::number(curve->Controls[indexControl+1]));
            QTableWidgetItem * newItemPosXZ = new QTableWidgetItem(QString::number(curve->Controls[indexControl+2]));
            QTableWidgetItem * newItemPosYX = new QTableWidgetItem(QString::number(curve->Controls[indexControl+3]));
            QTableWidgetItem * newItemPosYY = new QTableWidgetItem(QString::number(curve->Controls[indexControl+4]));
            QTableWidgetItem * newItemPosYZ = new QTableWidgetItem(QString::number(curve->Controls[indexControl+5]));
            QTableWidgetItem * newItemPosZX = new QTableWidgetItem(QString::number(curve->Controls[indexControl+6]));
            QTableWidgetItem * newItemPosZY = new QTableWidgetItem(QString::number(curve->Controls[indexControl+7]));
            QTableWidgetItem * newItemPosZZ = new QTableWidgetItem(QString::number(curve->Controls[indexControl+8]));

            ui->tableWidget_transformSelectedScale->setItem(row, 0, newItemKnot);
            ui->tableWidget_transformSelectedScale->setItem(row, 1, newItemPosXX);
            ui->tableWidget_transformSelectedScale->setItem(row, 2, newItemPosXY);
            ui->tableWidget_transformSelectedScale->setItem(row, 3, newItemPosXZ);
            ui->tableWidget_transformSelectedScale->setItem(row, 4, newItemPosYX);
            ui->tableWidget_transformSelectedScale->setItem(row, 5, newItemPosYY);
            ui->tableWidget_transformSelectedScale->setItem(row, 6, newItemPosYZ);
            ui->tableWidget_transformSelectedScale->setItem(row, 7, newItemPosZX);
            ui->tableWidget_transformSelectedScale->setItem(row, 8, newItemPosZY);
            ui->tableWidget_transformSelectedScale->setItem(row, 9, newItemPosZZ);

            indexControl += ctrlPerKnot;
        }
        ui->tableWidget_transformSelectedScale->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    }
}
template<>
void TrackgroupView::loadTransformerTrackSelectedParam(granny_curve_data_da_k32f_c32f *curvePos,
                                                       granny_curve_data_da_k32f_c32f *curveOri,
                                                       granny_curve_data_da_k32f_c32f *curveScale){
    //Pos
    for(int i = ui->gridLayout_transformSelectedPos->rowCount(); i >= 0; i--){
        GridLayoutUtil::removeRow(ui->gridLayout_transformSelectedPos, i, true);
    }
    //Padding
    QLabel * ptrLabelPaddingPos = new QLabel();
    ptrLabelPaddingPos->setText("Padding");
    QLabel * ptrLabelValPaddingPos = new QLabel();
    ptrLabelValPaddingPos->setText(QString::number(curvePos->Padding));
    //Format
    QLabel * ptrLabelFormatPos = new QLabel();
    ptrLabelFormatPos->setText("Format");
    QLabel * ptrLabelValFormatPos = new QLabel();
    ptrLabelValFormatPos->setText(QString::number(curvePos->CurveDataHeader.Format));
    //Padding
    QLabel * ptrLabelDegreePos = new QLabel();
    ptrLabelDegreePos->setText("Degree");
    QLabel * ptrLabelValDegreePos = new QLabel();
    ptrLabelValDegreePos->setText(QString::number(curvePos->CurveDataHeader.Degree));

    ui->gridLayout_transformSelectedPos->addWidget(ptrLabelFormatPos, 0,0);
    ui->gridLayout_transformSelectedPos->addWidget(ptrLabelValFormatPos, 0,1);
    ui->gridLayout_transformSelectedPos->addWidget(ptrLabelDegreePos, 1,0);
    ui->gridLayout_transformSelectedPos->addWidget(ptrLabelValDegreePos, 1,1);
    ui->gridLayout_transformSelectedPos->addWidget(ptrLabelPaddingPos, 2,0);
    ui->gridLayout_transformSelectedPos->addWidget(ptrLabelValPaddingPos, 2,1);

    //Orientation
    for(int i = ui->gridLayout_transformSelectedOri->rowCount(); i >= 0; i--){
        GridLayoutUtil::removeRow(ui->gridLayout_transformSelectedOri, i, true);
    }
    QLabel * ptrLabelPaddingOri = new QLabel();
    ptrLabelPaddingOri->setText("Padding");
    QLabel * ptrLabelValPaddingOri = new QLabel();
    ptrLabelValPaddingOri->setText(QString::number(curveOri->Padding));
    //Format
    QLabel * ptrLabelFormatOri = new QLabel();
    ptrLabelFormatOri->setText("Format");
    QLabel * ptrLabelValFormatOri = new QLabel();
    ptrLabelValFormatOri->setText(QString::number(curveOri->CurveDataHeader.Format));
    //Padding
    QLabel * ptrLabelDegreeOri = new QLabel();
    ptrLabelDegreeOri->setText("Degree");
    QLabel * ptrLabelValDegreeOri = new QLabel();
    ptrLabelValDegreeOri->setText(QString::number(curveOri->CurveDataHeader.Degree));

    ui->gridLayout_transformSelectedOri->addWidget(ptrLabelFormatOri, 0,0);
    ui->gridLayout_transformSelectedOri->addWidget(ptrLabelValFormatOri, 0,1);
    ui->gridLayout_transformSelectedOri->addWidget(ptrLabelDegreeOri, 1,0);
    ui->gridLayout_transformSelectedOri->addWidget(ptrLabelValDegreeOri, 1,1);
    ui->gridLayout_transformSelectedOri->addWidget(ptrLabelPaddingOri, 2,0);
    ui->gridLayout_transformSelectedOri->addWidget(ptrLabelValPaddingOri, 2,1);

    //Scale
    for(int i = ui->gridLayout_transformSelectedScale->rowCount(); i >= 0; i--){
        GridLayoutUtil::removeRow(ui->gridLayout_transformSelectedScale, i, true);
    }
    QLabel * ptrLabelPaddingScale = new QLabel();
    ptrLabelPaddingScale->setText("Padding");
    QLabel * ptrLabelValPaddingScale = new QLabel();
    ptrLabelValPaddingScale->setText(QString::number(curveScale->Padding));
    //Format
    QLabel * ptrLabelFormatScale = new QLabel();
    ptrLabelFormatScale->setText("Format");
    QLabel * ptrLabelValFormatScale = new QLabel();
    ptrLabelValFormatScale->setText(QString::number(curveScale->CurveDataHeader.Format));
    //Padding
    QLabel * ptrLabelDegreeScale = new QLabel();
    ptrLabelDegreeScale->setText("Degree");
    QLabel * ptrLabelValDegreeScale = new QLabel();
    ptrLabelValDegreeScale->setText(QString::number(curveScale->CurveDataHeader.Degree));

    ui->gridLayout_transformSelectedScale->addWidget(ptrLabelFormatScale, 0,0);
    ui->gridLayout_transformSelectedScale->addWidget(ptrLabelValFormatScale, 0,1);
    ui->gridLayout_transformSelectedScale->addWidget(ptrLabelDegreeScale, 1,0);
    ui->gridLayout_transformSelectedScale->addWidget(ptrLabelValDegreeScale, 1,1);
    ui->gridLayout_transformSelectedScale->addWidget(ptrLabelPaddingScale, 2,0);
    ui->gridLayout_transformSelectedScale->addWidget(ptrLabelValPaddingScale, 2,1);
}

void TrackgroupView::addTrackgroups(granny_int32 TrackGroupCount, granny_track_group ** TrackGroups){
    //Verifie le pointeur vers le tableau de TrackGroupss
    if(TrackGroups == nullptr){
        return;
    }
    //Variable iteration TrackGroups
    granny_int32 index;
    for(index = 0; index < TrackGroupCount;index++){
        //Pour tout les modèles du tableau
        try{
            if(TrackGroups[index] != nullptr){
                //Si le pointeur est valid
                try {
                    //stock le pointeur dans une variable locale
                    granny_track_group * curTrackgroup = TrackGroups[index];
                    if(curTrackgroup != nullptr){
                        //Si le pointeur du modèle est valide on recupère le nombre de ligne
                        int row = ui->tableWidget_trackgroups->rowCount();
                        //on ajoute une ligne et on increment le nombre de ligne
                        ui->tableWidget_trackgroups->insertRow(row);
                        QString trackGroupsName;
                        //Recupère le nombre de colone
                        //int column = ui->tableWidget_trackgroups->columnCount();
                        QTableWidgetItem *newItem = nullptr;
                        if(curTrackgroup->Name != nullptr){
                            //Si le nom du modèle est valide
                            trackGroupsName = QString::fromLocal8Bit(curTrackgroup->Name);
                        }
                        else{
                            //Si le nom du modèle n'est pas valide on genere un nom
                            trackGroupsName = QString::number((int)curTrackgroup, 16);
                        }
                        //Crée l'objet représentant le modèle dans la liste
                        newItem = new QTableWidgetItem(trackGroupsName);
                        //Ajoute le modèle à la liste
                        ui->tableWidget_trackgroups->setItem(row, 0, newItem);
                        mapListTrackgroup[row] = curTrackgroup;
                    }
                }catch (...){
                    log += "Error accessing TrackGroupss " + QString::number(index);
                }
            }
        }catch (...) {
            log += "Error accessing TrackGroupss table" + QString::number(index);
        }
    }
    if(index != 0){
        loadTrackgroupSelected(TrackGroups[0]);
    }
}

void TrackgroupView::resetTrackgroups(void){
    ui->tableWidget_trackgroups->clear();
    for(int i = ui->tableWidget_trackgroups->rowCount(); i >=0; i--){
        ui->tableWidget_trackgroups->removeRow(i);
    }
    ui->tableWidget_trackgroups->setHorizontalHeaderItem(0,new QTableWidgetItem("Name"));

    resetTransformerTrack();
    resetTrackLOD();
    resetTextTrack();
    resetVectorTrack();

    ui->label_valTrackgroupsFlags->setText("None");
    ui->label_trackgroupsLoopTranslat0->setText("None");
    ui->label_trackgroupsLoopTranslat1->setText("None");
    ui->label_trackgroupsLoopTranslat2->setText("None");
    ui->label_trackgroupsSelectedInitPlacmentFlag->setText("None");
    ui->label_trackgroupsSelectedInitPlacmentPos0->setText("None");
    ui->label_trackgroupsSelectedInitPlacmentPos1->setText("None");
    ui->label_trackgroupsSelectedInitPlacmentPos2->setText("None");
    ui->label_valTrackgroupsSelectedInitPlacmentOri0->setText("None");
    ui->label_valTrackgroupsSelectedInitPlacmentOri1->setText("None");
    ui->label_valTrackgroupsSelectedInitPlacmentOri2->setText("None");
    ui->label_valTrackgroupsSelectedInitPlacmentOri3->setText("None");
    matrixInitPlacmentScale->reset();
    ui->label_trackgroupsPeriodicLoopRadius->setText("None");
    ui->label_trackgroupsPeriodicLoopDAngle->setText("None");
    ui->label_trackgroupsPeriodicLoopDZ->setText("None");
    ui->label_valTrackgroupsPeriodicLoopAxeBasisX0->setText("None");
    ui->label_valTrackgroupsPeriodicLoopAxeBasisX1->setText("None");
    ui->label_valTrackgroupsPeriodicLoopAxeBasisX2->setText("None");
    ui->label_valTrackgroupsPeriodicLoopAxeBasisY0->setText("None");
    ui->label_valTrackgroupsPeriodicLoopAxeBasisY1->setText("None");
    ui->label_valTrackgroupsPeriodicLoopAxeBasisY2->setText("None");
    ui->label_valTrackgroupsPeriodicLoopAxeAxis0->setText("None");
    ui->label_valTrackgroupsPeriodicLoopAxeAxis1->setText("None");
    ui->label_valTrackgroupsPeriodicLoopAxeAxis2->setText("None");
}
//Affiche le trackgroup selectionné
void TrackgroupView::loadTrackgroupSelected(granny_track_group *Trackgroups){
    mapListVectorTrack.clear();
    mapListTransformTrack.clear();
    mapListTextTrack.clear();
    if(Trackgroups == nullptr){
        return;
    }
    try {
        loadTrackgroupSelectedFlags(Trackgroups);
        loadTrackgroupSelectedLoopTranslation(Trackgroups);
        loadTrackgroupSelectedInitPlacment(Trackgroups);
        loadTrackgroupSelectedPeriodicLoop(Trackgroups);
        loadVectorTrack(Trackgroups);
        loadTransformerTrack(Trackgroups);
        loadTrackLOD(Trackgroups);
        loadTextTrack(Trackgroups);
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Affiche le flag du trackgroup selectionné
void TrackgroupView::loadTrackgroupSelectedFlags(granny_track_group *Trackgroups){
    if(Trackgroups == nullptr){
        return;
    }
    try {
        QString str;
        switch (Trackgroups->Flags) {
            case GrannyAccumulationExtracted:{
                str = "GrannyAccumulationExtracted";
                break;
            }
            case GrannyTrackGroupIsSorted:{
                str = "GrannyTrackGroupIsSorted";
                break;
            }
            case GrannyAccumulationIsVDA:{
                str = "GrannyAccumulationIsVDA";
                break;
            }
            case GrannyTrackGroupIsMorphs:{
                str = "GrannyTrackGroupIsMorphs";
                break;
            }
            case Grannytrack_group_flags_forceint:{
                str = "Grannytrack_group_flags_forceint";
                break;
            }
            default:
                str = "Unknown flag (" + QString::number(Trackgroups->Flags) + ")";
                break;
        }
        ui->label_valTrackgroupsFlags->setText(str);
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Affiche la loop translation du trackgroup selectionné
void TrackgroupView::loadTrackgroupSelectedLoopTranslation(granny_track_group *Trackgroups){
    if(Trackgroups == nullptr){
        return;
    }
    try {
        ui->label_trackgroupsLoopTranslat0->setText(QString::number(Trackgroups->LoopTranslation[0]));
        ui->label_trackgroupsLoopTranslat1->setText(QString::number(Trackgroups->LoopTranslation[1]));
        ui->label_trackgroupsLoopTranslat2->setText(QString::number(Trackgroups->LoopTranslation[2]));
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Affiche l'init placement du trackgroup selectionné
void TrackgroupView::loadTrackgroupSelectedInitPlacment(granny_track_group *Trackgroups){
    if(Trackgroups == nullptr){
        return;
    }
    try {
        loadTrackgroupSelectedInitPlacmentFlags(Trackgroups);
        loadTrackgroupSelectedInitPlacmentPos(Trackgroups);
        loadTrackgroupSelectedInitPlacmentOri(Trackgroups);
        loadTrackgroupSelectedInitPlacmentScales(Trackgroups);
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Affiche le flag de l'init placement
void TrackgroupView::loadTrackgroupSelectedInitPlacmentFlags(granny_track_group *Trackgroups){
    if(Trackgroups == nullptr){
        return;
    }
    try {
        QString strFlag;
        switch (Trackgroups->InitialPlacement.Flags) {
            case GrannyHasPosition:{
                strFlag = "GrannyHasPosition";
                break;
            }
            case GrannyHasOrientation:{
                strFlag = "GrannyHasOrientation";
                break;
            }
            case GrannyHasScaleShear:{
                strFlag = "GrannyHasScaleShear";
                break;
            }
            case Grannytransform_flags_forceint:{
                strFlag = "Grannytransform_flags_forceint";
                break;
            }
            default:{
                strFlag = "Unkown flag (" + QString::number(Trackgroups->InitialPlacement.Flags) + ")";
            }
        }
        this->ui->label_trackgroupsSelectedInitPlacmentFlag->setText(strFlag);
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Affiche la position de l'init placement
void TrackgroupView::loadTrackgroupSelectedInitPlacmentPos(granny_track_group *Trackgroups){
    if(Trackgroups == nullptr){
        return;
    }
    try {
        ui->label_trackgroupsSelectedInitPlacmentPos0->setText(QString::number(Trackgroups->InitialPlacement.Position[0]));
        ui->label_trackgroupsSelectedInitPlacmentPos1->setText(QString::number(Trackgroups->InitialPlacement.Position[1]));
        ui->label_trackgroupsSelectedInitPlacmentPos2->setText(QString::number(Trackgroups->InitialPlacement.Position[2]));
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Affiche l'origine de l'init placement
void TrackgroupView::loadTrackgroupSelectedInitPlacmentOri(granny_track_group *Trackgroups){
    if(Trackgroups == nullptr){
        return;
    }
    try {
        ui->label_valTrackgroupsSelectedInitPlacmentOri0->setText(QString::number(Trackgroups->InitialPlacement.Orientation[0]));
        ui->label_valTrackgroupsSelectedInitPlacmentOri1->setText(QString::number(Trackgroups->InitialPlacement.Orientation[1]));
        ui->label_valTrackgroupsSelectedInitPlacmentOri2->setText(QString::number(Trackgroups->InitialPlacement.Orientation[2]));
        ui->label_valTrackgroupsSelectedInitPlacmentOri3->setText(QString::number(Trackgroups->InitialPlacement.Orientation[3]));
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Affiche la scale de l'init placement
void TrackgroupView::loadTrackgroupSelectedInitPlacmentScales(granny_track_group *Trackgroups){
    if(Trackgroups == nullptr){
        return;
    }
    try {
        matrixInitPlacmentScale->setValues(Trackgroups->InitialPlacement.ScaleShear);
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Affiche la periodic loop du trackgroup selectionné
void TrackgroupView::loadTrackgroupSelectedPeriodicLoop(granny_track_group *Trackgroups){
    if(Trackgroups == nullptr){
        return;
    }
    if(Trackgroups->PeriodicLoop == nullptr){
        return;
    }
    try {
        loadTrackgroupSelectedPeriodicLoopRadius(Trackgroups);
        loadTrackgroupSelectedPeriodicLoopDAngle(Trackgroups);
        loadTrackgroupSelectedPeriodicLoopDZ(Trackgroups);
        loadTrackgroupSelectedPeriodicLoopBasisX(Trackgroups);
        loadTrackgroupSelectedPeriodicLoopBasisY(Trackgroups);
        loadTrackgroupSelectedPeriodicLoopAxis(Trackgroups);
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Affiche le radius de la periodic loop
void TrackgroupView::loadTrackgroupSelectedPeriodicLoopRadius(granny_track_group *Trackgroups){
    if(Trackgroups == nullptr){
        return;
    }
    if(Trackgroups->PeriodicLoop == nullptr){
        return;
    }
    try {
        ui->label_trackgroupsPeriodicLoopRadius->setText(QString::number(Trackgroups->PeriodicLoop->Radius));
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Affiche le DAngle de la periodic loop
void TrackgroupView::loadTrackgroupSelectedPeriodicLoopDAngle(granny_track_group *Trackgroups){
    if(Trackgroups == nullptr){
        return;
    }
    try {
        ui->label_trackgroupsPeriodicLoopDAngle->setText(QString::number(Trackgroups->PeriodicLoop->dAngle));
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Affiche le DZ de la periodic loop
void TrackgroupView::loadTrackgroupSelectedPeriodicLoopDZ(granny_track_group *Trackgroups){
    if(Trackgroups == nullptr){
        return;
    }
    try {
        ui->label_trackgroupsPeriodicLoopDZ->setText(QString::number(Trackgroups->PeriodicLoop->dZ));
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Affiche le BasisX de la periodic loop
void TrackgroupView::loadTrackgroupSelectedPeriodicLoopBasisX(granny_track_group *Trackgroups){
    if(Trackgroups == nullptr){
        return;
    }
    try {
        ui->label_valTrackgroupsPeriodicLoopAxeBasisX0->setText(QString::number(Trackgroups->PeriodicLoop->BasisX[0]));
        ui->label_valTrackgroupsPeriodicLoopAxeBasisX1->setText(QString::number(Trackgroups->PeriodicLoop->BasisX[1]));
        ui->label_valTrackgroupsPeriodicLoopAxeBasisX2->setText(QString::number(Trackgroups->PeriodicLoop->BasisX[2]));
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Affiche le BasisY de la periodic loop
void TrackgroupView::loadTrackgroupSelectedPeriodicLoopBasisY(granny_track_group *Trackgroups){
    if(Trackgroups == nullptr){
        return;
    }
    try {
        ui->label_valTrackgroupsPeriodicLoopAxeBasisY0->setText(QString::number(Trackgroups->PeriodicLoop->BasisY[0]));
        ui->label_valTrackgroupsPeriodicLoopAxeBasisY1->setText(QString::number(Trackgroups->PeriodicLoop->BasisY[1]));
        ui->label_valTrackgroupsPeriodicLoopAxeBasisY2->setText(QString::number(Trackgroups->PeriodicLoop->BasisY[2]));
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Affiche le Axis de la periodic loop
void TrackgroupView::loadTrackgroupSelectedPeriodicLoopAxis(granny_track_group *Trackgroups){
    if(Trackgroups == nullptr){
        return;
    }
    try {
        ui->label_valTrackgroupsPeriodicLoopAxeAxis0->setText(QString::number(Trackgroups->PeriodicLoop->Axis[0]));
        ui->label_valTrackgroupsPeriodicLoopAxeAxis1->setText(QString::number(Trackgroups->PeriodicLoop->Axis[1]));
        ui->label_valTrackgroupsPeriodicLoopAxeAxis2->setText(QString::number(Trackgroups->PeriodicLoop->Axis[2]));
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Charge les vecteurs trackgroup dans le tableau
void TrackgroupView::loadVectorTrack(granny_track_group *Trackgroups){
    resetVectorTrack();
    if(Trackgroups == nullptr){
        return;
    }
    if(Trackgroups->VectorTracks == nullptr){
        return;
    }
    try {
        for(granny_int32 i = 0; i < Trackgroups->VectorTrackCount; i++){
            int row = ui->tableWidget_vectorTrack->rowCount();

            QTableWidgetItem * newItemName = new QTableWidgetItem(Trackgroups->VectorTracks[i].Name);
            QTableWidgetItem * newItemKey = new QTableWidgetItem(Trackgroups->VectorTracks[i].TrackKey);
            QTableWidgetItem * newItemDim = new QTableWidgetItem(Trackgroups->VectorTracks[i].Dimension);

            ui->tableWidget_vectorTrack->setItem(row, 0, newItemName);
            ui->tableWidget_vectorTrack->setItem(row, 1, newItemKey);
            ui->tableWidget_vectorTrack->setItem(row, 2, newItemDim);

            mapListVectorTrack[row] = &Trackgroups->VectorTracks[i];
        }
        ui->tableWidget_vectorTrack->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
void TrackgroupView::loadVectorTrackSelected( granny_vector_track  *VectorTrack){
    if(VectorTrack == nullptr){
        return;
    }
    try {
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Charge les tranform track dans le tableau
void TrackgroupView::loadTransformerTrack(granny_track_group *Trackgroups){
    resetTransformerTrack();
    if(Trackgroups == nullptr){
        return;
    }
    if(Trackgroups->TransformTracks == nullptr){
        return;
    }
    try {
        for(granny_int32 i = 0; i < Trackgroups->TransformTrackCount; i++){
            int row = ui->tableWidget_trackgroupsTransform->rowCount();

            ui->tableWidget_trackgroupsTransform->insertRow(row);
            QTableWidgetItem * newItemName = new QTableWidgetItem(Trackgroups->TransformTracks[i].Name);
            QString str;
            switch (Trackgroups->TransformTracks[i].Flags) {
                case GrannyUseAccumulatorNeighborhood:{
                    str = "GrannyUseAccumulatorNeighborhood";
                    break;
                }
                case Grannytransform_track_flags_forceint:{
                    str = "Grannytransform_track_flags_forceint";
                    break;
                }
                default:
                    str = "unknow flag (" + QString::number(Trackgroups->TransformTracks[i].Flags) + ")";
            }
            QTableWidgetItem * newItemFlag = new QTableWidgetItem(str);

            ui->tableWidget_trackgroupsTransform->setItem(row, 0, newItemName);
            ui->tableWidget_trackgroupsTransform->setItem(row, 1, newItemFlag);

            mapListTransformTrack[row] = &Trackgroups->TransformTracks[i];
        }
        ui->tableWidget_trackgroupsTransform->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
void TrackgroupView::loadTransformerTrackSelected(granny_transform_track *TransformTrack){
    if(TransformTrack == nullptr){
        return;
    }
    try {
        granny_variant * posCurve =  &TransformTrack->PositionCurve.CurveData;
        granny_variant * oriCurve =  &TransformTrack->OrientationCurve.CurveData;
        granny_variant * scaleCurve =  &TransformTrack->ScaleShearCurve.CurveData;

        //granny_data_type_definition const * posType =  GrannyCurveGetDataTypeDefinition(&TransformTrack->PositionCurve);

        if(strncmp(posCurve->Type->Name, "CurveDataHeader_DaK32fC32f", strlen("CurveDataHeader_DaK32fC32f")) == 0){
            granny_curve_data_da_k32f_c32f curvePos;
            granny_curve_data_da_k32f_c32f curveOri;
            granny_curve_data_da_k32f_c32f curveScale;
            curvePos.Padding = 0;
            curveOri.Padding = 0;
            curveScale.Padding = 0;
            GrannyConvertSingleObject(posCurve->Type, posCurve->Object, posCurve->Type, &curvePos, 0);
            GrannyConvertSingleObject(oriCurve->Type, oriCurve->Object, oriCurve->Type, &curveOri, 0);
            GrannyConvertSingleObject(scaleCurve->Type, scaleCurve->Object, scaleCurve->Type, &curveScale, 0);

            //position
            loadTransformerTrackSelectedPos(&curvePos);
            //Orientation
            loadTransformerTrackSelectedOri(&curveOri);
            //Scale
            loadTransformerTrackSelectedScale(&curveScale);
            //Additionnal parameters
            loadTransformerTrackSelectedParam(&curvePos, &curveOri, &curveScale);
        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_DaK16uC16u", strlen("CurveDataHeader_DaK16uC16u")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_DaK8uC8u", strlen("CurveDataHeader_DaK8uC8u")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_D3K16uC16u", strlen("CurveDataHeader_D3K16uC16u")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_D3K8uC8u", strlen("CurveDataHeader_D3K8uC8u")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_D4nK16uC15u", strlen("CurveDataHeader_D4nK16uC15u")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_D4nK8uC7u", strlen("CurveDataHeader_D4nK8uC7u")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_DaIdentity", strlen("CurveDataHeader_DaIdentity")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_DaConstant32f", strlen("CurveDataHeader_DaConstant32f")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_D3Constant32f", strlen("CurveDataHeader_D3Constant32f")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_D4Constant32f", strlen("CurveDataHeader_D4Constant32f")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_DaKeyframes32f", strlen("CurveDataHeader_DaKeyframes32f")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_D9I1K16uC16u", strlen("CurveDataHeader_D9I1K16uC16u")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_D9I3K16uC16u", strlen("CurveDataHeader_D9I3K16uC16u")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_D9I1K8uC8u", strlen("CurveDataHeader_D9I1K8uC8u")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_D9I3K8uC8u", strlen("CurveDataHeader_D9I3K8uC8u")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_D3I1K32fC32f", strlen("CurveDataHeader_D3I1K32fC32f")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_D3I1K16uC16u", strlen("CurveDataHeader_D3I1K16uC16u")) == 0){

        }
        else if(strncmp(posCurve->Type->Name, "CurveDataHeader_D3I1K8uC8u", strlen("CurveDataHeader_D3I1K8uC8u")) == 0){

        }
        else{

        }


    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}


//Charge les track LOD dans le tableau
void TrackgroupView::loadTrackLOD(granny_track_group *Trackgroups){
    resetTrackLOD();
    if(Trackgroups == nullptr){
        return;
    }
    if(Trackgroups->TransformLODErrors == nullptr){
        return;
    }
    try {
        for(granny_int32 i = 0; i < Trackgroups->TransformLODErrorCount; i++){
            int row = ui->tableWidget_vectorTrack->rowCount();

            QTableWidgetItem * newItemLOD = new QTableWidgetItem(QString::number(Trackgroups->TransformLODErrors[i]));
            ui->tableWidget_trackgroupsTransform->setItem(row, 0, newItemLOD);
        }
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Charge les text track dans le tableau
void TrackgroupView::loadTextTrack(granny_track_group *Trackgroups){
    resetTextTrack();
    ui->tableWidget_trackgroupsText->clear();
    ui->tableWidget_trackgroupsText->setHorizontalHeaderItem(0,new QTableWidgetItem("Time stamp"));
    ui->tableWidget_trackgroupsText->setHorizontalHeaderItem(1,new QTableWidgetItem("Text"));
    if(Trackgroups == nullptr){
        return;
    }
    if(Trackgroups->TextTracks == nullptr){
        return;
    }
    try {
        for(granny_int32 i = 0; i < Trackgroups->TextTrackCount; i++){
            int row = ui->tableWidget_trackgroupsText->rowCount();

            QTableWidgetItem * newItemName = new QTableWidgetItem(Trackgroups->TextTracks[i].Name);
            QTableWidgetItem * newItemEntrieCnt = new QTableWidgetItem(QString::number(Trackgroups->TextTracks[i].EntryCount));

            ui->tableWidget_trackgroupsText->setItem(row, 0, newItemName);
            ui->tableWidget_trackgroupsText->setItem(row, 1, newItemEntrieCnt);

            mapListTextTrack[row] = &Trackgroups->TextTracks[i];
        }
        ui->tableWidget_trackgroupsText->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Charge les info du text track selectionné
void TrackgroupView::loadTextTrackSelected(granny_text_track * TextTrack){
    if(TextTrack == nullptr){
        return;
    }
    try {
        for(granny_int32 i = 0; i < TextTrack->EntryCount; i++){
            int row = ui->tableWidget_trackgroupsTextEntries->rowCount();

            QTableWidgetItem * newItemTimeStamp = new QTableWidgetItem(QString::number(TextTrack->Entries[i].TimeStamp));
            QTableWidgetItem * newItemText = new QTableWidgetItem(TextTrack->Entries[i].Text);

            ui->tableWidget_trackgroupsTextEntries->setItem(row, 0, newItemTimeStamp);
            ui->tableWidget_trackgroupsTextEntries->setItem(row, 1, newItemText);
        }
        ui->tableWidget_trackgroupsTextEntries->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    } catch (...) {
        log += "Error lecture tackgroup selected\n";
    }
}
//Utilisateur clique sur la table des trackgroup
void TrackgroupView::on_tableWidget_trackgroups_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(TrackgroupSelected != row){
        loadTrackgroupSelected(mapListTrackgroup[row]);
        TrackgroupSelected = row;
    }
}
void TrackgroupView::on_tableWidget_trackgroups_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(previousRow == currentRow){
        return;
    }
    if(currentColumn < 0){
        //anti warning
        if(previousColumn != 0){
            return;
        }
        return;
    }
    if(TrackgroupSelected != currentRow){
        loadTrackgroupSelected(mapListTrackgroup[currentRow]);
        TrackgroupSelected = currentRow;
    }
}
//Utilisateur clique sur la table des vector track
void TrackgroupView::on_tableWidget_vectorTrack_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(VectorSelected != row){
        loadVectorTrackSelected(mapListVectorTrack[row]);
        VectorSelected = row;
    }
}
void TrackgroupView::on_tableWidget_vectorTrack_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(previousRow == currentRow){
        return;
    }
    if(currentColumn < 0){
        //anti warning
        if(previousColumn != 0){
            return;
        }
        return;
    }
    if(VectorSelected != currentRow){
        loadVectorTrackSelected(mapListVectorTrack[currentRow]);
        VectorSelected = currentRow;
    }
}
//Utilisateur clique sur la table des transform track
void TrackgroupView::on_tableWidget_trackgroupsTransform_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(TransformSelected != row){
        loadTransformerTrackSelected(mapListTransformTrack[row]);
        TransformSelected = row;
    }
}
void TrackgroupView::on_tableWidget_trackgroupsTransform_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(previousRow == currentRow){
        return;
    }
    if(currentColumn < 0){
        //anti warning
        if(previousColumn != 0){
            return;
        }
        return;
    }
    if(TransformSelected != currentRow){
        loadTransformerTrackSelected(mapListTransformTrack[currentRow]);
        TransformSelected = currentRow;
    }
}
//Utilisateur clique sur la table des text track
void TrackgroupView::on_tableWidget_trackgroupsText_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(TextSelected != row){
        loadTextTrackSelected(mapListTextTrack[row]);
        TextSelected = row;
    }
}
void TrackgroupView::on_tableWidget_trackgroupsText_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn){
    if(previousRow == currentRow){
        return;
    }
    if(currentColumn < 0){
        //anti warning
        if(previousColumn != 0){
            return;
        }
        return;
    }
    if(TextSelected != currentRow){
        loadTextTrackSelected(mapListTextTrack[currentRow]);
        TextSelected = currentRow;
    }
}

void TrackgroupView::resetTransformerTrack(){
    for(int i = ui->tableWidget_trackgroupsTransform->rowCount(); i >= 0; i--){
        ui->tableWidget_trackgroupsTransform->removeRow(i);
    }
    for(int i = ui->tableWidget_transformSelectedPos->rowCount(); i >= 0; i--){
        ui->tableWidget_transformSelectedPos->removeRow(i);
    }
    //ui->label_valTransformSelectedPosFormat->setText("None");
    //ui->label_valTransformSelectedPosDegree->setText("None");
    for(int i = ui->tableWidget_transformSelectedOri->rowCount(); i >= 0; i--){
        ui->tableWidget_transformSelectedOri->removeRow(i);
    }
    //ui->label_valTransformSelectedOriFormat->setText("None");
    //ui->label_valTransformSelectedOriDegree->setText("None");
    for(int i = ui->tableWidget_transformSelectedScale->rowCount(); i >= 0; i--){
        ui->tableWidget_transformSelectedScale->removeRow(i);
    }
    //ui->label_transformSelectedScaleFormat->setText("None");
    //ui->label_transformSelectedScaleDegree->setText("None");

}
void TrackgroupView::resetTrackLOD(){
    for(int i = ui->tableWidget_trackgroupsLOD->rowCount(); i >= 0; i--){
        ui->tableWidget_trackgroupsLOD->removeRow(i);
    }
}
void TrackgroupView::resetTextTrack(){
    for(int i = ui->tableWidget_trackgroupsText->rowCount(); i >= 0; i--){
        ui->tableWidget_trackgroupsText->removeRow(i);
    }
    for(int i = ui->tableWidget_trackgroupsTextEntries->rowCount(); i >= 0; i--){
        ui->tableWidget_trackgroupsTextEntries->removeRow(i);
    }
}
void TrackgroupView::resetVectorTrack(){
    for(int i = ui->tableWidget_vectorTrack->rowCount(); i >= 0; i--){
        ui->tableWidget_vectorTrack->removeRow(i);
    }
}
