#ifndef GR2WIDGET_H
#define GR2WIDGET_H

#include <QWidget>

namespace Ui {
class gr2Widget;
}

class gr2Widget : public QWidget
{
    Q_OBJECT

public:
    explicit gr2Widget(QWidget *parent = nullptr);
    ~gr2Widget();

private:
    Ui::gr2Widget *ui;
};

#endif // GR2WIDGET_H
