#ifndef TEXTURESVIEW_H
#define TEXTURESVIEW_H

#include <QWidget>

#include "matrix3x3.h"
#include "NBCpp/granny.h"

namespace Ui {
class TexturesView;
}

class TexturesView : public QWidget
{
    Q_OBJECT

public:
    explicit TexturesView(QWidget *parent = nullptr);
    ~TexturesView();
    //gridLayout_pixelLayout
public slots:
    void addTextures(granny_int32 TextureCount, granny_texture ** Textures);

    void resetTextures(void);
private slots:
    void on_tableWidget_texture_cellClicked(int row, int column);

    void resetTextureSelected(void);
    void resetTextureSelectedFileName(void);
    void resetTextureSelectedType(void);
    void resetTextureSelectedWidth(void);
    void resetTextureSelectedHeight(void);
    void resetTextureSelectedEncoding(void);
    void resetTextureSelectedSubFormat(void);
    void resetTextureSelectedLayout(void);
    void resetTextureSelectedLayoutBytePerPixel(void);
    void resetTextureSelectedLayoutShiftForCompo(void);
    void resetTextureSelectedLayoutBitsForCompo(void);
    void resetTextureSelectedImageCount(void);

    void loadTextureSelected(granny_texture * texture);
    void loadTextureSelectedFileName(granny_texture * texture);
    void loadTextureSelectedType(granny_texture * texture);
    void loadTextureSelectedWidth(granny_texture * texture);
    void loadTextureSelectedHeight(granny_texture * texture);
    void loadTextureSelectedEncoding(granny_texture * texture);
    void loadTextureSelectedSubFormat(granny_texture * texture);
    void loadTextureSelectedLayout(granny_texture * texture);
    void loadTextureSelectedLayoutBytePerPixel(granny_texture * texture);
    void loadTextureSelectedLayoutShiftForCompo(granny_texture * texture);
    void loadTextureSelectedLayoutBitsForCompo(granny_texture * texture);
    void loadTextureSelectedImageCount(granny_texture * texture);


    void on_tableWidget_texture_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

private:
    Ui::TexturesView *ui;
    QString log;
    int textureSelected;
    std::map<int, granny_texture *> mapListTexture;
};

#endif // TEXTURESVIEW_H
