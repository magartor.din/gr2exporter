#ifndef MESHESVIEW_H
#define MESHESVIEW_H

#include <QWidget>
#include <QTreeWidget>

#include "materialsview.h"
#include "NBCpp/granny.h"

namespace Ui {
class MeshesView;
}

class MeshesView : public QWidget
{
    Q_OBJECT

public:
    explicit MeshesView(QWidget *parent = nullptr);
    ~MeshesView();

public slots:
    void addMeshes(granny_int32 MeshCount, granny_mesh ** Meshes);
    void resetMeshes(void);
    void resetMeshesSelected(void);
    void resetMeshesSelectedMorph(void);
    void resetMeshesSelectedBoneBinded(void);

    void loadMeshesSelected(granny_mesh *meshes);
    void loadMeshesSelectedName(granny_mesh *meshes);
    void loadMeshesSelectedVertexAddr(granny_mesh *meshes);
    void loadMeshesSelectedTriTopo(granny_mesh *meshes);

    void loadMeshesSelectedMorph(granny_mesh *meshes);

    void loadMeshesSelectedMorphSelected(granny_morph_target *morph);
    void loadMeshesSelectedMorphName(granny_morph_target * morph);
    void loadMeshesSelectedMorphVertex(granny_morph_target * morph);
    void loadMeshesSelectedMorphDelta(granny_morph_target * morph);

    void loadMeshesSelectedMaterial(granny_mesh *meshes);

    void loadMeshesSelectedMaterialSelected(granny_material_binding * material);
    void loadMeshesSelectedMaterialSelectedName(granny_material_binding * material);

    void loadMeshesSelectedBoneBinded(granny_mesh *meshes);
    void loadMeshesSelectedBoneBindedSelected(granny_bone_binding *boneBinding);
    void loadMeshesSelectedBoneBindedSelectedName(granny_bone_binding *boneBinding);
    void loadMeshesSelectedBoneBindedSelectedOBB(granny_bone_binding *boneBinding);
    void loadMeshesSelectedBoneBindedSelectedOBBmin(granny_bone_binding *boneBinding);
    void loadMeshesSelectedBoneBindedSelectedOBBmax(granny_bone_binding *boneBinding);
    void loadMeshesSelectedBoneBindedSelectedTriangle(granny_bone_binding *boneBinding);

private slots:
    void on_tableWidget_meshes_cellClicked(int row, int column);

    void on_tableWidget_meshes_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

    void on_tableWidget_morph_cellClicked(int row, int column);

    void on_tableWidget_morph_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

    void on_tableWidget_material_cellChanged(int row, int column);

    void on_tableWidget_material_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

    void on_tableWidget_biendedBone_cellClicked(int row, int column);

    void on_tableWidget_biendedBone_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

private:
    Ui::MeshesView *ui;
    QString log;

    int meshSelected;
    int MorphSelected;
    int MaterialSelected;
    int BonesSelected;

    std::map<int, granny_mesh *> mapListMeshes;
    std::map<int, granny_morph_target *> mapListMorph;
    std::map<int, granny_material_binding *> mapListMaterial;
    std::map<int, granny_bone_binding *> mapListBones;
};

#endif // MESHESVIEW_H
