#include "matrix3x3.h"
#include "ui_matrix3x3.h"

Matrix3x3::Matrix3x3(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Matrix3x3)
{
    ui->setupUi(this);

    this->matLabel[0][0] = ui->label_M00;
    this->matLabel[0][1] = ui->label_M01;
    this->matLabel[0][2] = ui->label_M02;

    this->matLabel[1][0] = ui->label_M10;
    this->matLabel[1][1] = ui->label_M11;
    this->matLabel[1][2] = ui->label_M12;

    this->matLabel[2][0] = ui->label_M20;
    this->matLabel[2][1] = ui->label_M21;
    this->matLabel[2][2] = ui->label_M22;
}

Matrix3x3::~Matrix3x3()
{
    delete ui;
}
void Matrix3x3::reset(void){
    for(unsigned int i = 0; i < 3; i++){
        for(unsigned int j = 0; j < 3; j++){
            this->matLabel[i][j]->setText("0");
        }
    }
}

template<>
void Matrix3x3::setValues(int ** vals){
    try {
        for(unsigned int i = 0; i < 3; i++){
            for(unsigned int j = 0; j < 3; j++){
                this->matLabel[i][j]->setText(QString::number(vals[i][j]));
            }
        }
    } catch (...) {
        log += "Pointeur de donnée invalide\n";
    }
}
template<>
void Matrix3x3::setValues(granny_triple vals[3]){
    try {
        for(unsigned int i = 0; i < 3; i++){
            for(unsigned int j = 0; j < 3; j++){
                this->matLabel[i][j]->setText(QString::number(vals[i][j]));
            }
        }
    } catch (...) {
        log += "Pointeur de donnée invalide\n";
    }
}
template<>
void Matrix3x3::setValues(int vals[3][3]){
    try {
        for(unsigned int i = 0; i < 3; i++){
            for(unsigned int j = 0; j < 3; j++){
                this->matLabel[i][j]->setText(QString::number(vals[i][j]));
            }
        }
    } catch (...) {
        log += "Pointeur de donnée invalide\n";
    }
}
