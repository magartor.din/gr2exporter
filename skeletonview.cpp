#include "skeletonview.h"
#include "ui_skeletonview.h"

SkeletonView::SkeletonView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SkeletonView)
{
    ui->setupUi(this);
    //gridLayout_localTransform
    //horizontalLayout_boneInvWorld
    localTransform = new Matrix3x3();
    boneInvWorld = new Matrix4x4();
    extendedDateBone = new ExtendedDataView();
    ui->verticalLayout_bone->insertWidget(ui->verticalLayout_bone->count(),extendedDateBone);

    ui->gridLayout_localTransform->addWidget(localTransform, 3,1,1,1);
    ui->horizontalLayout_boneInvWorld->insertWidget(1,boneInvWorld,0, Qt::AlignLeft);
    ui->tableWidget_skeleton->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
    ui->tableWidget_bones->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);

    skeletonSelected = -1;
    boneSelected = -1;
}

SkeletonView::~SkeletonView(){
    delete ui;
    delete localTransform;
    delete boneInvWorld;
}

void SkeletonView::addSkeletons(granny_int32 SkeletonCount, granny_skeleton ** Skeletons){
    //Verifie le pointeur vers le tableau de skeletons
    if(Skeletons == nullptr){
        return;
    }
    //Variable iteration skeleton
    granny_int32 index;
    for(index = 0; index < SkeletonCount;index++){
        //Pour tout les modèles du tableau
        try{
            if(Skeletons[index] != nullptr){
                //Si le pointeur est valid
                try {
                    //stock le pointeur dans une variable locale
                    granny_skeleton * curSkeleton = Skeletons[index];
                    //Si le pointeur du modèle est valide on recupère le nombre de ligne
                    int row = ui->tableWidget_skeleton->rowCount();
                    //on ajoute une ligne et on increment le nombre de ligne
                    ui->tableWidget_skeleton->insertRow(row);
                    QString skeletonName;
                    //Recupère le nombre de colone
                    //int column = ui->tableWidget_skeleton->columnCount();
                    QTableWidgetItem *newItem = nullptr;
                    if(curSkeleton->Name != nullptr){
                        //Si le nom du modèle est valide
                        skeletonName = QString::fromLocal8Bit(curSkeleton->Name);
                    }
                    else{
                        //Si le nom du modèle n'est pas valide on genere un nom
                        skeletonName = QString::number((int)curSkeleton, 16);
                    }
                    //Crée l'objet représentant le modèle dans la liste
                    newItem = new QTableWidgetItem(skeletonName);
                    //Ajoute le modèle à la liste
                    ui->tableWidget_skeleton->setItem(row, 0, newItem);
                    mapListSkeleton[row] = curSkeleton;

                }catch (...){
                    log += "Error accessing skeleton " + QString::number(index);
                }
            }
        }catch (...) {
            log += "Error accessing skeletons table" + QString::number(index);
        }
    }
    ui->tableWidget_skeleton->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
}
void SkeletonView::resetSkeletons(void){
    mapListSkeleton.clear();
    mapListBone.clear();
    skeletonSelected = -1;
    boneSelected = -1;
    ui->label_valName->setText("None");
    ui->label_valLOD->setText("None");
    ui->tableWidget_bones->clear();
    ui->tableWidget_bones->setHorizontalHeaderItem(0,new QTableWidgetItem("Name"));
    ui->tableWidget_bones->setHorizontalHeaderItem(1,new QTableWidgetItem("Parents"));
    for(int i = ui->tableWidget_bones->rowCount(); i >= 0; i--){
        ui->tableWidget_bones->removeRow(i);
    }
    ui->tableWidget_skeleton->clear();
    ui->tableWidget_skeleton->setHorizontalHeaderItem(0,new QTableWidgetItem("Name"));
    for(int i = ui->tableWidget_skeleton->rowCount(); i >= 0; i--){
        ui->tableWidget_skeleton->removeRow(i);
    }
    resetBones();
}
void SkeletonView::resetBones(void){
    ui->label_valBoneName->setText("None");
    ui->label_valBoneParent->setText("None");
    ui->label_valBoneLOD->setText("None");
    boneInvWorld->reset();
    ui->label_valBoneTransformFlag->setText("None");
    ui->label_boneTransformPos0->setText("None");
    ui->label_boneTransformPos1->setText("None");
    ui->label_boneTransformPos2->setText("None");
    ui->label_boneTransformOri0->setText("None");
    ui->label_boneTransformOri1->setText("None");
    ui->label_boneTransformOri2->setText("None");
    ui->label_boneTransformOri3->setText("None");
    localTransform->reset();
    extendedDateBone->resetGrannyVariant();
}
void SkeletonView::loadSkeletonSelected(granny_skeleton * skeleton){
    if(skeleton == nullptr){
        return;
    }
    try {
        loadSkeletonSelectedName(skeleton);
        loadSkeletonSelectedLODError(skeleton);
        loadSkeletonSelectedBones(skeleton);
    } catch (...) {
        log += "Error accessing skeletons selected";
    }
}
void SkeletonView::loadSkeletonSelectedName(granny_skeleton * skeleton){
    if(skeleton == nullptr){
        return;
    }
    try {
        if(skeleton != nullptr){
            ui->label_valName->setText(skeleton->Name);
        }
        else {
            ui->label_valName->setText("0x" + QString::number((int)skeleton));
        }
    } catch (...) {
        log += "Error accessing skeletons selected";
    }
}
void SkeletonView::loadSkeletonSelectedLODError(granny_skeleton * skeleton){
    if(skeleton == nullptr){
        return;
    }
    try {
        ui->label_valLOD->setText(QString::number(skeleton->LODType));
    } catch (...) {
        log += "Error accessing skeletons selected";
    }
}
void SkeletonView::loadSkeletonSelectedBones(granny_skeleton * skeleton){
    if(skeleton == nullptr){
        return;
    }
    try {
        mapListBone.clear();
        for(granny_int32 i = 0; i < skeleton->BoneCount; i++){
            //Calcul le nombre de ligne de la liste
            int row = ui->tableWidget_bones->rowCount();
            //Ajoute une ligne
            ui->tableWidget_bones->insertRow(row);
            QString BoneName;
            QTableWidgetItem *newItemName = nullptr;
            QTableWidgetItem *newItemParent = nullptr;
            //Recupère le pointeur du bone courant
            granny_bone * curBone = &skeleton->Bones[i];
            //Crée un chaine représentant l'index du bone parent
            if(curBone->Name != nullptr){
                //Si le nom du modèle est valide
                BoneName = QString::fromLocal8Bit(curBone->Name);
            }
            else{
                //Si le nom du modèle n'est pas valide on genere un nom
                BoneName = QString::number((int)curBone, 16);
            }
            newItemName = new QTableWidgetItem(BoneName);
            //Crée un chaine représentant l'index du bone parent
            newItemParent = new QTableWidgetItem(QString::number(curBone->ParentIndex));

            ui->tableWidget_bones->setItem(row, 0, newItemName);
            ui->tableWidget_bones->setItem(row, 1, newItemParent);
            mapListBone[row] = curBone;
        }
        ui->tableWidget_bones->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
        loadBoneSelected(&skeleton->Bones[0]);
    } catch (...) {
        log += "Error accessing skeletons selected";
    }
}
//Affiche le bone selectionné
void SkeletonView::loadBoneSelected(granny_bone * bone){
    if(bone == nullptr){
        return;
    }
    try {
        loadBoneSelectedName(bone);
        loadBoneSelectedParentIndex(bone);
        loadBoneSelectedLODError(bone);
        loadBoneSelectedInverseWorldMatrix(bone);
        loadBoneSelectedLocalTransform(bone);
        loadBoneSelectedLocalTransformFlag(bone);
        loadBoneSelectedLocalTransformPos(bone);
        loadBoneSelectedLocalTransformOri(bone);
        loadBoneSelectedLocalTransformScale(bone);
        loadBoneSelectedExtendData(bone);
    } catch (...) {
        log += "Error accessing skeletons selected";
    }
}
//Affiche le nom du bone selectionné
void SkeletonView::loadBoneSelectedName(granny_bone * bone){
    if(bone == nullptr){
        return;
    }
    try {
        if(bone->Name == nullptr){
            ui->label_valBoneName->setText("0x" + QString::number((int)bone));
        }
        else{
            ui->label_valBoneName->setText(bone->Name);
        }
    } catch (...) {
        log += "Error accessing bone selected";
    }
}
//Affiche l'index du parent du bone selectionné
void SkeletonView::loadBoneSelectedParentIndex(granny_bone * bone){
    if(bone == nullptr){
        return;
    }
    try {
        QString str(QString::number(bone->ParentIndex));
        ui->label_valBoneParent->setText(str);
    } catch (...) {
        log += "Error accessing bone selected";
    }
}
//Affiche le LOD du bone selectionné
void SkeletonView::loadBoneSelectedLODError(granny_bone * bone){
    if(bone == nullptr){
        return;
    }
    try {
        QString str(QString::number(bone->LODError));
        ui->label_valBoneLOD->setText(str);

    } catch (...) {
        log += "Error accessing bone selected";
    }
}
//Affiche la matrice world du bone selectionné
void SkeletonView::loadBoneSelectedInverseWorldMatrix(granny_bone * bone){
    if(bone == nullptr){
        return;
    }
    try {
        boneInvWorld->setValues(bone->InverseWorld4x4);
    } catch (...) {
        log += "Error accessing bone selected";
    }
}
//Affiche la transformation local du bone selectionné
void SkeletonView::loadBoneSelectedLocalTransform(granny_bone * bone){
    if(bone == nullptr){
        return;
    }
    try {
        loadBoneSelectedLocalTransformFlag(bone);
        loadBoneSelectedLocalTransformPos(bone);
        loadBoneSelectedLocalTransformOri(bone);
        loadBoneSelectedLocalTransformScale(bone);
    } catch (...) {
        log += "Error accessing bone selected";
    }
}
//Affiche le flag de la transformation local du bone selectionné
void SkeletonView::loadBoneSelectedLocalTransformFlag(granny_bone * bone){
    if(bone == nullptr){
        return;
    }
    try {
        QString str;
        switch (bone->LocalTransform.Flags) {
            case GrannyHasPosition:{
                str = "GrannyHasPosition";
                break;
            }
            case GrannyHasOrientation:{
                str = "GrannyHasOrientation";
                break;
            }
            case GrannyHasScaleShear:{
                str = "GrannyHasScaleShear";
                break;
            }
            case Grannytransform_flags_forceint:{
                str = "Grannytransform_flags_forceint";
                break;
            }
            default:
                str = "Unknown flag" + QString::number(bone->LocalTransform.Flags);
        }
        ui->label_valBoneTransformFlag->setText(str);
    } catch (...) {
        log += "Error accessing bone selected";
    }
}
//Affiche le Position de la transformation local du bone selectionné
void SkeletonView::loadBoneSelectedLocalTransformPos(granny_bone * bone){
    if(bone == nullptr){
        return;
    }
    try {
        ui->label_boneTransformPos0->setText(QString::number(bone->LocalTransform.Position[0]));
        ui->label_boneTransformPos1->setText(QString::number(bone->LocalTransform.Position[1]));
        ui->label_boneTransformPos2->setText(QString::number(bone->LocalTransform.Position[2]));
    } catch (...) {
        log += "Error accessing bone selected";
    }
}
//Affiche l'orientation de la transformation local du bone selectionné
void SkeletonView::loadBoneSelectedLocalTransformOri(granny_bone * bone){
    if(bone == nullptr){
        return;
    }
    try {
        ui->label_boneTransformOri0->setText(QString::number(bone->LocalTransform.Orientation[0]));
        ui->label_boneTransformOri1->setText(QString::number(bone->LocalTransform.Orientation[1]));
        ui->label_boneTransformOri2->setText(QString::number(bone->LocalTransform.Orientation[2]));
        ui->label_boneTransformOri3->setText(QString::number(bone->LocalTransform.Orientation[3]));
    } catch (...) {
        log += "Error accessing bone selected";
    }
}
//Affiche le scale de la transformation local du bone selectionné
void SkeletonView::loadBoneSelectedLocalTransformScale(granny_bone * bone){
    if(bone == nullptr){
        return;
    }
    try {
        localTransform->setValues(bone->LocalTransform.ScaleShear);
    } catch (...) {
        log += "Error accessing bone selected";
    }
}
//Affiche les donnée étendu du bone selectionné
void SkeletonView::loadBoneSelectedExtendData(granny_bone * bone){
    if(bone == nullptr){
        return;
    }
    try {
        extendedDateBone->loadGrannyVariant(&bone->ExtendedData);
    } catch (...) {
        log += "Error accessing bone selected";
    }
}

void SkeletonView::on_tableWidget_skeleton_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(row != skeletonSelected){
        boneSelected = -1;
        skeletonSelected = row;
        loadSkeletonSelected(mapListSkeleton[row]);
    }
}
void SkeletonView::on_tableWidget_bones_cellClicked(int row, int column){
    if(column < 0){
        return;
    }
    if(row != boneSelected){
        boneSelected = row;
        loadBoneSelected(mapListBone[row]);
    }
}

void SkeletonView::on_tableWidget_bones_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
    if(currentColumn < 0){
        return;
    }
    if(currentRow == previousRow){
        //anti warning
        if(previousColumn)
            return;
        return;
    }
    if(currentRow != boneSelected){
        boneSelected = currentRow;
        loadBoneSelected(mapListBone[currentRow]);
    }
}
